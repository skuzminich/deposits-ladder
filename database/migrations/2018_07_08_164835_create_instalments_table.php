<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInstalmentsTable
 */
class CreateInstalmentsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'instalments',
            function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedTinyInteger('latest_version');

                $table->timestamp('created_at');

                $table->unsignedInteger('user_deposit_id');
                $table->foreign('user_deposit_id')
                    ->references('id')->on('user_deposits');
            }
        );

        Schema::create(
            'instalments_version',
            function (Blueprint $table) {
                $table->unsignedInteger('ref_id');
                $table->unsignedTinyInteger('version');
                $table->primary(['ref_id', 'version']);

                // Сумма взноса
                $table->unsignedDecimal('amount', 15, 2);
                // День взноса
                $table->timestamp('deposit_date');

                $table->timestamp('updated_at')->nullable();
                $table->softDeletes();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instalments');
        Schema::dropIfExists('instalments_version');
    }


}