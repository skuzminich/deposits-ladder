<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateWithdrawalsTable
 */
class CreateWithdrawalsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'withdrawals',
            function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedTinyInteger('latest_version');

                $table->timestamp('created_at');

                $table->unsignedInteger('user_deposit_id');
                $table->foreign('user_deposit_id')
                    ->references('id')->on('user_deposits');
            }
        );

        Schema::create(
            'withdrawals_version',
            function (Blueprint $table) {
                $table->unsignedInteger('ref_id');
                $table->unsignedTinyInteger('version');
                $table->primary(['ref_id', 'version']);

                // Сумма вывода
                $table->unsignedDecimal('amount', 15, 2);
                // День вывода
                $table->timestamp('withdrawal_date');

                $table->timestamp('updated_at')->nullable();
                $table->softDeletes();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawals');
        Schema::dropIfExists('withdrawals_version');
    }


}