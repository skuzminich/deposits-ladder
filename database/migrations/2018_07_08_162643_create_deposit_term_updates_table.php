<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDepositTermUpdatesTable
 */
class CreateDepositTermUpdatesTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'deposit_term_updates',
            function (Blueprint $table) {
                $table->increments('id');

                $table->timestamp('inform_date');
                $table->timestamp('start_date');
                $table->timestamp('deposit_from_date')->nullable();
                $table->timestamp('deposit_to_date')->nullable();

                $table->unsignedDecimal('irreducible_balance')->nullable();
                // Условия отзыва вклада
                $table->string('full_revoke_conditions')->nullable();
                // Условия частичного отзыва вклада
                $table->string('partial_revoke_conditions')->nullable();
                // Процент начисляемый при отзыве вклада
                // До 60 дней проценты по вкладу до восстребванию, более 60 проценты не начисляются
                // До X дней одна ставка, больше X другая
                $table->unsignedDecimal('full_revoke_percent')->nullable();
                // Процент отнимаемый от ставки при частичном сниятии
                $table->unsignedDecimal('partial_revoke_sanctions_percent')->nullable();

                // Как часто происходит начисление процентов, ежемесячно, еженедельно, разово
                $table->unsignedTinyInteger('calculation_of_interest_period')->nullable();
                // Возможна ли каптализация процентов
                $table->unsignedTinyInteger('capitalization_allowed')->nullable();
                // Возможность частичного снятия процентов
                $table->unsignedTinyInteger('percent_withdrawal_allowed')->nullable();

                // Фиксированная или плавающая или от СР или от ОН
                $table->unsignedTinyInteger('rate_type')->nullable();
                // Ставка или проценты, которые нужно отнять от СР или от ОН
                $table->unsignedDecimal('rate', 4, 2)->nullable();
                // Период фиксированной ставки, например 'P12M'
                $table->char('fixed_rate_period', 6)->nullable();

                // Предусмотрены ли дополнительные взносы
                $table->unsignedTinyInteger('refilling_type')->nullable();
                // Период в течении которого разрешены взносы
                $table->char('refilling_period', 6)->nullable();
                // Минимальная сумма пополнения
                $table->unsignedDecimal('refilling_min_sum')->nullable();

                // Сколько раз прологнируется
                $table->unsignedTinyInteger('prolongation')->nullable();
                // Условия пролонгации
                $table->string('prolongation_conditions')->nullable();

                // Минимальная сумма вклада
                $table->unsignedDecimal('min_sum', 9, 2)->nullable();
                // Максимальная сумма вклада
                $table->unsignedDecimal('max_sum', 15, 2)->nullable();

                // Вклад может быть открыт на третье лицо
                $table->unsignedTinyInteger('third_person')->nullable();
                // Ссылка на вклад
                $table->string('link', 256)->nullable();
                // Ограничение на открытие вклада
                $table->string('restriction', 256)->nullable();

                $table->unsignedInteger('deposit_term_id');
                $table->foreign('deposit_term_id')
                    ->references('id')->on('deposit_terms');

                $table->timestamps();
                $table->softDeletes();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_term_updates');
    }


}