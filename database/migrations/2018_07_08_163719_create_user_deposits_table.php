<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUserDepositsTable
 */
class CreateUserDepositsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'user_deposits',
            function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedTinyInteger('latest_version');

                $table->timestamp('created_at');

                $table->unsignedInteger('deposit_term_id');
                $table->foreign('deposit_term_id')
                    ->references('id')->on('deposit_terms');

                $table->unsignedInteger('user_id');
                $table->foreign('user_id')
                    ->references('id')->on('users');
            }
        );

        Schema::create(
            'user_deposits_version',
            function (Blueprint $table) {
                $table->unsignedInteger('ref_id');
                $table->unsignedTinyInteger('version');
                $table->primary(['ref_id', 'version']);

                // Сумма вклада
                $table->unsignedDecimal('balance', 15, 2);
                // День открытия вклада
                $table->timestamp('start_date');
                // День закрытия вклада
                $table->timestamp('end_date')->nullable();

                $table->timestamp('updated_at')->nullable();
                $table->softDeletes();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_deposits');
        Schema::dropIfExists('user_deposits_version');
    }


}