<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBanksTable
 */
class CreateBanksTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'banks',
            function (Blueprint $table) {
                $table->tinyIncrements('id');

                $table->unsignedTinyInteger('latest_version');
                $table->unsignedTinyInteger('status')->default(0);

                $table->timestamp('created_at');
            }
        );

        Schema::create(
            'banks_version',
            function (Blueprint $table) {
                $table->unsignedTinyInteger('ref_id');
                $table->unsignedTinyInteger('version');
                $table->primary(['ref_id', 'version']);

                $table->string('full_name', 128);
                $table->string('short_name', 64);
                $table->string('link', 64);

                $table->timestamp('updated_at')->nullable();
                $table->softDeletes();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
        Schema::dropIfExists('banks_version');
    }


}