<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDepositsTable
 */
class CreateDepositsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'deposits',
            function (Blueprint $table) {
                $table->smallIncrements('id');

                $table->unsignedTinyInteger('latest_version');
                $table->unsignedTinyInteger('status')->default(0);

                $table->timestamp('created_at');

                $table->unsignedTinyInteger('bank_id');
                $table->foreign('bank_id')
                    ->references('id')->on('banks');

            }
        );

        Schema::create(
            'deposits_version',
            function (Blueprint $table) {
                $table->unsignedSmallInteger('ref_id');
                $table->unsignedTinyInteger('version');
                $table->primary(['ref_id', 'version']);

                $table->string('name', 128);
                $table->string('link', 256)->nullable();
                $table->string('open_restriction', 512)->nullable();

                $table->timestamp('updated_at')->nullable();
                $table->softDeletes();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
        Schema::dropIfExists('deposits_version');
    }


}