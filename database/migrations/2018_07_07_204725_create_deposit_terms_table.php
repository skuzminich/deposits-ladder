<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDepositTermsTable
 */
class CreateDepositTermsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'deposit_terms',
            function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedTinyInteger('latest_version');

                // Срок в днях, месяцах, годах, до востребования 'P3M'
                $table->char('period', 6);
                // Отзывный или безотзывный
                $table->unsignedTinyInteger('is_irrevocable');

                $table->timestamp('created_at');

                $table->unsignedTinyInteger('deposit_id');
                $table->foreign('deposit_id')
                    ->references('id')->on('deposits');

                $table->unsignedTinyInteger('currency_id');
                $table->foreign('currency_id')
                    ->references('id')->on('currencies');
            }
        );

        Schema::create(
            'deposit_terms_version',
            function (Blueprint $table) {
                $table->unsignedInteger('ref_id');
                $table->unsignedTinyInteger('version');
                $table->primary(['ref_id', 'version']);

                $table->string('name', 128)->nullable();

                // Неснижаемый остаток
                $table->unsignedDecimal('irreducible_balance')->nullable();
                // Условия отзыва вклада
                $table->string('full_revoke_conditions')->nullable();
                // Условия частичного отзыва вклада
                $table->string('partial_revoke_conditions')->nullable();
                // Процент начисляемый при отзыве вклада
                // До 60 дней проценты по вкладу до восстребванию, более 60 проценты не начисляются
                // До X дней одна ставка, больше X другая
                $table->unsignedDecimal('full_revoke_percent')->nullable();
                // Процент отнимаемый от ставки при частичном сниятии
                $table->unsignedDecimal('partial_revoke_sanctions_percent')->nullable();

                // Как часто происходит начисление процентов, ежемесячно, еженедельно, разово
                $table->unsignedTinyInteger('calculation_of_interest_period');
                // Возможна ли каптализация процентов
                $table->unsignedTinyInteger('capitalization_allowed');
                // Возможность частичного снятия процентов
                $table->unsignedTinyInteger('percent_withdrawal_allowed');

                // Фиксированная или плавающая или от СР или от ОН
                $table->unsignedTinyInteger('rate_type');
                // Ставка или проценты, которые нужно отнять от СР или от ОН
                $table->unsignedDecimal('rate', 4, 2);
                // Период фиксированной ставки, например 'P12M'
                $table->char('fixed_rate_period', 6)->nullable();

                // Предусмотрены ли дополнительные взносы
                $table->unsignedTinyInteger('refilling_type');
                // Период в течении которого разрешены взносы
                $table->char('refilling_period', 6)->nullable();
                // Минимальная сумма пополнения
                $table->unsignedDecimal('refilling_min_sum')->nullable();

                // Сколько раз прологнируется
                $table->unsignedTinyInteger('prolongation');
                // Условия пролонгации
                $table->string('prolongation_conditions')->nullable();

                // Минимальная сумма вклада
                $table->unsignedDecimal('min_sum', 9, 2);
                // Максимальная сумма вклада
                $table->unsignedDecimal('max_sum', 15, 2)->nullable();

                // Вклад может быть открыт на третье лицо
                $table->unsignedTinyInteger('third_person')->nullable();
                // Ссылка на вклад
                $table->string('link', 256)->nullable();
                // Ограничение на открытие вклада
                $table->string('restriction', 256)->nullable();
                $table->unsignedTinyInteger('internet');

                $table->timestamp('updated_at')->nullable();
                $table->softDeletes();
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_terms');
        Schema::dropIfExists('deposit_terms_version');
    }


}