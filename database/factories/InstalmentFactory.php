<?php

use App\Instalment;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    Instalment::class,
    function (Faker $faker) {
        return [
            Instalment::AMOUNT => $faker->randomFloat(2, 100, 1000),
            Instalment::DEPOSIT_DATE => $faker->dateTimeBetween('-1 year', '-1 day')->format('Y-m-d H:i:s'),
        ];
    }
);