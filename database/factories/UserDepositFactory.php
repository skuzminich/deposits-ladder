<?php

use App\UserDeposit;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    UserDeposit::class,
    function (Faker $faker) {
        $allDepositsIds = \App\Deposit::where('id' ,'>' ,0)->pluck('id')->toArray();
        return [
            UserDeposit::BALANCE => $faker->randomFloat(2, 100, 1000000),
            UserDeposit::START_DATE => $faker->dateTimeBetween('-1 year', '-2 month')->format('Y-m-d H:i:s'),
            UserDeposit::END_DATE => $faker->dateTimeBetween('-1 month', '+1 year')->format('Y-m-d H:i:s'),
            UserDeposit::DEPOSIT_TERM_ID => $faker->randomElement($allDepositsIds),
        ];
    }
);
