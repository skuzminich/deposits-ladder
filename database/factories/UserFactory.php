<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    App\User::class,
    function (Faker $faker) {
        /** @noinspection SpellCheckingInspection */
        return [
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'password' => $faker->sha256,
            'remember_token' => $faker->isbn10,
        ];
    }
);
