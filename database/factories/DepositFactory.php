<?php

use App\Deposit;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    Deposit::class,
    function (Faker $faker) {
        $randomText = $faker->realText(64);
        $words      = preg_split('/[[:space:][:punct:][:graph:][:cntrl:][:ascii:]]+/', $randomText);
        $name       = $faker->randomElements(array_filter($words), $faker->numberBetween(1, 3));
        return [
            Deposit::NAME => implode(' ', $name),
            Deposit::LINK => $faker->url,
            Deposit::STATUS => $faker->randomElement(Deposit::getAllStatuses()),
            Deposit::OPEN_RESTRICTION => null,
        ];
    }
);
