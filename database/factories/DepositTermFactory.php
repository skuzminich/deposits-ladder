<?php

use App\DepositTerm;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    DepositTerm::class,
    function (Faker $faker) {
        $allCurrencyIds = \App\Currency::where('id' ,'>' ,0)->pluck('id')->toArray();
        return [
            DepositTerm::PERIOD => 'P' . $faker->numberBetween(1, 12) . 'M',
            DepositTerm::CURRENCY_ID => $faker->randomElement($allCurrencyIds),
            DepositTerm::IS_IRREVOCABLE_FIELD => $faker->boolean,
            DepositTerm::NAME => ($faker->boolean) ? $faker->text(128) : null,
            DepositTerm::CALCULATION_OF_INTEREST_PERIOD => $faker->randomElement(DepositTerm::getAllCalculationPeriods()),
            DepositTerm::CAPITALIZATION_ALLOWED => $faker->randomElement(DepositTerm::getAllCapitalizationStatuses()),
            DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => $faker->randomElement(DepositTerm::getAllPercentWithdrawalStatuses()),
            DepositTerm::RATE_TYPE => $faker->randomElement(DepositTerm::getAllRateTypes()),
            DepositTerm::RATE => $faker->randomFloat(2, 0, 50),
            DepositTerm::REFILLING_TYPE => $faker->randomElement(DepositTerm::getAllRefillingTypes()),
            DepositTerm::INTERNET => $faker->randomElement(DepositTerm::getAllInternetStatuses()),
            DepositTerm::PROLONGATION => $faker->randomNumber(1),
            DepositTerm::MIN_SUM => $faker->randomFloat(2, 0, 100),
            DepositTerm::IRREDUCIBLE_BALANCE => $faker->randomFloat(0, 100, 1000),
            DepositTerm::FULL_REVOKE_CONDITIONS => $faker->realText(),
            DepositTerm::FULL_REVOKE_PERCENT => $faker->randomFloat(2, 0, 10),
            DepositTerm::PARTIAL_REVOKE_CONDITIONS => $faker->realText(),
            DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT => $faker->randomFloat(2, 0, 10),
            DepositTerm::FIXED_RATE_PERIOD => 'P' . $faker->numberBetween(1, 6) . 'M  ',
            DepositTerm::REFILLING_PERIOD => 'P' . $faker->numberBetween(1, 6) . 'M  ',
            DepositTerm::REFILLING_MIN_SUM => $faker->numberBetween(10, 100),
            DepositTerm::PROLONGATION_CONDITIONS => $faker->realText(),
            DepositTerm::MAX_SUM => $faker->randomFloat(2, 1000, 1000000),
            DepositTerm::THIRD_PERSON => $faker->boolean,
            DepositTerm::LINK => ($faker->boolean) ? $faker->url : null,
            DepositTerm::RESTRICTION => null,
        ];
    }
);
