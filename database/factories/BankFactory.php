<?php

use App\Bank;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    Bank::class,
    function (Faker $faker) {
        $company = $faker->company;
        return [
            Bank::SHORT_NAME => mb_substr($company, mb_strpos($company, ' '), 64),
            Bank::FULL_NAME => $company,
            Bank::LINK => substr($faker->url, 0, 64),
            Bank::STATUS => $faker->randomElement(Bank::getAllStatuses()),
        ];
    }
);
