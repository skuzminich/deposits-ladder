<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    App\Currency::class,
    function (Faker $faker) {
        return [
            \App\Currency::NAME => $faker->word,
            \App\Currency::ISO => $faker->currencyCode,
            \App\Currency::SYMBOL => $faker->randomLetter . $faker->randomLetter,
        ];
    }
);
