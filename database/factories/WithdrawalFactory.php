<?php

use App\Withdrawal;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    Withdrawal::class,
    function (Faker $faker) {
        return [
            Withdrawal::AMOUNT => $faker->randomFloat(2, 100, 1000),
            Withdrawal::WITHDRAWAL_DATE => $faker->dateTimeBetween('-1 year', '-1 day')->format('Y-m-d H:i:s'),
        ];
    }
);
