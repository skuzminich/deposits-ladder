<?php

use App\DepositTermUpdate;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(
    DepositTermUpdate::class,
    function (Faker $faker) {
        $result = [
            DepositTermUpdate::INFORM_DATE => $faker->dateTimeBetween('-1 month')->format('Y-m-d H:i:s'),
            DepositTermUpdate::START_DATE => $faker->dateTimeBetween('-1 month')->format('Y-m-d H:i:s'),
            DepositTermUpdate::DEPOSIT_FROM_DATE => $faker->dateTimeBetween('-1 year', '-1 month')->format('Y-m-d H:i:s'),
            DepositTermUpdate::DEPOSIT_TO_DATE => $faker->dateTimeBetween('-1 month', '-1 day')->format('Y-m-d H:i:s'),
        ];

        $dummy     = new DepositTermUpdate();
        $termNames = array_diff(
            $dummy->getFillable(),
            [
                DepositTermUpdate::INFORM_DATE,
                DepositTermUpdate::START_DATE,
                DepositTermUpdate::DEPOSIT_FROM_DATE,
                DepositTermUpdate::DEPOSIT_TO_DATE,
                $dummy->getCreatedAtColumn(),
                $dummy->getUpdatedAtColumn(),
                $dummy->getDeletedAtColumn()
            ]
        );
        $randomTermNames = $faker->randomElements($termNames, $faker->numberBetween(1, 3));
        $randomTerms     = factory(\App\DepositTerm::class)->raw();
        foreach ($randomTermNames as $randomTerm) {
            $result[$randomTerm] = $randomTerms[$randomTerm];
        }

        return $result;
    }
);
