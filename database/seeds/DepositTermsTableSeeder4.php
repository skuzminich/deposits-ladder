<?php /** @noinspection PhpUndefinedClassInspection */

use \App\DepositTerm;

/**
 * Class DepositTermsTableSeeder
 */
class DepositTermsTableSeeder4 extends DepositTermsTableSeeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        /* Москва Минск */
        /* МТБанк */

        /* Москва Минск */
        $this->create(
            'Прогрессивный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M180D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_SCHEDULE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M180D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_SCHEDULE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Капитальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 8.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 8.5,
                    DepositTerm::MIN_SUM => 10,
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2M',
                    DepositTerm::RATE => 8.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2M',
                    DepositTerm::RATE => 8.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 9.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M180D',
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 9.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M180D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 2.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Основательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 2.5,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        /* МТБанк */
        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 1,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 10,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::OVERNIGHT,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 2,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::OVERNIGHT,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P5M',
                    DepositTerm::RATE => 4.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 10,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 4,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 2,
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P5M',
                    DepositTerm::RATE => 3.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 1,
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 1.4,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M6M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 10.1,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M6M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.1,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 11.3,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P14M',
                    DepositTerm::RATE => 11.8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P16M',
                    DepositTerm::RATE => 12.3,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 1,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 10,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::OVERNIGHT,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 4,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 2,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::OVERNIGHT,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P5M',
                    DepositTerm::RATE => 3.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 7,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 10,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 2,
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P5M',
                    DepositTerm::RATE => 2.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 1,
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::OVERNIGHT,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 1.15,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M6M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M6M',
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 11.5,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P14M',
                    DepositTerm::RATE => 12,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'МТБелки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'МТБелки online',
                    DepositTerm::PERIOD => 'P16M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );
    }


}
