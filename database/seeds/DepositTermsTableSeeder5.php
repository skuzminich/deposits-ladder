<?php /** @noinspection PhpUndefinedClassInspection */

use \App\DepositTerm;

/**
 * Class DepositTermsTableSeeder
 */
class DepositTermsTableSeeder5 extends DepositTermsTableSeeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        /* Решение */
        /* Франсабанк */
        /* Альфа */
        /* БНБ */

        /* Решение */
        $this->create(
            'Формула успеха',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 11,
                ]
            )
        );

        $this->create(
            'Формула успеха',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Формула успеха',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 3,
                ]
            )
        );

        $this->create(
            'Формула успеха',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Формула успеха',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Формула успеха',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Формула успеха',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M180D',
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Online-решение',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Online-решение new',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 5,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Свободное накопление',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 3,
                ]
            )
        );

        $this->create(
            'Свободное накопление',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        $this->create(
            'Спринт-депозит new',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2M',
                    DepositTerm::RATE => 4,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Твой ход',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M30D',
                ]
            )
        );

        /* Франсабанк */
        $this->create(
            'Результативный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P45D',
                    DepositTerm::RATE => 7.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Результативный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P60D',
                ]
            )
        );

        $this->create(
            'Результативный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P150D',
                ]
            )
        );

        $this->create(
            'Результативный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P385D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Результативный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P735D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Верный рассчет',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P45D',
                    DepositTerm::RATE => 8.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Верный рассчет',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P60D',
                ]
            )
        );

        $this->create(
            'Верный рассчет',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::FIXED_RATE_PERIOD => 'P3M',
                ]
            )
        );

        $this->create(
            'Верный рассчет',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P385D',
                    DepositTerm::RATE => 11.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::FIXED_RATE_PERIOD => 'P3M',
                ]
            )
        );

        $this->create(
            'Верный рассчет',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P735D',
                    DepositTerm::RATE => 12,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::FIXED_RATE_PERIOD => 'P3M',
                ]
            )
        );

        /* Альфа */
        $this->create(
            'Альфа-Ракета',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_SCHEDULE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P14M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                ]
            )
        );

        $this->create(
            'Альфа-Фреш',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.1,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                    DepositTerm::REFILLING_PERIOD => 'M2M',
                ]
            )
        );

        $this->create(
            'Альфа-Фреш',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                    DepositTerm::REFILLING_PERIOD => 'M2M',
                ]
            )
        );

        $this->create(
            'Альфа-Фреш',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::TWICE_PER_MONTH,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                    DepositTerm::REFILLING_PERIOD => 'M6M',
                ]
            )
        );

        $this->create(
            'Альфа-Фикс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                ]
            )
        );

        $this->create(
            'Альфа-Фикс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2M',
                    DepositTerm::RATE => 9.1,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                    DepositTerm::REFILLING_PERIOD => 'P1M',
                ]
            )
        );

        $this->create(
            'Альфа-Фикс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 10.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                    DepositTerm::REFILLING_PERIOD => 'P1M',
                ]
            )
        );

        $this->create(
            'Альфа-Фикс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 11.9,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 1200000,
                    DepositTerm::FIXED_RATE_PERIOD => 'P11M',
                    DepositTerm::REFILLING_PERIOD => 'P1M',
                ]
            )
        );

        $this->create(
            'Альфа-Актив',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 7,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::FIXED_RATE_PERIOD => 'P11M',
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                ]
            )
        );

        $this->create(
            'Альфа-Актив',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::FIXED_RATE_PERIOD => 'P11M',
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                ]
            )
        );

        $this->create(
            'Альфа-Клик',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 100000,
                    DepositTerm::PROLONGATION => 10,
                ]
            )
        );

        $this->create(
            'Insync',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 7.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 100000,
                    DepositTerm::PROLONGATION => 10,
                ]
            )
        );

        $this->create(
            'Insync',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 100000,
                    DepositTerm::PROLONGATION => 10,
                    DepositTerm::REFILLING_PERIOD => 'P2M'
                ]
            )
        );

        /* БНБ */
        $this->create(
            'Рублю, Ergo Sum',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P33D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Рублю, Ergo Sum',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Рублю, Ergo Sum',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Верное решение',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Верное решение',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Верное решение',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Вклад в будущее',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                ]
            )
        );

        $this->create(
            'Вклад в будущее',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::ONCE,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                ]
            )
        );

        $this->create(
            'Вклад в будущее',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                ]
            )
        );

        $this->create(
            'Вклад в будущее',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::ONCE,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                ]
            )
        );

        $this->create(
            'Вклад в будущее',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P14M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                ]
            )
        );

        $this->create(
            'Вклад в будущее',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::ONCE,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P14M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                ]
            )
        );

        $this->create(
            'До востребования БНБ-Банк',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => '',
                    DepositTerm::RATE => 2.6,
                    DepositTerm::MIN_SUM => 500,
                ]
            )
        );
    }


}
