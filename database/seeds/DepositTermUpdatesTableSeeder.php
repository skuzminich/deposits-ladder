<?php /** @noinspection PhpUndefinedClassInspection */

/** @noinspection PhpUndefinedClassInspection */

use Illuminate\Database\Seeder;
use \App\DepositTerm;
use \App\DepositTermUpdate;

/**
 * Class DepositTermUpdatesTableSeeder
 */
class DepositTermUpdatesTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deposit_term_updates')->insert(
            [
                DepositTermUpdate::DEPOSIT_TERM_ID => 1,
                DepositTermUpdate::INFORM_DATE => '2018-08-08',
                DepositTermUpdate::START_DATE => '2018-08-10',
                DepositTermUpdate::DEPOSIT_FROM_DATE => '2018-01-01',
                DepositTermUpdate::DEPOSIT_TO_DATE => '2018-02-01',
                DepositTerm::CALCULATION_OF_INTEREST_PERIOD => 0,
                DepositTerm::CAPITALIZATION_ALLOWED => 0,
                DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => 0,
                DepositTerm::RATE_TYPE => 0,
                DepositTerm::RATE => 10,
                DepositTerm::REFILLING_TYPE => 0,
                DepositTerm::PROLONGATION => 0,
                DepositTerm::MIN_SUM => 0,
            ]
        );

        DB::table('deposit_term_updates')->insert(
            [
                DepositTermUpdate::DEPOSIT_TERM_ID => 2,
                DepositTermUpdate::INFORM_DATE => '2018-07-07',
                DepositTermUpdate::START_DATE => '2018-07-08',
                DepositTermUpdate::DEPOSIT_FROM_DATE => '2018-02-01',
                DepositTermUpdate::DEPOSIT_TO_DATE => '2018-03-01',
                DepositTerm::CALCULATION_OF_INTEREST_PERIOD => 1,
                DepositTerm::CAPITALIZATION_ALLOWED => 1,
                DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => 1,
                DepositTerm::RATE_TYPE => 1,
                DepositTerm::RATE => 20,
                DepositTerm::REFILLING_TYPE => 2,
                DepositTerm::PROLONGATION => 3,
                DepositTerm::MIN_SUM => 100,
                DepositTerm::IRREDUCIBLE_BALANCE => 100,
                DepositTerm::FULL_REVOKE_CONDITIONS => 'never',
                DepositTerm::FULL_REVOKE_PERCENT => 2.5,
                DepositTerm::PARTIAL_REVOKE_CONDITIONS => 'no no no',
                DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT => 5,
                DepositTerm::FIXED_RATE_PERIOD => 'P6M',
                DepositTerm::REFILLING_PERIOD => 'P6M',
                DepositTerm::REFILLING_MIN_SUM => 50,
                DepositTerm::PROLONGATION_CONDITIONS => 'bla bla bla',
                DepositTerm::MAX_SUM => 1000000000,
                DepositTerm::THIRD_PERSON => 1,
                DepositTerm::LINK => 'http://tut.by',
                DepositTerm::RESTRICTION => 'no no no',
            ]
        );

        DB::table('deposit_term_updates')->insert(
            [
                DepositTermUpdate::INFORM_DATE => '2018-06-06',
                DepositTermUpdate::START_DATE => '2018-06-06',
                DepositTermUpdate::DEPOSIT_TERM_ID => 3,
                DepositTermUpdate::DEPOSIT_FROM_DATE => '2018-03-01',
                DepositTermUpdate::DEPOSIT_TO_DATE => '2018-04-01',
                DepositTerm::CALCULATION_OF_INTEREST_PERIOD => 3,
                DepositTerm::CAPITALIZATION_ALLOWED => 1,
                DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => 0,
                DepositTerm::RATE_TYPE => 3,
                DepositTerm::RATE => 0.7,
                DepositTerm::REFILLING_TYPE => 1,
                DepositTerm::PROLONGATION => 0,
                DepositTerm::MIN_SUM => 30,
            ]
        );

        DB::table('deposit_term_updates')->insert(
            [
                DepositTermUpdate::DEPOSIT_TERM_ID => 1,
                DepositTermUpdate::INFORM_DATE => '2018-05-05',
                DepositTermUpdate::START_DATE => '2018-05-05',
                DepositTermUpdate::DEPOSIT_FROM_DATE => '2018-04-01',
                DepositTermUpdate::DEPOSIT_TO_DATE => '2018-05-01',
                DepositTerm::RATE => 3,
            ]
        );

        /** @var \App\Deposit $onDeposit */
        $onDeposit     = \App\Deposit::where(\App\Deposit::NAME,'ON вклад')->get()->first();
        $onDepositTerm = $onDeposit->depositTerms->where(DepositTerm::PERIOD, 'P2M   ')->first();
        $onUpdates     = [
            '2017-09-07 00:00:00' => 9,
            '2017-11-11 00:00:00' => 10.4,
            '2017-12-06 00:00:00' => 9.6,
            '2018-02-15 00:00:00' => 9.4,
            '2018-07-04 00:00:00' => 9.2,
        ];

        $this->_generateUpdates($onDepositTerm, $onUpdates);

        /** @var \App\Deposit $onDeposit */
        $openDeposit     = \App\Deposit::where(\App\Deposit::NAME,'Открытие')->get()->first();
        $openDepositTerm = $openDeposit->depositTerms->where(DepositTerm::NAME, 'Открытие Online')->first();
        $openUpdates     = [
            '2017-09-26 00:00:00' => 8.5,
            '2017-10-18 00:00:00' => 9.5,
            '2017-11-21 00:00:00' => 10.4,
            '2017-12-06 00:00:00' => 9.6,
            '2018-02-15 00:00:00' => 9.4,
            '2018-07-04 00:00:00' => 9.2,
        ];

        $this->_generateUpdates($openDepositTerm, $openUpdates);
    }


    /**
     * @param DepositTerm $term
     * @param array $updates
     */
    private function _generateUpdates(DepositTerm $term, array $updates) : void
    {
        foreach ($updates as $date => $rate) {
            $onUpdate = new DepositTermUpdate();
            $onUpdate->setAttribute(DepositTermUpdate::INFORM_DATE, $date);
            $onUpdate->setAttribute(DepositTermUpdate::START_DATE, $date);
            $onUpdate->setAttribute(DepositTerm::RATE, $rate);

            $onUpdate->depositTerm()->associate($term);

            $onUpdate->save();

            $startDate = \DateTime::createFromFormat('Y-m-d h:i:s', $date)->setTime(0,0);
            /** @var \App\DepositTerm $term */
            $term = DepositTerm::findOrFail($term->id);
            $term->setAttribute(DepositTerm::RATE, $rate);

            $term->setAttribute(\App\DepositTerm::UPDATED_AT, $startDate);
            $term->save();
        }
    }


}