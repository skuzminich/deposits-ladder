<?php /** @noinspection PhpUndefinedClassInspection */

use App\Bank;
use Illuminate\Database\Seeder;
use \App\Deposit;

/**
 * Class DepositsTableSeeder
 */
class DepositsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        /* Белагпропромбанк */
        $this->_create(
            'Линия роста 3.0',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/liniya-rosta-3-0-do-goda/'
        );

        $this->_create(
            'Стандарт',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/srochnyj-bezotzyvnyj-bankovskij-vklad-depozit-standart/'
        );

        $this->_create(
            '@gro',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/crochnyj-bankovskij-vklad-depozit-gro/'
        );

        $this->_create(
            'Депозитная карта',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/srochnyj-otzyvnyj-bankovskij-vklad-depozit-depozitnaya-karta/'
        );

        $this->_create(
            'Максимум',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/srochnyj-bezotzyvnyj-vklad-maksimum/'
        );

        $this->_create(
            'Забота',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/zabota/',
            'Вклад могут открыть только владельцы пакета "Забота" по месту открытия пенсионного счета "Забота".'
        );

        $this->_create(
            'Пенсионный',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/pensionnyi/',
            'Вклад "Пенсионный" можно открыть для зачисления и хранения денежных средств, поступающих от государственного органа,
            осуществляющего перечисление пенсий, пособий, иных выплат по государственному социальному страхованию.'
        );

        $this->_create(
            'До востребования Белагропромбанк',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/restante/'
        );

        $this->_create(
            'Максимум Плюс',
            'Белагропромбанк',
            'http://www.belapb.by/rus/natural/deposit/srochnyj-bezotzyvnyj-vklad-depozit-maksimum-plyus/',
            'Открытие вклада (депозита) только безналичным перечислением денежных средств, при предъявлении к погашению облигаций
            ОАО «Белагропромбанк» 209-213 выпусков.'
        );

        /* Беларусбанк */
        $this->_create(
            'Классик',
            'Беларусбанк'
        );

        $this->_create(
            'Весенний (версия 2.0)',
            'Беларусбанк',
            'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33708'
        );

        $this->_create(
            'Тренд',
            'Беларусбанк',
            'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32026'
        );

        $this->_create(
            'Свободный выбор',
            'Беларусбанк',
            'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31947',
            'Комплексный продукт https://belarusbank.by/ru/fizicheskim_licam/31919/32116'
        );

        $this->_create(
            'Классик Почтовый',
            'Беларусбанк',
            'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31528',
            'Вклад открывается ТОЛЬКО в структурных подразделениях РУП «Белпочта», которым делегированы права от имени
            ОАО «АСБ Беларусбанк», осуществлять прием денежных средств во вклады.'
        );

        $this->_create(
            'Попечение',
            'Беларусбанк',
            'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/28490',
            'Вклад на условиях до востребования для несовершеннолетних относящегося к категории детей-сирот и/или детей,
            оставшихся без попечения родителей, открывается только опекуном (попечителем), лицом,
            на которое законодательством возложено выполнение соответствующих обязанностей'
        );

        $this->_create(
            'До востребования Беларусбанк',
            'Беларусбанк',
            'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/28491'
        );

        /* Белвэб */
        $this->_create(
            'Сберегательный вкл@д',
            'Банк БелВЭБ',
            'https://www.belveb.by/individual/depozity-i-scheta/depozity-online/sberegatelnyy-vkl-d/'
        );

        $this->_create(
            'Зручны',
            'Банк БелВЭБ',
            'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/zruchny-depazit/'
        );

        $this->_create(
            'Интернет-вкл@д',
            'Банк БелВЭБ',
            'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/internet-vkl-d/'
        );

        $this->_create(
            'Престижный',
            'Банк БелВЭБ',
            'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/prestizhnyy/'
        );

        $this->_create(
            'Выдатны',
            'Банк БелВЭБ',
            'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/vydatny/'
        );

        $this->_create(
            'Универсальный',
            'Банк БелВЭБ',
            'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/universalnyy-s-plavayushchey-protsentnoy-stavkoy/'
        );

        $this->_create(
            'Персональный',
            'Банк БелВЭБ',
            'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/personalnyy/'
        );

        /* Белгазпромбанк */
        $this->_create(
            'ON вклад',
            'Белгазпромбанк',
            'http://belgazprombank.by/personal_banking/vkladi_depoziti/internet_vkladi/v_bel_rub/'
        );

        $this->_create(
            'Спринт',
            'Белгазпромбанк',
            'http://belgazprombank.by/personal_banking/vkladi_depoziti/v_nacional_noj_valjute/vklad-na-45-dney-bezotzyvnyy-sprint/'
        );

        $this->_create(
            'Отличный',
            'Белгазпромбанк',
            'http://belgazprombank.by/personal_banking/vkladi_depoziti/v_nacional_noj_valjute/vklad-na-7-mesyatsev-bezotzyvnyy-otlichnyy-byr/'
        );

        $this->_create(
            'Накопительный',
            'Белгазпромбанк',
            'http://belgazprombank.by/personal_banking/vkladi_depoziti/v_nacional_noj_valjute/vklad-na-god-otzyvnyy-nakopitelnyy/'
        );

        $this->_create(
            'Прогрессивный',
            'Белгазпромбанк',
            'http://belgazprombank.by/personal_banking/vkladi_depoziti/v_nacional_noj_valjute/vklad-bezotzyvnyy-progressivnyy/'
        );

        $this->_create(
            'Базовый',
            'Белгазпромбанк',
            'http://belgazprombank.by/personal_banking/vkladi_depoziti/v_nacional_noj_valjute/na-3-mesyatsa-bezotzyvnyy-bazovyy/'
        );

        $this->_create(
            'Выгодный',
            'Белгазпромбанк',
            'http://belgazprombank.by/personal_banking/vkladi_depoziti/v_nacional_noj_valjute/vklad-na-3-goda-otzyvnyy-vygodnyy/'
        );

        /* Белинвестбанк */
        $this->_create(
            'Личный выбор',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/Lichnyj-vybor-v-naczional'
        );

        $this->_create(
            'Накопительный плюс',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/Nakopitel'
        );

        $this->_create(
            'Пенсионный',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/Pensionnyj-v-BYN-bezotzyvnyy',
            'Пенсионерам'
        );

        $this->_create(
            'Срочный',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/Srochnyj-v-naczional'
        );

        $this->_create(
            'Отличный плюс',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/Otlichnyj-plyus-v-naczional'
        );

        $this->_create(
            'Активный',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/Aktivnyj-v-naczional'
        );

        $this->_create(
            'BIBcoin',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/BIBcoin-v-naczional'
        );

        $this->_create(
            'С правом востребования',
            'Белинвестбанк',
            'https://www.belinvestbank.by/individual/deposit/S-pravom-vostrebovaniya-v-naczional'
        );

        /* БТА */
        $this->_create(
            'Открытие',
            'БТА Банк',
            'http://www.btabank.by/ru/block/2335'
        );

        $this->_create(
            'БТА Мини',
            'БТА Банк',
            'http://www.btabank.by/ru/block/2238'
        );

        $this->_create(
            'БТА Максима',
            'БТА Банк',
            'http://www.btabank.by/ru/page/private/1550'
        );

        $this->_create(
            'БТА Копилка',
            'БТА Банк',
            'http://www.btabank.by/ru/page/private/2262'
        );

        $this->_create(
            'БТА Рекордный',
            'БТА Банк',
            'http://www.btabank.by/ru/page/private/2265'
        );

        $this->_create(
            'БТА Сбережения',
            'БТА Банк',
            'http://www.btabank.by/ru/page/private/2267'
        );

        $this->_create(
            'БТА Профит',
            'БТА Банк',
            'http://www.btabank.by/ru/page/1553'
        );

        $this->_create(
            'БТА +',
            'БТА Банк',
            'http://www.btabank.by/ru/block/2410'
        );

        /* Идея */
        $this->_create(
            'Базовый',
            'Идея Банк'
        );

        /* Идея */
        $this->_create(
            'Онлайн',
            'Идея Банк'
        );

        $this->_create(
            'Фирменный',
            'Идея Банк',
            null,
            'Вклад можно открыть, если вы являетесь держателем фирменного продукта Банка
                    или размещаете вклад в Банке в первый раз. Фирменные продукты:  "PartyCard", "PartyCard [on air]", "Ещё",
                    "Ещё [on air]", "На здоровье", "Лучший курс", "Prime", любая карта "CREDO".'
        );

        $this->_create(
            'Валютный',
            'Идея Банк'
        );

        $this->_create(
            'Валютный фирменный',
            'Идея Банк',
            null,
            'Вклад можно открыть, если вы являетесь держателем фирменного продукта Банка
                    или размещаете вклад в Банке в первый раз. Фирменные продукты:  "PartyCard", "PartyCard [on air]", "Ещё",
                    "Ещё [on air]", "На здоровье", "Лучший курс", "Prime", любая карта "CREDO".'
        );

        /* Москва Минск */
        $this->_create(
            'Прогрессивный',
            'Банк Москва-Минск',
            'https://www.mmbank.by/personal/deposites/vklad_progressivnij/'
        );

        $this->_create(
            'Капитальный',
            'Банк Москва-Минск',
            'https://www.mmbank.by/personal/deposites/vklad_kapital_nij/'
        );

        $this->_create(
            'Оптимальный',
            'Банк Москва-Минск',
            'https://www.mmbank.by/personal/deposites/vklad_optimal_nij/'
        );

        $this->_create(
            'Основательный',
            'Банк Москва-Минск',
            'https://www.mmbank.by/personal/deposites/vklad_osnovatel_nij/'
        );

        $this->_create(
            'Весомый',
            'Банк Москва-Минск',
            'https://www.mmbank.by/personal/deposites/vklad_vesomij/'
        );

        /* МТБанк */
        $this->_create(
            'МТБелки',
            'МТБанк',
            'https://www.mtbank.by/private/deposits/br/mtbelki'
        );

        $this->_create(
            'Актуальный',
            'МТБанк',
            'https://www.mtbank.by/private/deposits/fc'
        );

        /** Решение */
        $this->_create(
            'Формула успеха',
            'Банк Решение',
            'https://rbank.by/life/deposits/rouble/vklad_formula_uspekha/'
        );

        $this->_create(
            'Online-решение',
            'Банк Решение',
            'https://rbank.by/life/deposits/rouble/vklad_online_reshenie/'
        );

        $this->_create(
            'Online-решение new',
            'Банк Решение',
            'https://rbank.by/life/deposits/rouble/vklad_online_reshenie_new/'
        );

        $this->_create(
            'Свободное накопление',
            'Банк Решение',
            'https://rbank.by/life/deposits/rouble/vklad_svobodnoe_nakoplenie/'
        );

        $this->_create(
            'Спринт-депозит new',
            'Банк Решение',
            'https://rbank.by/life/deposits/rouble/vklad_sprint_depozit_new/'
        );

        $this->_create(
            'Твой ход',
            'Банк Решение',
            'https://rbank.by/life/deposits/rouble/vklad_reshayte_sami/'
        );

        /* Франсабанк */
        $this->_create(
            'Результативный',
            'Франсабанк',
            'https://fransabank.by/chastnym-klientam/vklady/tekushchie-vklady/vklad-rezultativnyy/'
        );

        $this->_create(
            'Верный рассчет',
            'Франсабанк',
            'https://fransabank.by/chastnym-klientam/vklady/tekushchie-vklady/vklad-vernyy-raschet/'
        );

        $this->_create(
            'Верный рассчет плюс',
            'Франсабанк',
            'https://fransabank.by/chastnym-klientam/vklady/tekushchie-vklady/vklad-vernyy-raschet-plyus/'
        );

        /* Альфа */
        $this->_create(
            'Альфа-Ракета',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/rocket/'
        );

        $this->_create(
            'Альфа-Фреш',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/fresh/'
        );

        $this->_create(
            'Альфа-Фикс',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/alfa-prime/'
        );

        $this->_create(
            'Альфа-Актив',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/alfa-activ/'
        );

        $this->_create(
            'Альфа-Клик',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/insync/#tarifs',
            'Вклад доступен клиентам в мобильном банке INSYNC.BY'
        );

        $this->_create(
            'Insync',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/insync/#tarifs'
        );

        $this->_create(
            'Альфа-Профит',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/alfa-comfort/'
        );

        $this->_create(
            'Сезоны',
            'Альфа-Банк',
            'https://www.alfabank.by/saving/deposits/alfa-vesna/'
        );

        /* БНБ */
        $this->_create(
            'Рублю, Ergo Sum',
            'БНБ-Банк',
            'https://www.bnb.by/o-lichnom/sberezhenie/bezotzyvnye/rublyu-ergo-sum/'
        );

        $this->_create(
            'Благо семьи',
            'БНБ-Банк',
            'https://www.bnb.by/o-lichnom/sberezhenie/otzyvnye/blago-semi/'
        );

        $this->_create(
            'Верное решение',
            'БНБ-Банк',
            'https://www.bnb.by/o-lichnom/sberezhenie/bezotzyvnye/vernoe-reshenie/'
        );

        $this->_create(
            'Вклад в будущее',
            'БНБ-Банк',
            'https://www.bnb.by/o-lichnom/sberezhenie/bezotzyvnye/vklad-v-budushchee/'
        );

        $this->_create(
            'Лучшее будущее',
            'БНБ-Банк',
            'https://www.bnb.by/o-lichnom/sberezhenie/bezotzyvnye/luchshee-budushchee/'
        );

        $this->_create(
            'До востребования БНБ-Банк',
            'БНБ-Банк',
            'https://www.bnb.by/o-lichnom/sberezhenie/do-vostrebovaniya/do-vostrebovaniya/'
        );

        /* БПС */
        $this->_create(
            'Сохраняй',
            'БПС-Сбербанк',
            'https://www.bps-sberbank.by/deposit/save-online-BYN-unrecall/BYN/attributes'
        );

        $this->_create(
            'Приумножай',
            'БПС-Сбербанк',
            'https://www.bps-sberbank.by/deposit/multiply-ofline-BYN/BYN/attributes'
        );

        $this->_create(
            'Доверяй',
            'БПС-Сбербанк',
            'https://www.bps-sberbank.by/deposit/trust-online-BYN-recall-urgent/BYN/attributes'
        );

        $this->_create(
            'Онлайн-1',
            'БПС-Сбербанк',
            'https://www.bps-sberbank.by/deposit/control-online-1-BYN/BYN/attributes'
        );

        $this->_create(
            'Управляй Онлайн',
            'БПС-Сбербанк',
            'https://www.bps-sberbank.by/deposit/control-online-recall/BYN/attributes'
        );

        /* ВТБ */
        $this->_create(
            'Удобный',
            'Банк ВТБ',
            'https://www.vtb-bank.by/chastnym-licam/vklady/vklad-depozit-udobnyy'
        );

        $this->_create(
            'Верный курс',
            'Банк ВТБ',
            'https://www.vtb-bank.by/chastnym-licam/vklady/vklad-depozit-vernyy-kurs',
            'Вклад может быть внесен только в безналичном порядке со счета вкладчика, открытого в банке в долларах США,
            путем банковского перевода с покупкой Банком долларов США по установленному курсу'
        );

        $this->_create(
            'Окно возможностей',
            'Банк ВТБ',
            'https://www.vtb-bank.by/chastnym-licam/vklady/vklad-depozit-okno-vozmozhnostey'
        );

        /* Приор */
        $this->_create(
            'Выше.net',
            'Приорбанк',
            'https://www.priorbank.by/electronic-deposit'
        );

        $this->_create(
            'Пять звёзд',
            'Приорбанк',
            'https://www.priorbank.by/5-zvezd'
        );

        $this->_create(
            'Горячие деньги',
            'Приорбанк',
            'https://www.priorbank.by/goryachie-dengi'
        );

        /* Статус */
        $this->_create(
            'Расчетливые бабки',
            'Статусбанк',
            'https://stbank.by/private_client/deposits/irrevocable/raschetlivye-babki/'
        );

        $this->_create(
            'Старательные бабки',
            'Статусбанк',
            'https://stbank.by/private_client/deposits/irrevocable/staratelnye-babki/'
        );

        $this->_create(
            'Бережливые бабки',
            'Статусбанк',
            'https://stbank.by/private_client/deposits/irrevocable/berezhlivye-babki/'
        );

        $this->_create(
            'Иностранные бабки',
            'Статусбанк',
            'https://stbank.by/private_client/deposits/irrevocable/inostrannye-babki/'
        );

        $this->_create(
            'Запасливые бабки',
            'Статусбанк',
            'https://stbank.by/private_client/deposits/revocable/zapaslivye-babki/'
        );

        $this->_create(
            'Валютные бабки',
            'Статусбанк',
            'https://stbank.by/private_client/deposits/revocable/valyutnye-babki/'
        );

        /* Паритет */
        $this->_create(
            'Стабильный доход',
            'Паритетбанк'
        );

        $this->_create(
            'Привлекательный',
            'Паритетбанк'
        );

        $this->_create(
            'Оптимальный',
            'Паритетбанк',
            'http://www.paritetbank.by/services/private/deposit/optimal/'
        );

        $this->_create(
            'Капитал',
            'Паритетбанк',
            'http://www.paritetbank.by/services/private/deposit/capital/'
        );

        /* РРБ */
        $this->_create(
            'Безотзывный',
            'РРБ-Банк',
            'https://rrb.by/fizicheskim-licam/vkladi/vkladi-v-nacionalnoi-valyute'
        );

        $this->_create(
            'Infinite',
            'РРБ-Банк',
            'https://rrb.by/fizicheskim-licam/vkladi/vkladi-v-nacionalnoi-valyute'
        );

        $this->_create(
            'Infinite Плюс',
            'РРБ-Банк',
            'https://rrb.by/fizicheskim-licam/vkladi/vkladi-v-nacionalnoi-valyute'
        );

        $this->_create(
            'Национальный',
            'РРБ-Банк',
            'https://rrb.by/fizicheskim-licam/vkladi/vkladi-v-nacionalnoi-valyute'
        );

        $this->_create(
            'Европейский лояльный',
            'РРБ-Банк',
            'https://rrb.by/fizicheskim-licam/vkladi/vkladi-v-inostrannoi-valyute'
        );

        $this->_create(
            'Отзывный',
            'РРБ-Банк',
            'https://rrb.by/fizicheskim-licam/vkladi/vkladi-v-inostrannoi-valyute'
        );

        $this->_create(
            'Free-card',
            'РРБ-Банк',
            'https://rrb.by/fizicheskim-licam/vkladi/vkladi-v-inostrannoi-valyute'
        );

        /* Технобанк */
        $this->_create(
            'Максимум',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/deposits/product/maximum/'
        );

        $this->_create(
            'Новая вершина',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/deposits/product/apex-bel/'
        );

        $this->_create(
            'ТРИумф',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/deposits/product/trio/'
        );

        $this->_create(
            'Новый',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/deposits/product/new-usd/'
        );

        $this->_create(
            'Наше будущее',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/deposits/product/our-future-usd/'
        );

        $this->_create(
            'Отличный',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/deposits/product/excellent-usd/'
        );

        $this->_create(
            'Выгодный',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/deposits/product/profitable/'
        );

        $this->_create(
            'е-Соло',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/internet-deposits/product/e-solo/'
        );

        $this->_create(
            'е-Дуэт',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/internet-deposits/product/e-duet/'
        );

        $this->_create(
            'е-Успешный',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/internet-deposits/product/e-successful/'
        );

        $this->_create(
            'е-Спринт',
            'Технобанк',
            'https://tb.by/individuals/deposits-savings/internet-deposits/product/e-sprint/'
        );

        /* Цептер */
        $this->_create(
            'Цептер Аккорд',
            'Цептер Банк',
            'https://www.zepterbank.by/personal/deposits/accord/'
        );

        $this->_create(
            'Цептер Этюд',
            'Цептер Банк',
            'https://www.zepterbank.by/personal/deposits/etud/'
        );

        $this->_create(
            'Цептер Соната',
            'Цептер Банк',
            'https://www.zepterbank.by/personal/deposits/sonata/'
        );
    }


    /**
     * @param string $name
     * @param string $bank
     * @param string $link
     * @param string $restriction
     */
    private function _create(string $name, string $bank, string $link = null, string $restriction = null): void
    {
        $bankId = Bank::where(Bank::SHORT_NAME, $bank)->first()->id;

        $id = DB::table('deposits')->insertGetId(
            [
                Deposit::STATUS => Deposit::STATUS_ACTIVE,
                'latest_version' => 1,
                Deposit::BANK_ID => $bankId,
                Deposit::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('deposits_version')->insert(
            [
                'ref_id' => $id,
                'version' => 1,
                Deposit::NAME => $name,
                Deposit::LINK => $link,
                Deposit::OPEN_RESTRICTION => $restriction,
            ]
        );
    }


}