<?php /** @noinspection PhpUndefinedClassInspection */

use \App\DepositTerm;

/**
 * Class DepositTermsTableSeeder
 */
class DepositTermsTableSeeder6 extends DepositTermsTableSeeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        /* БПС */
        /* ВТБ */
        /* Приор */
        /* Статус */
        /* Паритет */
        /* РРБ */
        /* Технобанк */
        /* Цептер */

        /* БПС */
        $this->create(
            'Сохраняй',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::CUSTOM_RATE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P90D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Сохраняй',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::CUSTOM_RATE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P90D',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Сохраняй',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::CUSTOM_RATE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Сохраняй',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::CUSTOM_RATE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Приумножай',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P30D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 10,
                    DepositTerm::LINK => 'https://www.bps-sberbank.by/deposit/save-online-BYN-capital/BYN/attributes',
                ]
            )
        );

        $this->create(
            'Доверяй',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_POSSIBLE,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P180D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Онлайн-1',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P30D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Управляй Онлайн',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P175D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 1000,
                    DepositTerm::MAX_SUM => 30000,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        /* ВТБ */
        $this->create(
            'Удобный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P395D',
                    DepositTerm::RATE => 1,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Верный курс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P395D',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 2000,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::REFILLING_PERIOD => 'P25D',
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P65D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P395D',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::REFILLING_PERIOD => 'P25D',
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P732D',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::REFILLING_PERIOD => 'P360D',
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::REFILLING_PERIOD => 'P720D',
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::LINK => 'https://www.vtb-bank.by/chastnym-licam/vklady/vklad-depozit-okno-vozmozhnostey-0',
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::LINK => 'https://www.vtb-bank.by/chastnym-licam/vklady/vklad-depozit-okno-vozmozhnostey-0',
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P395D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::REFILLING_PERIOD => 'P360D',
                    DepositTerm::LINK => 'https://www.vtb-bank.by/chastnym-licam/vklady/vklad-depozit-okno-vozmozhnostey-0',
                ]
            )
        );

        $this->create(
            'Окно возможностей',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::MAX_SUM => 200000,
                    DepositTerm::REFILLING_PERIOD => 'P1060D',
                    DepositTerm::LINK => 'https://www.vtb-bank.by/chastnym-licam/vklady/vklad-depozit-okno-vozmozhnostey-0',
                ]
            )
        );

        /* Приор */
        $this->create(
            'Выше.net',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 7,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Выше.net',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Выше.net',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Выше.net',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Выше.net (безотзывный)',
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::PROLONGATION => 1,
                    DepositTerm::LINK => 'https://www.priorbank.by/elektronnyj-depozit-vyse.net-bezotzyvnij-',
                ]
            )
        );

        $this->create(
            'Пять звёзд',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Пять звёзд',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Пять звёзд',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Пять звёзд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Пять звёзд. Скала',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Пять звёзд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Пять звёзд. Скала',
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Пять звёзд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Пять звёзд. Скала',
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 1,
                ]
            )
        );

        $this->create(
            'Горячие деньги',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Пять звёзд. Скала',
                    DepositTerm::PERIOD => '',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 1,
                ]
            )
        );

        /* Статус */
        $this->create(
            'Расчетливые бабки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 8.9,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 5,
                    DepositTerm::REFILLING_MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Старательные бабки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P3M',
                    DepositTerm::REFILLING_MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Бережливые бабки',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Запасливые бабки',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::FIXED_RATE_PERIOD => 'P3M',
                    DepositTerm::REFILLING_PERIOD => 'P12M',
                    DepositTerm::REFILLING_MIN_SUM => 50,
                ]
            )
        );

        /* Паритет */
        $this->create(
            'Стабильный доход',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 7,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/stabilny/'
                ]
            )
        );

        $this->create(
            'Стабильный доход',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/stabilny/'
                ]
            )
        );

        $this->create(
            'Стабильный доход',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/new_page_561/'
                ]
            )
        );

        $this->create(
            'Стабильный доход',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/new_page_561/'
                ]
            )
        );

        $this->create(
            'Стабильный доход',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 9.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/new_page_561/'
                ]
            )
        );

        $this->create(
            'Стабильный доход',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/new_page_561/'
                ]
            )
        );

        $this->create(
            'Стабильный доход',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P15M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/new_page_561/'
                ]
            )
        );

        $this->create(
            'Привлекательный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                    DepositTerm::LINK => 'http://www.paritetbank.by/services/private/deposit/privlekatelny/'
                ]
            )
        );

        $this->create(
            'Оптимальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        /* РРБ */
        $this->create(
            'Безотзывный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Безотзывный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_SCHEDULE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P395D',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Infinite',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 0.5,
                    DepositTerm::MIN_SUM => 5000,
                ]
            )
        );

        $this->create(
            'Infinite Плюс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 8.5,
                    DepositTerm::MIN_SUM => 50000,
                ]
            )
        );

        $this->create(
            'Национальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 0.5,
                    DepositTerm::MIN_SUM => 50000,
                ]
            )
        );

        /* Технобанк */
        $this->create(
            'Максимум',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 500,
                ]
            )
        );

        $this->create(
            'Новая вершина',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'ТРИумф',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'е-Соло',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 7.8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'е-Дуэт',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P60D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        /* Цептер */
        $this->create(
            'Цептер Аккорд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_MIN_SUM => 10,
                    DepositTerm::PROLONGATION => 9,
                ]
            )
        );

        $this->create(
            'Цептер Аккорд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Цептер Аккорд +',
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_MIN_SUM => 10,
                    DepositTerm::PROLONGATION => 9,
                    DepositTerm::LINK => 'https://www.zepterbank.by/personal/deposits/tsepter-akkord-plus/',
                ]
            )
        );

        $this->create(
            'Цептер Этюд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Цептер Этюд +',
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.zepterbank.by/personal/deposits/etyud-plus/',
                ]
            )
        );

        $this->create(
            'Цептер Соната',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Цептер Соната +',
                    DepositTerm::PERIOD => 'P390D',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.zepterbank.by/personal/deposits/tsepter-sonata-plus/',
                ]
            )
        );
    }


}
