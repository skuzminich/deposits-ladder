<?php /** @noinspection PhpUndefinedClassInspection */

use \App\DepositTerm;

/**
 * Class DepositTermsTableSeeder
 */
class DepositTermsTableSeeder2 extends DepositTermsTableSeeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        /* Белвэб */
        /* Белгазпромбанк */

        /* Белвэб */
        $this->create(
            'Сберегательный вкл@д',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9.0,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Сберегательный вкл@д',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Сберегательный вкл@д',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Сберегательный вкл@д',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Сберегательный вкл@д',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 4,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Зручны',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => '',
                    DepositTerm::RATE => 7,
                    DepositTerm::MIN_SUM => 0,
                ]
            )
        );

        $this->create(
            'Зручны',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Зручны анл@йн',
                    DepositTerm::PERIOD => '',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 0,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/depozit-do-vostrebovaniya-zruchny-anl-yn/',
                ]
            )
        );

        $this->create(
            'Интернет-вкл@д',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Интернет-вкл@д',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Интернет-вкл@д',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Интернет-вкл@д',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Интернет-вкл@д',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Интернет-вкл@д',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'Престижный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100000,
                ]
            )
        );

        $this->create(
            'Престижный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 4,
                    DepositTerm::MIN_SUM => 100000,
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P36M',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Выдатны-вэб',
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/vydatny-veb/',
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Выдатны-вэб',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/vydatny-veb/',
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Выдатны-вэб',
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/vydatny-veb/',
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Выдатны-вэб',
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/vydatny-veb/',
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Выдатны-вэб',
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/vydatny-veb/',
                ]
            )
        );

        $this->create(
            'Выдатны',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Выдатны-вэб',
                    DepositTerm::PERIOD => 'P36M',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/vydatny-veb/',
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::REFINANCING,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P36M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Универсальный безотзывный',
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/universalnyy-bezotzyvnyy-s-fiksirovannoy-protsentnoy-stavkoy/',
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Универсальный безотзывный',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/universalnyy-bezotzyvnyy-s-fiksirovannoy-protsentnoy-stavkoy/',
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Универсальный безотзывный',
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/universalnyy-bezotzyvnyy-s-fiksirovannoy-protsentnoy-stavkoy/',
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Универсальный безотзывный',
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 4,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/universalnyy-bezotzyvnyy-s-fiksirovannoy-protsentnoy-stavkoy/',
                ]
            )
        );

        $this->create(
            'Универсальный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Универсальный безотзывный',
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 4,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://www.belveb.by/individual/depozity-i-scheta/depozity-otkryvaemye-v-ofisakh-banka/universalnyy-bezotzyvnyy-s-fiksirovannoy-protsentnoy-stavkoy/',
                ]
            )
        );

        /* Белгазпромбанк */
        $this->create(
            'ON вклад',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'ON-Вклад v.1.12',
                    DepositTerm::PERIOD => 'P2M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P25D',
                ]
            )
        );

        $this->create(
            'ON вклад',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'ON-Вклад v.1.13',
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P25D',
                ]
            )
        );

        $this->create(
            'ON вклад',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'ON-Вклад v.1.14',
                    DepositTerm::PERIOD => 'P11M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P150D',
                ]
            )
        );

        $this->create(
            'ON вклад',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'ON-Вклад v.1.15',
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P1M',
                ]
            )
        );

        $this->create(
            'Спринт',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P45D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'P10D',
                ]
            )
        );

        $this->create(
            'Отличный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P7M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::FIXED_RATE_PERIOD => 'P90D',
                    DepositTerm::REFILLING_PERIOD => 'P25D',
                ]
            )
        );

        $this->create(
            'Накопительный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::FIXED_RATE_PERIOD => 'P90D',
                    DepositTerm::REFILLING_PERIOD => 'P11M',
                ]
            )
        );

        $this->create(
            'Прогрессивный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_SCHEDULE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P13M',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::REFILLING_PERIOD => 'P1M',
                ]
            )
        );

        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Выгодный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 200,
                    DepositTerm::FIXED_RATE_PERIOD => 'P90D',
                    DepositTerm::REFILLING_PERIOD => 'P35M',
                ]
            )
        );
    }


}
