<?php /** @noinspection PhpUndefinedClassInspection */

use App\Bank;
use App\Currency;
use App\Deposit;
use Illuminate\Database\Seeder;
use \App\DepositTerm;

/**
 * Class DepositTermsTableSeeder
 */
class DepositTermsTableSeeder extends Seeder
{

    const BYN = [DepositTerm::CURRENCY_ID => 'BYN'];
    const IRREVOCABLE = [DepositTerm::IS_IRREVOCABLE_FIELD => DepositTerm::IS_IRREVOCABLE];
    const REVOCABLE   = [DepositTerm::IS_IRREVOCABLE_FIELD => DepositTerm::IS_NOT_IRREVOCABLE];
    const MONTHLY     = [DepositTerm::CALCULATION_OF_INTEREST_PERIOD => DepositTerm::CALCULATION_MONTHLY];
    const WEEKLY      = [DepositTerm::CALCULATION_OF_INTEREST_PERIOD => DepositTerm::CALCULATION_WEEKLY];
    const TWICE_PER_MONTH = [DepositTerm::CALCULATION_OF_INTEREST_PERIOD => DepositTerm::CALCULATION_TWICE_PER_MONTH];
    const ONCE        = [DepositTerm::CALCULATION_OF_INTEREST_PERIOD => DepositTerm::CALCULATION_ONCE];
    const FIXED       = [DepositTerm::RATE_TYPE => DepositTerm::RATE_FIXED];
    const VARIABLE    = [DepositTerm::RATE_TYPE => DepositTerm::RATE_VARIABLE];
    const OVERNIGHT   = [DepositTerm::RATE_TYPE => DepositTerm::RATE_OVERNIGHT];
    const REFINANCING = [DepositTerm::RATE_TYPE => DepositTerm::RATE_REFINANCING_RATE];
    const FIXED_SCHEDULE    = [DepositTerm::RATE_TYPE => DepositTerm::RATE_FIXED_SCHEDULE];
    const FIXED_PERIOD      = [DepositTerm::RATE_TYPE => DepositTerm::RATE_FIXED_PERIOD];
    const CUSTOM_RATE       = [DepositTerm::RATE_TYPE => DepositTerm::RATE_CUSTOM];
    const NO_INTERNET       = [DepositTerm::INTERNET => DepositTerm::INTERNET_NO];
    const INTERNET_POSSIBLE = [DepositTerm::INTERNET => DepositTerm::INTERNET_POSSIBLE];
    const INTERNET_ONLY     = [DepositTerm::INTERNET => DepositTerm::INTERNET_ONLY];
    const CAPITALIZATION    = [DepositTerm::CAPITALIZATION_ALLOWED => DepositTerm::CAPITALIZATION_ENABLED];
    const NO_CAPITALIZATION = [DepositTerm::CAPITALIZATION_ALLOWED => DepositTerm::CAPITALIZATION_DISABLED];
    const REFILLING        = [DepositTerm::REFILLING_TYPE => DepositTerm::REFILLING_ENABLED];
    const NO_REFILLING     = [DepositTerm::REFILLING_TYPE => DepositTerm::REFILLING_DISABLED];
    const REFILLING_PERIOD = [DepositTerm::REFILLING_TYPE => DepositTerm::REFILLING_ENABLED_PERIOD];
    const THIRD_PERSON     = [DepositTerm::THIRD_PERSON => DepositTerm::THIRD_PERSON_ALLOWED];
    const NO_THIRD_PERSON  = [DepositTerm::THIRD_PERSON => DepositTerm::THIRD_PERSON_DISALLOWED];
    const NO_PERCENT_WITHDRAWAL = [DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => DepositTerm::PERCENTS_WITHDRAWAL_ENABLED];
    const OTHERS = [
        DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => DepositTerm::PERCENTS_WITHDRAWAL_ENABLED,
        DepositTerm::PROLONGATION => 0,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        /* Белагпропромбанк */
        /* Беларусбанк */
        /* Белагпропромбанк */
        $this->create(
            'Стандарт',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 6,
                    DepositTerm::MIN_SUM => 50
                ]
            )
        );

        $this->create(
            'Стандарт',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 7,
                    DepositTerm::MIN_SUM => 50
                ]
            )
        );

        $this->create(
            'Стандарт',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 9.1,
                    DepositTerm::MIN_SUM => 50
                ]
            )
        );

        $this->create(
            'Стандарт',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 9.3,
                    DepositTerm::MIN_SUM => 50
                ]
            )
        );

        $this->create(
            'Стандарт',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 10.2,
                    DepositTerm::MIN_SUM => 50
                ]
            )
        );

        $this->create(
            'Стандарт',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P735D',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'P365D'
                ]
            )
        );

        $this->create(
            'Стандарт',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1110D',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'P740D'
                ]
            )
        );

        $this->create(
            'Линия роста 3.0',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 4.0,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::PROLONGATION => 8,
                ]
            )
        );

        $this->create(
            'Линия роста 3.0',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 7.8,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'P60D',
                    DepositTerm::PROLONGATION => 2,
                ]
            )
        );

        $this->create(
            'Линия роста 3.0',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 7.9,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'P150D',
                ]
            )
        );

        $this->create(
            'Линия роста 3.0',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P280D',
                    DepositTerm::RATE => 7.9,
                    DepositTerm::MIN_SUM => 10,
                    DepositTerm::REFILLING_PERIOD => 'P245D',
                ]
            )
        );

        $this->create(
            '@gro',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 5.0,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            '@gro',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 7.9,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'P60D',
                ]
            )
        );

        $this->create(
            '@gro',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 9.0,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'P150D',
                ]
            )
        );

        $this->create(
            '@gro',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 8.0,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'P335D',
                ]
            )
        );

        $this->create(
            'Депозитная карта',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::CUSTOM_RATE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P360D',
                    DepositTerm::RATE => 4.0,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Максимум',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 11.5,
                    DepositTerm::MIN_SUM => 1000,
                    DepositTerm::FIXED_RATE_PERIOD => 'P95D',
                ]
            )
        );

        $this->create(
            'До востребования Белагропромбанк',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => '',
                    DepositTerm::RATE => 0.1,
                    DepositTerm::MIN_SUM => 0,
                ]
            )
        );

        /* Беларусбанк */
        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный Удобный',
                    DepositTerm::PERIOD => 'P230D',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 300,
                    DepositTerm::REFILLING_PERIOD => 'P30D',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33882',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Классик Безотзывный Яркий',
                    DepositTerm::PERIOD => 'P45D',
                    DepositTerm::RATE => 9.18,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33881',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Классик Безотзывный Весенний',
                    DepositTerm::PERIOD => 'P195D',
                    DepositTerm::RATE => 9.7,
                    DepositTerm::MIN_SUM => 500,
                    DepositTerm::FIXED_RATE_PERIOD => 'P3M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33521',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Интернет-депозит Безотзывный Весенний',
                    DepositTerm::PERIOD => 'P195D',
                    DepositTerm::RATE => 9.8,
                    DepositTerm::MIN_SUM => 500,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33522',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Классик Безотзывный на 1 год',
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 7.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32617',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный до года',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 3.2,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31476',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный до года',
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 5.2,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31476',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный до года',
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 6.9,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31476',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный до года',
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 7.9,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31476',
                    DepositTerm::RESTRICTION => 'В рамках Клуба "Бархат"',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный свыше года',
                    DepositTerm::PERIOD => 'P18M',
                    DepositTerm::RATE => 9.7,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::REFILLING_PERIOD => 'P15M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31484',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный свыше года',
                    DepositTerm::PERIOD => 'P24M',
                    DepositTerm::RATE => 10.2,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::REFILLING_PERIOD => 'P21M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31484',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный Детский',
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 200,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33405',
                    DepositTerm::RESTRICTION => 'Вклад принимается на имя несовершеннолетнего в возрасте до 18 лет от лиц
                    независимо от родственных отношений либо в установленных законодательством случаях от самого несовершеннолетнего.',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный Детский',
                    DepositTerm::PERIOD => 'P4Y',
                    DepositTerm::RATE => 10.4,
                    DepositTerm::MIN_SUM => 200,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33405',
                    DepositTerm::RESTRICTION => 'Вклад принимается на имя несовершеннолетнего в возрасте до 18 лет от лиц
                    независимо от родственных отношений либо в установленных законодательством случаях от самого несовершеннолетнего.',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Безотзывный Детский',
                    DepositTerm::PERIOD => 'P5Y',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 200,
                    DepositTerm::REFILLING_PERIOD => 'M3M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33405',
                    DepositTerm::RESTRICTION => 'Вклад принимается на имя несовершеннолетнего в возрасте до 18 лет от лиц
                    независимо от родственных отношений либо в установленных законодательством случаях от самого несовершеннолетнего.',
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Классик Отзывный на 35 дней',
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 1,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31701',
                    DepositTerm::PROLONGATION => 6,
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Отзывный до года',
                    DepositTerm::PERIOD => 'P95D',
                    DepositTerm::RATE => 1.5,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31483',
                    DepositTerm::PROLONGATION => 2,
                ]
            )
        );

        $this->create(
            'Классик',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Классик Отзывный до года',
                    DepositTerm::PERIOD => 'P285D',
                    DepositTerm::RATE => 2.5,
                    DepositTerm::MIN_SUM => 150,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31483',
                ]
            )
        );

        $this->create(
            'Весенний (версия 2.0)',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::PERIOD => 'P105D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 500,
                    DepositTerm::REFILLING_PERIOD => 'P1M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33708',
                ]
            )
        );

        $this->create(
            'Весенний (версия 2.0)',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Интернет-депозит Весенний (версия 2.0)',
                    DepositTerm::PERIOD => 'P105D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 500,
                    DepositTerm::REFILLING_PERIOD => 'P1M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/33709',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Отзывный',
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 2,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32026',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Отзывный',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 3,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32026',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Отзывный',
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32026',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Отзывный',
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 6,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32026',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Безотзывный',
                    DepositTerm::PERIOD => 'P1M',
                    DepositTerm::RATE => 3.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32027',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Безотзывный',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32027',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Безотзывный',
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 6,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M1M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32027',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Безотзывный',
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 7.5,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M6M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32027',
                ]
            )
        );

        $this->create(
            'Тренд',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Интернет-депозит Тренд Безотзывный',
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 8,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::REFILLING_PERIOD => 'M6M',
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/32027',
                    DepositTerm::RESTRICTION => 'В рамках Клубов "Карт-бланш" и #настарт',
                ]
            )
        );

        $this->create(
            'Свободный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P11M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 500,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31947',
                ]
            )
        );

        $this->create(
            'Классик Почтовый',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Классик Почтовый Безотзывный',
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 7.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31529',
                ]
            )
        );

        $this->create(
            'Классик Почтовый',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::NO_REFILLING,
                self::ONCE,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                self::NO_PERCENT_WITHDRAWAL,
                [
                    DepositTerm::NAME => 'Классик Почтовый Отзывный',
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 5.5,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'https://belarusbank.by/ru/fizicheskim_licam/33357/vklady/byr/31528',
                ]
            )
        );

        $this->create(
            'До востребования Беларусбанк',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => '',
                    DepositTerm::RATE => 0.5,
                    DepositTerm::MIN_SUM => 1,
                ]
            )
        );
    }


    /**
     * @param string $deposit
     * @param array $terms
     * @param string $bankName
     */
    protected function create(string $deposit, array $terms, string $bankName = null) : void
    {
        if (empty($bankName) === false) {
            $bankId    = Bank::where(Bank::SHORT_NAME, $bankName)->first()->id;
            $depositId = Deposit::where([[Deposit::NAME, $deposit], [Deposit::BANK_ID, $bankId]])->first()->id;
        } else {
            $depositId = Deposit::where(Deposit::NAME, $deposit)->first()->id;
        }

        $currencyId = Currency::where(Currency::ISO, $terms[DepositTerm::CURRENCY_ID])->first()->id;

        $id = DB::table('deposit_terms')->insertGetId(
            [
                'latest_version' => 1,
                DepositTerm::DEPOSIT_ID => $depositId,
                DepositTerm::CURRENCY_ID => $currencyId,
                DepositTerm::PERIOD => $terms[DepositTerm::PERIOD],
                DepositTerm::IS_IRREVOCABLE_FIELD => $terms[DepositTerm::IS_IRREVOCABLE_FIELD],
                DepositTerm::CREATED_AT => new \DateTime(),
            ]
        );

        $insert = [
            'ref_id' => $id,
            'version' => 1,
        ];

        $fields = [DepositTerm::RATE, DepositTerm::MIN_SUM, DepositTerm::LINK,
            DepositTerm::NAME, DepositTerm::INTERNET, DepositTerm::CALCULATION_OF_INTEREST_PERIOD,
            DepositTerm::CAPITALIZATION_ALLOWED, DepositTerm::PERCENT_WITHDRAWAL_ALLOWED, DepositTerm::RATE_TYPE,
            DepositTerm::REFILLING_TYPE, DepositTerm::PROLONGATION, DepositTerm::THIRD_PERSON, DepositTerm::RESTRICTION
        ];

        foreach ($fields as $field) {
            if (isset($terms[$field])) {
                $insert[$field] = $terms[$field];
            }
        }

        DB::table('deposit_terms_version')->insert($insert);
    }


}
