<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Database\Seeder;
use \App\UserDeposit;

/**
 * Class UserDepositsTableSeeder
 */
class UserDepositsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_deposits')->insert(
            [
                'latest_version' => 1,
                UserDeposit::USER_ID => 1,
                UserDeposit::DEPOSIT_TERM_ID => 1,
                UserDeposit::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('user_deposits_version')->insert(
            [
                'ref_id' => 1,
                'version' => 1,
                UserDeposit::BALANCE => 100,
                UserDeposit::START_DATE => '2018-01-01 00:00:00',
            ]
        );

        DB::table('user_deposits')->insert(
            [
                'latest_version' => 1,
                UserDeposit::USER_ID => 1,
                UserDeposit::DEPOSIT_TERM_ID => 2,
                UserDeposit::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('user_deposits_version')->insert(
            [
                'ref_id' => 2,
                'version' => 1,
                UserDeposit::BALANCE => 200,
                UserDeposit::START_DATE => '2018-02-02 00:00:00',
            ]
        );

        DB::table('user_deposits')->insert(
            [
                'latest_version' => 1,
                UserDeposit::USER_ID => 1,
                UserDeposit::DEPOSIT_TERM_ID => 3,
                UserDeposit::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('user_deposits_version')->insert(
            [
                'ref_id' => 3,
                'version' => 1,
                UserDeposit::BALANCE => 300,
                UserDeposit::START_DATE => '2018-03-03 00:00:00',
            ]
        );
    }


}