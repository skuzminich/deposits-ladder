<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Database\Seeder;
use \App\Instalment;

/**
 * Class InstalmentsTableSeeder
 */
class InstalmentsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instalments')->insert(
            [
                'latest_version' => 1,
                Instalment::USER_DEPOSIT_IT => 1,
                Instalment::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('instalments_version')->insert(
            [
                'ref_id' => 1,
                'version' => 1,
                Instalment::DEPOSIT_DATE => '2018-03-03 00:00:00',
                Instalment::AMOUNT => '100',
            ]
        );

        DB::table('instalments')->insert(
            [
                'latest_version' => 1,
                Instalment::USER_DEPOSIT_IT => 1,
                Instalment::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('instalments_version')->insert(
            [
                'ref_id' => 2,
                'version' => 1,
                Instalment::DEPOSIT_DATE => '2018-04-04 00:00:00',
                Instalment::AMOUNT => '200',
            ]
        );

        DB::table('instalments')->insert(
            [
                'latest_version' => 1,
                Instalment::USER_DEPOSIT_IT => 2,
                Instalment::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('instalments_version')->insert(
            [
                'ref_id' => 3,
                'version' => 1,
                Instalment::DEPOSIT_DATE => '2018-05-05 00:00:00',
                Instalment::AMOUNT => '300',
            ]
        );

        DB::table('instalments')->insert(
            [
                'latest_version' => 1,
                Instalment::USER_DEPOSIT_IT => 3,
                Instalment::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('instalments_version')->insert(
            [
                'ref_id' => 4,
                'version' => 1,
                Instalment::DEPOSIT_DATE => '2018-06-06 00:00:00',
                Instalment::AMOUNT => '400',
            ]
        );
    }


}