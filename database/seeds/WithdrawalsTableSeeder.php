<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Database\Seeder;
use \App\Withdrawal;

/**
 * Class WithdrawalsTableSeeder
 */
class WithdrawalsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('withdrawals')->insert(
            [
                'latest_version' => 1,
                Withdrawal::USER_DEPOSIT_IT => 1,
                Withdrawal::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('withdrawals_version')->insert(
            [
                'ref_id' => 1,
                'version' => 1,
                Withdrawal::WITHDRAWAL_DATE => '2018-04-04 00:00:00',
                Withdrawal::AMOUNT => '100',
            ]
        );

        DB::table('withdrawals')->insert(
            [
                'latest_version' => 1,
                Withdrawal::USER_DEPOSIT_IT => 1,
                Withdrawal::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('withdrawals_version')->insert(
            [
                'ref_id' => 2,
                'version' => 1,
                Withdrawal::WITHDRAWAL_DATE => '2018-05-05 00:00:00',
                Withdrawal::AMOUNT => '200',
            ]
        );

        DB::table('withdrawals')->insert(
            [
                'latest_version' => 1,
                Withdrawal::USER_DEPOSIT_IT => 2,
                Withdrawal::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('withdrawals_version')->insert(
            [
                'ref_id' => 3,
                'version' => 1,
                Withdrawal::WITHDRAWAL_DATE => '2018-06-06 00:00:00',
                Withdrawal::AMOUNT => '300',
            ]
        );

        DB::table('withdrawals')->insert(
            [
                'latest_version' => 1,
                Withdrawal::USER_DEPOSIT_IT => 3,
                Withdrawal::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('withdrawals_version')->insert(
            [
                'ref_id' => 4,
                'version' => 1,
                Withdrawal::WITHDRAWAL_DATE => '2018-07-07 00:00:00',
                Withdrawal::AMOUNT => '400',
            ]
        );
    }


}