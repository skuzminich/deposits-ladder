<?php /** @noinspection PhpUndefinedClassInspection */

use \App\DepositTerm;

/**
 * Class DepositTermsTableSeeder
 */
class DepositTermsTableSeeder3 extends DepositTermsTableSeeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        /* Белинвестбанк */
        /* БТА */
        /* Идея */

        /* Белинвестбанк */
        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 9.8,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 15000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 15000,
                    DepositTerm::MAX_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2Y',
                    DepositTerm::RATE => 10.8,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 15000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2Y',
                    DepositTerm::RATE => 11,
                    DepositTerm::MIN_SUM => 15000,
                    DepositTerm::MAX_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2Y',
                    DepositTerm::RATE => 11.3,
                    DepositTerm::MIN_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 11.8,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 15000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 12,
                    DepositTerm::MIN_SUM => 15000,
                    DepositTerm::MAX_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 12.3,
                    DepositTerm::MIN_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 15000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 10.2,
                    DepositTerm::MIN_SUM => 15000,
                    DepositTerm::MAX_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P1Y',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2Y',
                    DepositTerm::RATE => 11,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 15000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2Y',
                    DepositTerm::RATE => 11.2,
                    DepositTerm::MIN_SUM => 15000,
                    DepositTerm::MAX_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P2Y',
                    DepositTerm::RATE => 11.5,
                    DepositTerm::MIN_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 12,
                    DepositTerm::MIN_SUM => 50,
                    DepositTerm::MAX_SUM => 15000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 12.2,
                    DepositTerm::MIN_SUM => 15000,
                    DepositTerm::MAX_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Личный выбор',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3Y',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 80000,
                ]
            )
        );

        $this->create(
            'Накопительный плюс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P360D',
                    DepositTerm::RATE => 7.3,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Накопительный плюс',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P360D',
                    DepositTerm::RATE => 7.6,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 8.1,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 8.1,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 8.35,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 8.6,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P9M',
                    DepositTerm::RATE => 8.6,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Срочный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 8.85,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Отличный плюс',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_SCHEDULE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 6.85,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Отличный плюс',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_SCHEDULE,
                self::REFILLING,
                self::MONTHLY,
                self::NO_CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 7.05,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Активный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P360D',
                    DepositTerm::RATE => 6.85,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'Активный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::VARIABLE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P360D',
                    DepositTerm::RATE => 7.05,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'BIBcoin',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::CUSTOM_RATE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 6.85,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'BIBcoin',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::CUSTOM_RATE,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 7.05,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'С правом востребования',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_SCHEDULE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P36M',
                    DepositTerm::RATE => 6,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        $this->create(
            'С правом востребования',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_SCHEDULE,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P36M',
                    DepositTerm::RATE => 6.5,
                    DepositTerm::MIN_SUM => 5,
                ]
            )
        );

        /* БТА */
        $this->create(
            'Открытие',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.1,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'Открытие',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Открытие Online',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'БТА Мини',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 9.1,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 9,
                ]
            )
        );

        $this->create(
            'БТА Мини',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'БТА Мини Online',
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::PROLONGATION => 9,
                    DepositTerm::LINK => 'http://www.btabank.by/ru/block/2332',
                ]
            )
        );

        $this->create(
            'БТА Максима',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 7.9,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        $this->create(
            'БТА Максима',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'БТА Максима Online',
                    DepositTerm::PERIOD => 'P6M',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::LINK => 'http://www.btabank.by/ru/block/2337',
                ]
            )
        );

        $this->create(
            'БТА Копилка',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 50,
                ]
            )
        );

        $this->create(
            'БТА Профит',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P12M',
                    DepositTerm::RATE => 10.35,
                    DepositTerm::MIN_SUM => 100,
                    DepositTerm::FIXED_RATE_PERIOD => 'P6M',
                ]
            )
        );

        $this->create(
            'БТА +',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::PERIOD => 'P14M',
                    DepositTerm::RATE => 10.5,
                    DepositTerm::MIN_SUM => 100,
                ]
            )
        );

        /* Идея */
        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Базовый 1',
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 5.7,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-bazovy-1/',
                ]
            ),
            'Идея Банк'
        );

        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Базовый 3',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 6.7,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-bazovy-3/',
                ]
            ),
            'Идея Банк'
        );

        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Базовый 3 плюс',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 8.9,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-bazovy-3-plus/',
                ]
            ),
            'Идея Банк'
        );

        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Базовый 6',
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 7.6,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-bazovy-6/',
                ]
            ),
            'Идея Банк'
        );

        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Базовый 6 плюс',
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 10,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-bazovy-6-plus/',
                ]
            ),
            'Идея Банк'
        );

        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_PERIOD,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Базовый 12',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 7.75,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::FIXED_RATE_PERIOD => 'P6M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-bazovy-12/',
                ]
            ),
            'Идея Банк'
        );

        $this->create(
            'Базовый',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Базовый 12 плюс',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 12.2,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::FIXED_RATE_PERIOD => 'P9M',
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-bazovy-12-plus/',
                ]
            ),
            'Идея Банк'
        );

        $this->create(
            'Онлайн',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Онлайн 1',
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 5.9,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-online-1/',
                ]
            )
        );

        $this->create(
            'Онлайн',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Онлайн 3',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 6.9,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-online-3/',
                ]
            )
        );

        $this->create(
            'Онлайн',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Онлайн 3 плюс',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.1,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-online-3-plus/',
                ]
            )
        );

        $this->create(
            'Онлайн',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Онлайн 6',
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 7.8,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-online-6/',
                ]
            )
        );

        $this->create(
            'Онлайн',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Онлайн 6 плюс',
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 10.2,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-online-6-plus/',
                ]
            )
        );

        $this->create(
            'Онлайн',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Онлайн 12',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 7.95,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::FIXED_RATE_PERIOD => 'P6M',
                    DepositTerm::REFILLING_PERIOD => 'P11M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-online-12/',
                ]
            )
        );

        $this->create(
            'Онлайн',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Онлайн 12 плюс',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 12.4,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::FIXED_RATE_PERIOD => 'P9M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-online-12-plus/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный 1',
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 5.8,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-1/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный 3',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 6.8,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-3/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный 3 плюс',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-3-plus/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный 6',
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 7.7,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-6/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный 6 плюс',
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 10.1,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-6-plus/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный 12',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 7.85,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::FIXED_RATE_PERIOD => 'P6M',
                    DepositTerm::REFILLING_PERIOD => 'P11M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-12/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::NO_INTERNET,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный 12 плюс',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 12.3,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::FIXED_RATE_PERIOD => 'P6M',
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-12-plus/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::NO_REFILLING,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный Онлайн 1',
                    DepositTerm::PERIOD => 'P35D',
                    DepositTerm::RATE => 6,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-online-1/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный Онлайн 3',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 7,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P2M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-online-3/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный Онлайн 3 плюс',
                    DepositTerm::PERIOD => 'P3M',
                    DepositTerm::RATE => 9.2,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-online-3-plus/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный Онлайн 6',
                    DepositTerm::PERIOD => 'P185D',
                    DepositTerm::RATE => 7.9,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-online-6/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED,
                self::REFILLING_PERIOD,
                self::WEEKLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный Онлайн 6 плюс',
                    DepositTerm::PERIOD => 'P190D',
                    DepositTerm::RATE => 10.3,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-online-6-plus/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::REVOCABLE,
                self::FIXED_PERIOD,
                self::NO_REFILLING,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный Онлайн 12',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 8.05,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::FIXED_RATE_PERIOD => 'P6M',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-online-12/',
                ]
            )
        );

        $this->create(
            'Фирменный',
            array_merge(
                self::BYN,
                self::IRREVOCABLE,
                self::FIXED_PERIOD,
                self::REFILLING_PERIOD,
                self::MONTHLY,
                self::CAPITALIZATION,
                self::INTERNET_ONLY,
                self::NO_THIRD_PERSON,
                self::OTHERS,
                [
                    DepositTerm::NAME => 'Фирменный Онлайн 12 плюс',
                    DepositTerm::PERIOD => 'P370D',
                    DepositTerm::RATE => 12.5,
                    DepositTerm::MIN_SUM => 20,
                    DepositTerm::FIXED_RATE_PERIOD => 'P9M',
                    DepositTerm::REFILLING_PERIOD => 'P5D',
                    DepositTerm::LINK => 'https://www.ideabank.by/private-osoby/vklady/vklad-firmenny-online-12-plus/',
                ]
            )
        );
    }


}
