<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Database\Seeder;
use \App\Bank;

/**
 * Class BanksTableSeeder
 */
class BanksTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $this->_create('Белагропромбанк', 'ОАО "Белагропромбанк"', 'http://www.belapb.by');
        $this->_create('Беларусбанк', 'ОАО "Сберегательный банк "Беларусбанк"', 'https://belarusbank.by');
        $this->_create('Банк БелВЭБ', 'ОАО "Банк БелВЭБ"', 'https://www.belveb.by');
        $this->_create('Белгазпромбанк', 'ОАО "Белгазпромбанк"', 'http://belgazprombank.by');
        $this->_create('Белинвестбанк', 'ОАО "Белорусский банк развития и реконструкции "Белинвестбанк"', 'https://www.belinvestbank.by');
        $this->_create('БТА Банк', 'ЗАО "БТА Банк"', 'http://www.btabank.by');
        $this->_create('Идея Банк', 'ЗАО "Идея Банк"', 'https://www.ideabank.by');
        $this->_create('Банк Москва-Минск', 'ОАО "Банк "Москва-Минск"', 'https://www.mmbank.by');
        $this->_create('МТБанк', 'ЗАО "МТБанк"', 'https://www.mtbank.by');
        $this->_create('Банк Решение', 'ЗАО "Банк "Решение"', 'https://rbank.by');
        $this->_create('Франсабанк', 'ОАО "Франсабанк"', 'https://fransabank.by');
        $this->_create('Альфа-Банк', 'ЗАО "Альфа-Банк"', 'https://www.alfabank.by');
        $this->_create('БНБ-Банк', 'ОАО "Белорусский народный банк"', 'https://www.bnb.by');
        $this->_create('БПС-Сбербанк', 'ОАО "БПС-Сбербанк"', 'https://www.bps-sberbank.by');
        $this->_create('Банк ВТБ', 'ЗАО "Банк ВТБ(Беларусь)"', 'https://www.vtb-bank.by');
        $this->_create('Приорбанк', 'ОАО "Приорбанк"', 'https://www.priorbank.by');
        $this->_create('Статусбанк', 'ОАО "Статусбанк"', 'https://stbank.by');
        $this->_create('Паритетбанк', 'ОАО "Паритетбанк"', 'http://www.paritetbank.by');
        $this->_create('РРБ-Банк', 'ЗАО "РРБ-Банк"', 'https://rrb.by');
        $this->_create('Технобанк', 'ОАО "Технобанк"', 'https://tb.by/individuals');
        $this->_create('Абсолютбанк', 'ЗАО "Абсолютбанк"', 'https://absolutbank.by');
        $this->_create('Цептер Банк', 'ЗАО "Цептер Банк"', 'https://www.zepterbank.by');
        $this->_create('БСБ Банк', 'ЗАО "Белорусско-Швейцарский Банк "БСБ Банк"', 'https://www.bsb.by');
        $this->_create('ТК Банк', 'ЗАО "Банк торговый капитал"', 'https://tcbank.by');
        $this->_create('Хоум Кредит Банк', 'ОАО "Хоум Кредит Банк"', 'http://homecredit.by');
    }


    /**
     * @param string $shortName
     * @param string $fullName
     * @param string $link
     */
    private function _create(string $shortName, string $fullName, string $link = null) : void
    {
        $id = DB::table('banks')->insertGetId(
            [
                Bank::STATUS => Bank::STATUS_ACTIVE,
                'latest_version' => 1,
                Bank::CREATED_AT => new \DateTime(),
            ]
        );

        DB::table('banks_version')->insert(
            [
                'ref_id' => $id,
                'version' => 1,
                Bank::SHORT_NAME => $shortName,
                Bank::FULL_NAME => $fullName,
                Bank::LINK => $link,
            ]
        );
    }


}