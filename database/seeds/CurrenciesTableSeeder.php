<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Database\Seeder;
use \App\Currency;

/**
 * Class CurrenciesTableSeeder
 */
class CurrenciesTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert(
            [
                Currency::NAME => 'Белорусский рубль',
                Currency::ISO => 'BYN',
                Currency::SYMBOL => 'Br',
            ]
        );

        DB::table('currencies')->insert(
            [
                Currency::NAME => 'Доллар США',
                Currency::ISO => 'USD',
                Currency::SYMBOL => '$',
            ]
        );

        DB::table('currencies')->insert(
            [
                Currency::NAME => 'Евро',
                Currency::ISO => 'EUR',
                Currency::SYMBOL => '€',
            ]
        );
    }


}