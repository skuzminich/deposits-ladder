<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() : void
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(DepositsTableSeeder::class);
        $this->call(DepositTermsTableSeeder::class);
        $this->call(DepositTermsTableSeeder2::class);
        $this->call(DepositTermsTableSeeder3::class);
        $this->call(DepositTermsTableSeeder4::class);
        $this->call(DepositTermsTableSeeder5::class);
        $this->call(DepositTermsTableSeeder6::class);
        $this->call(DepositTermUpdatesTableSeeder::class);
        $this->call(UserDepositsTableSeeder::class);
        $this->call(InstalmentsTableSeeder::class);
        $this->call(WithdrawalsTableSeeder::class);
    }


    /**
     * Generates fake data in database
     */
    private function _generateFake() : void
    {
        factory(App\Currency::class, 3)->create();

        factory(App\Bank::class, 5)
            ->create()
            ->each(
                function(\App\Bank $bank) {
                    $deposits = factory(\App\Deposit::class, 5)->make();
                    $bank->deposits()->saveMany($deposits);

                    $deposits->each(
                        function(\App\Deposit $deposit) {
                            $depositTerms = factory(\App\DepositTerm::class, 5)->make();
                            $deposit->depositTerms()->saveMany($depositTerms);

                            $depositTerms->each(
                                function(\App\DepositTerm $term) {
                                    $updates = factory(\App\DepositTermUpdate::class, 5)->make();
                                    $term->depositTermUpdates()->saveMany($updates);
                                }
                            );
                        }
                    );
                }
            );

        factory(App\User::class, 5)
            ->create()
            ->each(
                function(\App\User $user) {
                    $userDeposits = factory(\App\UserDeposit::class, 5)->make();
                    $user->userDeposits()->saveMany($userDeposits);

                    $userDeposits->each(
                        function(\App\UserDeposit $userDeposit) {
                            $instalments = factory(\App\Instalment::class, 5)->make();
                            $userDeposit->instalments()->saveMany($instalments);

                            $withdrawals = factory(\App\Withdrawal::class, 5)->make();
                            $userDeposit->instalments()->saveMany($withdrawals);
                        }
                    );
                }
            );
    }


}