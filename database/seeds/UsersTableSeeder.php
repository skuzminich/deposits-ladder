<?php /** @noinspection PhpUndefinedClassInspection */

use Illuminate\Database\Seeder;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @noinspection SpellCheckingInspection */
        DB::table('users')->insert(
            [
                'name' => 'Сергей Кузьминич',
                'email' => 'KuzminichSergey@yandex.ru',
                'password' => '$2y$10$W5I0VwWj6uDa6XfTnCiuD.lDQyxRbiF/OVR2w.0840Q4ojZXhNrza',
            ]
        );

    }


}