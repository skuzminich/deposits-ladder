<?php
namespace App\Scopes;

use App\Bank;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BankScope
 * @package App\Scopes
 */
class BankScope implements Scope
{


    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->where(Bank::STATUS, Bank::STATUS_ACTIVE)->orderBy(Bank::SHORT_NAME);
    }


}