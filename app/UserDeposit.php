<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ProAI\Versioning\SoftDeletes;
use ProAI\Versioning\Versionable;

/**
 * Class UserDeposit
 * @package App
 *
 * @property int $id
 * @property int $deposit_term_id
 * @property int $users_id
 * @property float $balance
 * @property string $start_date
 * @property string $end_date
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property DepositTerm $depositTerm
 * @property User $user
 * @property Withdrawal[] $withdrawals
 * @property Instalment[] $instalments
 */
class UserDeposit extends Model
{
    use Versionable, SoftDeletes;

    public $timestamps = true;

    /** @var array  */
    protected $fillable = [self::DEPOSIT_TERM_ID, self::USER_ID, self::CREATED_AT, self::UPDATED_AT, self::DELETED_AT];

    /** @var array  */
    protected $versioned = [self::BALANCE, self::START_DATE, self::END_DATE, self::UPDATED_AT, self::DELETED_AT];

    /** FIELDS NAMES */
    const ID = 'id';
    const DEPOSIT_TERM_ID = 'deposit_term_id';
    const USER_ID    = 'user_id';
    const BALANCE    = 'balance';
    const START_DATE = 'start_date';
    const END_DATE   = 'end_date';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function depositTerm() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\DepositTerm');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\User');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function withdrawals() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Withdrawal');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instalments() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Instalment');
    }


}