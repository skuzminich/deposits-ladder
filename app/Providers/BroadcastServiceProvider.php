<?php /** @noinspection PhpCSValidationInspection */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use /** @noinspection PhpUndefinedClassInspection */ Illuminate\Support\Facades\Broadcast;

/**
 * Class BroadcastServiceProvider
 * @package App\Providers
 */
class BroadcastServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() : void
    {
        /** @noinspection PhpUndefinedClassInspection */
        Broadcast::routes();

        /** @noinspection PhpIncludeInspection */
        require base_path('routes/channels.php');
    }


}