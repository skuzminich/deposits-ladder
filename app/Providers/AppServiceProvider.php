<?php /** @noinspection PhpUndefinedClassInspection */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() : void
    {
        \Blade::include('components.form-start', 'startForm');
        \Blade::include('components.form-end', 'endForm');
        \Blade::include('components.form-error', 'error');
        \Blade::include('components.form-validation', 'validation');
        \Blade::include('components.form-text', 'text');
        \Blade::include('components.form-number', 'number');
        \Blade::include('components.form-checkbox', 'checkbox');
        \Blade::include('components.form-radio', 'radio');
        \Blade::include('components.form-select', 'select');
        \Blade::include('components.form-date', 'date');
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() : void
    {
        if ($this->app->environment() === 'dev') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }


}
