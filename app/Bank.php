<?php

namespace App;

use App\Scopes\BankScope;
use Illuminate\Database\Eloquent\Model;
use ProAI\Versioning\SoftDeletes;
use ProAI\Versioning\Versionable;

/**
 * Class Bank
 * @package App
 *
 * @property integer $id
 * @property integer $latest_version
 * @property integer $status
 * @property string $full_name
 * @property string $short_name
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Deposit[] $deposits
 */
class Bank extends Model
{
    use Versionable, SoftDeletes;

    public $timestamps = true;

    /** @var array */
    protected $fillable = [self::STATUS, self::CREATED_AT, self::UPDATED_AT, self::DELETED_AT];

    /** @var array  */
    protected $versioned = [self::FULL_NAME, self::SHORT_NAME, self::LINK, self::UPDATED_AT, self::DELETED_AT];

    /** FIELDS NAMES */
    const ID     = 'id';
    const STATUS = 'status';
    const SHORT_NAME = 'short_name';
    const FULL_NAME  = 'full_name';
    const LINK       = 'link';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';

    /** FIELDS VALUES */
    const STATUS_ACTIVE = 0;
    const STATUS_DEPOSITS_FREEZED = 1;
    const STATUS_LIQUIDATED       = 2;

    const WITH_LABELS = true;


    /**
     * Gets all available bank statuses
     * @param bool $withLabels
     * @return array
     */
    static public function getAllStatuses($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::STATUS_ACTIVE => __('banks.active'),
                self::STATUS_DEPOSITS_FREEZED => __('banks.freeze'),
                self::STATUS_LIQUIDATED => __('banks.inactive'),
            ];
        } else {
            return [
                self::STATUS_ACTIVE,
                self::STATUS_DEPOSITS_FREEZED,
                self::STATUS_LIQUIDATED
            ];
        }
    }


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        $bankScope = new BankScope;
        static::addGlobalScope($bankScope);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deposits() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Deposit');
    }


}
