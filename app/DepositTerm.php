<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use ProAI\Versioning\SoftDeletes;
use ProAI\Versioning\Versionable;

/**
 * Class DepositTerm
 * @package App
 *
 * @property int $id
 * @property integer $currency_id
 * @property string $period
 * @property integer $is_irrevocable
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Deposit $deposit
 * @property Currency $currency
 * @property Collection $depositTermUpdates
 * @property Collection $userDeposits
 */
class DepositTerm extends Model
{
    use Versionable, SoftDeletes;

    public $timestamps = true;

    /** @var array  */
    protected $fillable = [
        self::CURRENCY_ID,
        self::PERIOD,
        self::IS_IRREVOCABLE_FIELD,
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT
    ];

    /** @var array  */
    protected $versioned = [self::NAME, self::CALCULATION_OF_INTEREST_PERIOD, self::CAPITALIZATION_ALLOWED,
        self::PERCENT_WITHDRAWAL_ALLOWED, self::RATE_TYPE, self::RATE, self::REFILLING_TYPE, self::INTERNET,
        self::PROLONGATION, self::MIN_SUM, self::IRREDUCIBLE_BALANCE, self::FULL_REVOKE_CONDITIONS, self::FULL_REVOKE_PERCENT,
        self::PARTIAL_REVOKE_CONDITIONS, self::PARTIAL_REVOKE_SANCTIONS_PERCENT, self::FIXED_RATE_PERIOD,
        self::REFILLING_PERIOD, self::REFILLING_MIN_SUM, self::PROLONGATION_CONDITIONS, self::MAX_SUM, self::THIRD_PERSON,
        self::LINK, self::RESTRICTION, self::UPDATED_AT, self::DELETED_AT
    ];

    /** FIELDS NAMES */
    const ID = 'id';
    const CURRENCY_ID = 'currency_id';
    const DEPOSIT_ID  = 'deposit_id';
    const PERIOD      = 'period';
    const IS_IRREVOCABLE_FIELD = 'is_irrevocable';
    const NAME = 'name';
    const CALCULATION_OF_INTEREST_PERIOD = 'calculation_of_interest_period';
    const CAPITALIZATION_ALLOWED     = 'capitalization_allowed';
    const PERCENT_WITHDRAWAL_ALLOWED = 'percent_withdrawal_allowed';
    const RATE_TYPE = 'rate_type';
    const RATE      = 'rate';
    const REFILLING_TYPE = 'refilling_type';
    const INTERNET       = 'internet';
    const PROLONGATION   = 'prolongation';
    const MIN_SUM        = 'min_sum';
    const IRREDUCIBLE_BALANCE       = 'irreducible_balance';
    const FULL_REVOKE_CONDITIONS    = 'full_revoke_conditions';
    const FULL_REVOKE_PERCENT       = 'full_revoke_percent';
    const PARTIAL_REVOKE_CONDITIONS = 'partial_revoke_conditions';
    const PARTIAL_REVOKE_SANCTIONS_PERCENT = 'partial_revoke_sanctions_percent';
    const FIXED_RATE_PERIOD       = 'fixed_rate_period';
    const REFILLING_PERIOD        = 'refilling_period';
    const REFILLING_MIN_SUM       = 'refilling_min_sum';
    const PROLONGATION_CONDITIONS = 'prolongation_conditions';
    const MAX_SUM      = 'max_sum';
    const THIRD_PERSON = 'third_person';
    const LINK        = 'link';
    const RESTRICTION = 'restriction';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    /** FIELDS VALUES */
    const IS_NOT_IRREVOCABLE = 0;
    const IS_IRREVOCABLE     = 1;

    const CALCULATION_ONCE    = 0;
    const CALCULATION_WEEKLY  = 1;
    const CALCULATION_MONTHLY = 2;
    const CALCULATION_TWICE_PER_MONTH = 3;

    const CAPITALIZATION_DISABLED = 0;
    const CAPITALIZATION_ENABLED  = 1;

    const PERCENTS_WITHDRAWAL_DISABLED = 0;
    const PERCENTS_WITHDRAWAL_ENABLED  = 1;

    const RATE_FIXED     = 0;
    const RATE_VARIABLE  = 1;
    const RATE_OVERNIGHT = 2;
    const RATE_REFINANCING_RATE = 3;
    const RATE_FIXED_SCHEDULE   = 4;
    const RATE_FIXED_PERIOD     = 5;
    const RATE_CUSTOM = 10;

    const REFILLING_DISABLED       = 0;
    const REFILLING_ENABLED        = 1;
    const REFILLING_ENABLED_PERIOD = 2;

    const INTERNET_NO       = 0;
    const INTERNET_ONLY     = 1;
    const INTERNET_POSSIBLE = 2;

    const THIRD_PERSON_DISALLOWED = 0;
    const THIRD_PERSON_ALLOWED    = 1;

    const WITH_LABELS = true;


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllIrrevocableStatuses($withLabels = false)
    {
        if ($withLabels) {
            return [
                self::IS_IRREVOCABLE  => __('banks.deposits.terms.irrevocable'),
                self::IS_NOT_IRREVOCABLE => __('banks.deposits.terms.revocable'),
            ];
        } else {
            return [
                self::IS_IRREVOCABLE,
                self::IS_NOT_IRREVOCABLE,
            ];
        }
    }


    /**
     * @param $withLabels
     * @return array
     */
    static public function getAllCalculationPeriods($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::CALCULATION_MONTHLY => __('banks.deposits.terms.monthly'),
                self::CALCULATION_WEEKLY  => __('banks.deposits.terms.weekly'),
                self::CALCULATION_ONCE    => __('banks.deposits.terms.once'),
                self::CALCULATION_TWICE_PER_MONTH => __('banks.deposits.terms.twice_per_month'),
            ];
        } else {
            return [
                self::CALCULATION_MONTHLY,
                self::CALCULATION_WEEKLY,
                self::CALCULATION_ONCE,
                self::CALCULATION_TWICE_PER_MONTH,
            ];
        }
    }


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllCapitalizationStatuses($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::CAPITALIZATION_ENABLED  => __('banks.yes'),
                self::CAPITALIZATION_DISABLED => __('banks.no'),
            ];
        } else {
            return [
                self::CAPITALIZATION_ENABLED,
                self::CAPITALIZATION_DISABLED,
            ];
        }
    }


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllPercentWithdrawalStatuses($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::PERCENTS_WITHDRAWAL_ENABLED  => __('banks.yes'),
                self::PERCENTS_WITHDRAWAL_DISABLED => __('banks.no'),
            ];
        } else {
            return [
                self::PERCENTS_WITHDRAWAL_ENABLED,
                self::PERCENTS_WITHDRAWAL_DISABLED,
            ];
        }
    }


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllRateTypes($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::RATE_FIXED     => __('banks.deposits.terms.rate_fixed'),
                self::RATE_VARIABLE  => __('banks.deposits.terms.rate_variable'),
                self::RATE_OVERNIGHT => __('banks.deposits.terms.rate_overnight'),
                self::RATE_REFINANCING_RATE => __('banks.deposits.terms.rate_refinancing'),
                self::RATE_FIXED_SCHEDULE   => __('banks.deposits.terms.rate_fixed_schedule'),
                self::RATE_FIXED_PERIOD     => __('banks.deposits.terms.fixed_rate_period'),
                self::RATE_CUSTOM => __('banks.deposits.terms.rate_custom'),
            ];
        } else {
            return [
                self::RATE_FIXED,
                self::RATE_VARIABLE,
                self::RATE_OVERNIGHT,
                self::RATE_REFINANCING_RATE,
                self::RATE_FIXED_SCHEDULE,
                self::RATE_FIXED_PERIOD,
                self::RATE_CUSTOM,
            ];
        }
    }


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllRefillingTypes($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::REFILLING_ENABLED  => __('banks.deposits.terms.refilling_enabled'),
                self::REFILLING_DISABLED => __('banks.deposits.terms.refilling_disabled'),
                self::REFILLING_ENABLED_PERIOD => __('banks.deposits.terms.refilling_period'),
            ];
        } else {
            return [
                self::REFILLING_ENABLED,
                self::REFILLING_DISABLED,
                self::REFILLING_ENABLED_PERIOD,
            ];
        }
    }


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllInternetStatuses($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::INTERNET_NO       => __('banks.deposits.terms.internet_no'),
                self::INTERNET_ONLY     => __('banks.deposits.terms.internet_only'),
                self::INTERNET_POSSIBLE => __('banks.deposits.terms.internet_possible'),
            ];
        } else {
            return [
                self::INTERNET_NO,
                self::INTERNET_ONLY,
                self::INTERNET_POSSIBLE,
            ];
        }
    }


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllThirdPersonStatuses($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::THIRD_PERSON_ALLOWED  => __('banks.yes'),
                self::THIRD_PERSON_DISALLOWED => __('banks.no'),
            ];
        } else {
            return [
                self::THIRD_PERSON_ALLOWED,
                self::THIRD_PERSON_DISALLOWED,
            ];
        }
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deposit() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Deposit');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Currency');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function depositTermUpdates() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\DepositTermUpdate');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userDeposits() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\UserDeposit');
    }


}