<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property UserDeposit[] $userDeposits
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    public $timestamps = true;

    /** @var array  */
    protected $fillable = [self::NAME, self::EMAIL, self::PASSWORD, self::REMEMBER_TOKEN, self::CREATED_AT, self::UPDATED_AT,
        self::DELETED_AT];

    /** @var array  */
    protected $hidden = [
        self::PASSWORD, self::REMEMBER_TOKEN,
    ];

    /** FIELDS NAMES */
    const ID    = 'id';
    const NAME  = 'name';
    const EMAIL = 'email';
    const CREATED_AT     = 'created_at';
    const UPDATED_AT     = 'updated_at';
    const DELETED_AT     = 'deleted_at';
    const PASSWORD       = 'password';
    const REMEMBER_TOKEN = 'remember_token';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userDeposits() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\UserDeposit');
    }


}