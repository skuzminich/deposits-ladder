<?php

namespace App;

use App\Scopes\DepositScope;
use Illuminate\Database\Eloquent\Model;
use ProAI\Versioning\SoftDeletes;
use ProAI\Versioning\Versionable;

/**
 * Class Deposit
 * @package App
 *
 * @property integer $id
 * @property integer $bank_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Bank $bank
 */
class Deposit extends Model
{
    use Versionable, SoftDeletes;

    public $timestamps = true;

    /**
     * The "type" of the auto-incrementing ID.
     * @var string
     */
    protected $keyType = 'integer';

    /** @var array  */
    protected $fillable = [self::BANK_ID, self::STATUS, self::CREATED_AT, self::UPDATED_AT, self::DELETED_AT];

    /** @var array  */
    protected $versioned = [self::NAME, self::LINK, self::OPEN_RESTRICTION, self::UPDATED_AT, self::DELETED_AT];

    /** FIELDS NAMES */
    const ID      = 'id';
    const BANK_ID = 'bank_id';
    const NAME    = 'name';
    const STATUS  = 'status';
    const LINK    = 'link';
    const OPEN_RESTRICTION = 'open_restriction';
    const UPDATED_AT       = 'updated_at';
    const DELETED_AT       = 'deleted_at';

    /** FIELDS VALUES */
    const STATUS_ACTIVE   = 0;
    const STATUS_FREEZED  = 1;
    const STATUS_ARCHIVED = 2;

    const WITH_LABELS = true;


    /**
     * @param bool $withLabels
     * @return array
     */
    static public function getAllStatuses($withLabels = false) : array
    {
        if ($withLabels) {
            return [
                self::STATUS_ACTIVE   => __('banks.deposits.active'),
                self::STATUS_ARCHIVED => __('banks.deposits.inactive'),
                self::STATUS_FREEZED  => __('banks.deposits.freezed'),
            ];
        } else {
            return [
                self::STATUS_ACTIVE,
                self::STATUS_ARCHIVED,
                self::STATUS_FREEZED,
            ];
        }
    }


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        $depositScope = new DepositScope;
        static::addGlobalScope($depositScope);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Bank');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function depositTerms() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\DepositTerm');
    }


}