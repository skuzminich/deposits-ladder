<?php
namespace App\Http\Resources;

use Illuminate\Database\Eloquent\Concerns\HidesAttributes;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Resource
 * @package App\Http\Resources
 */
class Resource extends JsonResource
{
    use HidesAttributes;
}