<?php

namespace App\Http\Resources;

/**
 * Class Withdrawal
 * @package App\Http\Resources
 */
class Withdrawal extends Resource
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route(
                    'users.deposits.withdrawals.show',
                    ['user' => $this->userDeposit->user, 'deposit' => $this->userDeposit, 'withdrawal' => $this]
                )
            ],
            $this->resource->makeHidden('userDeposit')->toArray($request),
        ];
    }


}