<?php

namespace App\Http\Resources;

/**
 * Class DepositTerm
 * @package App\Http\Resources
 */
class DepositTerm extends Resource
{

    /** @var array  */
    public $hidden = ['deposit'];


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route('deposits.terms.show', ['deposit' => $this->deposit, 'term' => $this])
            ],
            'data' => $this->resource->makeHidden('deposit')->toArray($request),
        ];
    }


}