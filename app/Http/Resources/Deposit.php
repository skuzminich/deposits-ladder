<?php

namespace App\Http\Resources;

/**
 * Class Deposit
 * @package App\Http\Resources
 */
class Deposit extends Resource
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route('deposits.show', ['deposit' => $this->id])
            ],
            'data' => parent::toArray($request),
        ];
    }


}