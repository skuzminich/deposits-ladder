<?php

namespace App\Http\Resources;

/**
 * Class UserDeposit
 * @package App\Http\Resources
 */
class UserDeposit extends Resource
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route(
                    'users.deposits.show',
                    ['user' => $this->user, 'deposit' => $this]
                )
            ],
            $this->resource->makeHidden(['user', 'depositTerm'])->toArray($request),
        ];
    }


}