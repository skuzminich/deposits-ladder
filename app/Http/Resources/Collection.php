<?php
namespace App\Http\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class BankCollection
 * @package App\Http\Resources
 */
class Collection extends ResourceCollection
{

    protected $self = null;
    protected $resourceClass = null;


    /**
     * Collection constructor.
     * @param $resource
     * @param string $self
     * @param string $resourceClass
     */
    public function __construct($resource, string $self, string $resourceClass)
    {
        $this->self = $self;
        $this->resourceClass = $resourceClass;

        parent::__construct($resource);
    }


    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $resourceClass = $this->resourceClass;
        return [
            'links' => [
                'self' => $this->self,
            ],
            'data' => $this->collection->map(
                function(Model $item) use ($request, $resourceClass) {
                    /** @var \App\Http\Resources\Resource $resource */
                    $resource = new $resourceClass($item);
                    return $resource->toArray($request);
                }
            ),
        ];
    }


}