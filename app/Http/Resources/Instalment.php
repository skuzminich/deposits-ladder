<?php

namespace App\Http\Resources;

/**
 * Class Instalment
 * @package App\Http\Resources
 */
class Instalment extends Resource
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route(
                    'users.deposits.instalments.show',
                    ['user' => $this->userDeposit->user, 'deposit' => $this->userDeposit, 'instalment' => $this]
                )
            ],
            $this->resource->makeHidden('userDeposit')->toArray($request),
        ];
    }


}