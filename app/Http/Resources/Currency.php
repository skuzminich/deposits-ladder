<?php

namespace App\Http\Resources;

/**
 * Class Currency
 * @package App\Http\Resources
 */
class Currency extends Resource
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route('currencies.show', ['currency' => $this->id])
            ],
            'data' => parent::toArray($request),
        ];
    }


}