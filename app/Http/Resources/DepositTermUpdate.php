<?php

namespace App\Http\Resources;

/**
 * Class DepositTermsUpdate
 * @package App\Http\Resources
 */
class DepositTermUpdate extends Resource
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route(
                    'deposits.terms.updates.show',
                    ['deposit' => $this->depositTerm->deposit, 'term' => $this->depositTerm, 'update' => $this]
                )
            ],
            'data' => $this->resource->makeHidden('depositTerm')->toArray($request),
        ];
    }


}