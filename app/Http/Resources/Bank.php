<?php

namespace App\Http\Resources;

/**
 * Class Bank
 * @package App\Http\Resources
 */
class Bank extends Resource
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'links' => [
                'self' => route('banks.show', ['bank' => $this->id])
            ],
            'data' => parent::toArray($request),
        ];
    }


}