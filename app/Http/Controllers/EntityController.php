<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;

/**
 * Class EntityController
 * @package App\Http\Controllers
 */
abstract class EntityController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const CHECKBOXES = [];

    const ID    = 'id';
    const MODEL = 'model';
    const ITEMS = 'items';
    const ROUTER_NAME = 'routerName';
    const VALIDATION  = 'validation';

    const CLASS_NAME        = 'class';
    const VIEW_FIELD        = 'view';
    const ROUTE_FIELD       = 'route';
    const DEPENDENCY_METHOD = 'dependency';
    const DB_FIELD = 'dbField';

    const FILTER = 'filter';

    const EDIT  = 'edit';
    const INDEX = 'index';

    protected $entityName;

    protected $parentEntities = [];


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setEntityName();
        $this->setParentEntityVariable();
    }


    /**
     * Sets default values of parent entities view field name, router field name and others
     */
    protected function setParentEntityVariable() : void
    {
        if (empty($this->parentEntities) === false) {
            $initialConfig        = $this->parentEntities;
            $this->parentEntities = [];
            foreach ($initialConfig as $index => $entity) {
                $entityName = $index;
                if (is_string($entityName) === false && is_string($entity)) {
                    $entityName = $entity;
                }

                /** @noinspection SpellCheckingInspection */
                $uppered     = ucfirst($entityName);
                $lowered     = lcfirst($entityName);
                $underscored = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $entityName));

                $config = [
                    self::CLASS_NAME        => $uppered,
                    self::VIEW_FIELD        => $lowered,
                    self::ROUTE_FIELD       => $lowered,
                    self::DEPENDENCY_METHOD => $lowered,
                    self::DB_FIELD          => $underscored,
                ];

                if (is_array($entity)) {
                    $config = array_merge($config, $entity);
                }

                $this->parentEntities[$index] = $config;
            }

            $this->parentEntities = array_values($this->parentEntities);
        }
    }


    /**
     * Sets entity name
     */
    protected function setEntityName() : void
    {
        $path = explode('\\', static::class);
        $fullClassName    = array_pop($path);
        $entityName       = substr($fullClassName, 0, -strlen('Controller'));
        $this->entityName = lcfirst($entityName);
    }


    /**
     * @return array
     */
    abstract static protected function getValidationArray() : array;


    /**
     * @return array
     */
    protected function getExcludeFields() : array
    {
        return [static::ID];
    }


    /**
     * @param Model $model
     * @param array $data
     * @return Model
     */
    protected function fillModel(Model $model, array $data) : Model
    {
        $fields = array_diff(
            array_keys(static::getValidationArray()),
            $this::getExcludeFields()
        );

        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $model->$field = $data[$field];
            } elseif (in_array($field, static::CHECKBOXES)) {
                $model->$field = 0;
            } else {
                $model->$field = null;
            }
        }

        return $model;
    }


    /**
     * @param string $key
     * @param array $entityIds
     * @return array
     */
    protected function loadParents($key, array $entityIds = null)
    {
        $result = [];
        if (empty($entityIds) === false) {
            $nameSpace    = current(explode('\\', static::class));
            $parentsCount = count($this->parentEntities);
            for ($i = 0; $i < $parentsCount; $i++) {
                $entity = $this->parentEntities[$i];
                /** @var Model $entityClass */
                $entityClass = $entity[static::CLASS_NAME];
                $entityId    = $entityIds[$i];
                $entityClass = '\\' . $nameSpace . '\\' . $entityClass;

                $result[$entity[$key]] = $entityClass::findOrFail($entityId);
            }
        }

        return $result;
    }


    /**
     * @return string
     */
    protected function getRouterName() : string
    {
        return Str::snake($this->entityName, '.');
    }


    /**
     * @return string
     */
    protected function getModelClassName() : string
    {
        $nameSpace = current(explode('\\', static::class));
        return '\\' . $nameSpace . '\\' . ucfirst($this->entityName);
    }


}