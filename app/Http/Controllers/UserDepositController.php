<?php

namespace App\Http\Controllers;

use App\DepositTerm;
use App\UserDeposit;
use Illuminate\Http\Request;

/**
 * Class UserDepositController
 * @package App\Http\Controllers
 */
class UserDepositController extends AdminEntityController
{

    /** @var array */
    protected $parentEntities = ['user'];


    /**
     * @return array
     */
    protected function getRelationsForList(): array
    {
        return ['depositTerm', 'depositTerm.currency', 'depositTerm.deposit', 'depositTerm.deposit.bank'];
    }


    /**
     * @param Request $request
     * @return array
     */
    protected function getCustomViewVariables(Request $request) : array
    {
        $allTerms = DepositTerm::all();
        $terms    = [];

        foreach ($allTerms as $term) {
            $terms[] = [
                'id' => $term->id,
                'name' => $term->deposit->name . ' ' . $term->currency->iso . ' ' . $term->period,
            ];
        }

        return ['depositTerms' => collect([['id' => '', 'name' => __('common.choose_please')]])->concat($terms)];
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [
            UserDeposit::ID => 'nullable|regex:/[0-9]+/u',
            UserDeposit::DEPOSIT_TERM_ID => 'required|regex:/[0-9]+/u',
            UserDeposit::USER_ID => 'required|regex:/[0-9]+/u',
            UserDeposit::BALANCE => 'required|between:0,1000000000000',
            UserDeposit::START_DATE => 'required',
            UserDeposit::END_DATE => 'nullable',
        ];
    }


}
