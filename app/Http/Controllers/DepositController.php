<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Deposit;
use App\Scopes\BankScope;
use App\Scopes\DepositScope;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class DepositController
 * @package App\Http\Controllers
 */
class DepositController extends AdminEntityController
{


    /**
     * @param array $parentIds
     * @param array $filter
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getItems(array $parentIds =[], array $filter = null) : \Illuminate\Database\Eloquent\Collection
    {
        $wheres = [];
        if (empty($filter['search']) === false) {
            $wheres[] = ['key' => Deposit::NAME, 'operator' => 'like', 'value' => '%' . $filter['search'] . '%'];
        }

        if (empty($filter['bank']) === false) {
            $wheres[Deposit::BANK_ID] = $filter['bank'];
        }

        if (empty($wheres)) {
            $items = Deposit::withoutGlobalScopes([DepositScope::class, BankScope::class])->with('bank')->get();
        } else {
            $items = Deposit::withoutGlobalScopes([DepositScope::class, BankScope::class])->with('bank')->where($wheres)->get();
        }

        return $items;
    }


    /**
     * @param Request $request
     * @return array
     */
    protected function getCustomViewVariables(Request $request) : array
    {
        $banks = Bank::get([Bank::ID, Bank::SHORT_NAME]);
        return ['banks' => collect([['id' => '', 'short_name' => __('common.choose_please')]])->concat($banks)];
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [
            Deposit::ID => 'nullable|regex:/[0-9]+/u',
            Deposit::BANK_ID => 'required|regex:/[0-9]+/u',
            Deposit::NAME => 'required|max:128',
            Deposit::LINK => 'nullable|max:256',
            Deposit::OPEN_RESTRICTION => 'nullable|max:512',
            Deposit::STATUS => 'required|' . Rule::in(...Deposit::getAllStatuses()),
        ];
    }


}
