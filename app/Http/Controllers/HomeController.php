<?php

namespace App\Http\Controllers;
use App\Bank;
use App\Deposit;
use App\DepositTerm;
use App\DepositTermUpdate;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends AdminEntityController
{

    const TREND = 'trend';


    protected function setEntityName() : void
    {
        $this->entityName = 'depositTerm';
    }


    /**
     * @return string
     */
    protected function getRouterName() : string
    {
        return 'home';
    }


    /**
     * @param Request $request
     * @return array
     */
    protected function getCustomViewVariables(Request $request) : array
    {
        $banks = Bank::get([Bank::ID, Bank::SHORT_NAME]);

        $filter = $request->get(static::FILTER);
        if (empty($filter) || empty($filter[\App\Deposit::BANK_ID])) {
            $deposits = Deposit::get([Deposit::ID, Deposit::NAME]);
        } else {
            $deposits = Deposit::whereIn(Deposit::BANK_ID, $filter[Deposit::BANK_ID])->get();
        }

        $currencies = \App\Currency::all(['id', 'name']);
        $allPeriods = DB::table('deposit_terms')->distinct('period')->get(['period']);

        $periods = [];
        foreach ($allPeriods as $period) {
            $periods[] = $period->period;
        }

        $sort = function($a, $b) {
            $reg = '/^([P]{1})([0-9]+)([DMY]{1})$/';
            preg_match($reg, $a, $firstMatches);
            preg_match($reg, $b, $secondMatches);

            $getDays = function($match) {
                if (empty($match) === false) {
                    $days = 0;
                    if ($match[3] === 'Y') {
                        $days = $match[2] * 365;
                    } else if ($match[3] === 'M') {
                        $days = $match[2] * 30.5;
                    } else if ($match[3] === 'D') {
                        $days = $match[2];
                    }

                    return $days;
                }

                return 0;
            };

            $first  = $getDays($firstMatches);
            $second = $getDays($secondMatches);

            return $first >= $second;
        };

        $trimmedPeriods = array_map('trim', $periods);
        uasort($trimmedPeriods, $sort);
        $sortedPeriods    = array_values($trimmedPeriods);
        $sortedPeriods[0] = __('banks.deposits.poste_restante');

        return [
            'banks' => $banks,
            'deposits' => $deposits,
            'currencies' => $currencies,
            'periods' => $sortedPeriods,
        ];
    }


    /**
     * @return array
     */
    static protected function getValidationArray(): array
    {
        return [];
    }


    /**
     * @return array
     */
    protected function getRelationsForList(): array
    {
        return ['deposit', 'deposit.bank', 'currency', 'depositTermUpdates'];
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request   $request
     * @param mixed ...$parentIds
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function index(Request $request, ...$parentIds) : \Illuminate\View\View
    {
        $filter = $request->get(static::FILTER);
        if (empty($filter)) {
            $filter = $request->query();
        }

        $viewVariables = [];

        try {
            $queryBuilder = $this->prepareFilterQuery($filter);
            $items        = $queryBuilder->paginate(50);

            $standardVariables = $this->loadParents(static::VIEW_FIELD, $parentIds);
            $customVariables   = $this->getCustomViewVariables($request);
            $methodVariables   = [
                static::ROUTER_NAME => $this->getRouterName(),
                static::ITEMS => $items,
                static::FILTER => $filter,
            ];

            $viewVariables = array_merge($standardVariables, $methodVariables, $customVariables);
        } catch (\Exception $e) {
            report($e);
            info('Application was unable to get items or view variables in ' . static::class . '->' . __METHOD__);
            abort(500);
        }

        return view(
            $this->getRouterName() . '.' . static::INDEX,
            $viewVariables
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    protected function trend(Request $request) : \Illuminate\View\View
    {
        $filter = $request->get(static::FILTER);

        $viewVariables = [];

        try {
            $items       = [];
            $stringDates = [];
            $maxRatesAtMoments = [];

            if (empty($filter) === false && empty(array_filter($filter)) === false) {
                $queryBuilder = $this->prepareFilterQuery($filter);
                $items        = $queryBuilder->get();

                /** @var \Carbon\Carbon[] $moments */
                $updateMoments = $this->_getUpdateMoments($items, $filter);

                list($stringDates, $maxRatesAtMoments) = $this->fillUpdates($items, $updateMoments);
            }

            $customVariables = $this->getCustomViewVariables($request);
            $methodVariables = [
                static::ROUTER_NAME => $this->getRouterName(),
                static::ITEMS => $items,
                static::FILTER => $filter,
                'updateDates' => $stringDates,
                'maxRatesAtMoments' => $maxRatesAtMoments,
            ];

            $viewVariables = array_merge($methodVariables, $customVariables);
        } catch (\Exception $e) {
            report($e);
            info('Application was unable to get items or view variables in ' . static::class . '->' . __METHOD__);
            abort(500);
        }

        return view(
            $this->getRouterName() . '.' . static::TREND,
            $viewVariables
        );
    }


    /**
     * @param Collection $depositTerms
     * @param array $moments
     * @return array
     */
    protected function fillUpdates(Collection $depositTerms, array $moments) : array
    {
        $stringDates       = [];
        $maxRatesAtMoments = [];

        $previousDate = null;
        foreach (array_sort($moments) as $moment) {
            /** @var \Carbon\Carbon $moment */
            $date = $moment->format('d.m.Y');
            $stringDates[] = $date;
            $maxRatesAtMoments[$date] = 0;

            foreach ($depositTerms as $term) {
                /** @var DepositTerm $term*/
                $rateAtMoment = $term->getAttribute(DepositTerm::RATE);

                /** @var \App\DepositTerm $termAtMoment */
                $termAtMoment = $term->moment($moment)->find($term->getAttribute(DepositTerm::ID));
                if ($termAtMoment !== null) {
                    $rateAtMoment = $termAtMoment->getAttribute(DepositTerm::RATE);
                }

                $term->setAttribute($date, $rateAtMoment);

                if ($maxRatesAtMoments[$date] < $rateAtMoment) {
                    $maxRatesAtMoments[$date] = $rateAtMoment;
                }

                $previousRate = $term->getAttribute($previousDate);
                if ($previousDate !== null) {
                    $cellVariants = $term->getAttribute('_cellVariants');
                    if ((float)$previousRate > (float)$rateAtMoment) {
                        $cellVariants[$date] = 'warning';
                    } else if ((float)$previousRate < (float)$rateAtMoment) {
                        $cellVariants[$date] = 'info';
                    }

                    $term->setAttribute('_cellVariants', $cellVariants);
                }
            }

            $previousDate = $date;
        }

        return [$stringDates, $maxRatesAtMoments];
    }


    /**
     * @param Collection $items
     * @param array $filter
     * @return array
     */
    private function _getUpdateMoments(Collection $items, array $filter) : array
    {
        $moments = [];
        foreach ($items as $term) {
            /** @var DepositTerm $term*/
            $rateUpdates = $term->depositTermUpdates->where(DepositTerm::RATE, '!==', null);
            if (empty($filter['from_date']) === false) {
                $rateUpdates = $rateUpdates->where(DepositTermUpdate::START_DATE, '>=', $filter['from_date']);
            }

            if (empty($filter['to_date']) === false) {
                $rateUpdates = $rateUpdates->where(DepositTermUpdate::START_DATE, '<=', $filter['to_date']);
            }

            foreach ($rateUpdates as $update) {
                /** @var DepositTermUpdate $update*/
                $dateSql = $update->getAttribute(DepositTermUpdate::START_DATE);
                $date    = \DateTime::createFromFormat('Y-m-d h:i:s', $dateSql);

                $moments[] = Carbon::instance($date->setTime(23, 59, 59));
            }
        }

        $moments[] = Carbon::create();

        return $moments;
    }


    /**
     * @param array $filter
     * @return DepositTerm|\Illuminate\Database\Eloquent\Builder
     */
    protected function prepareFilterQuery($filter) : \Illuminate\Database\Eloquent\Builder
    {
        $queryBuilder = DepositTerm::with($this->getRelationsForList());

        if (empty($filter) === false && empty(array_filter($filter)) === false) {
            if (empty($filter['search']) === false) {
                $queryBuilder->where(['key' => DepositTerm::NAME, 'operator' => 'like', 'value' => '%' . $filter['search'] . '%']);
            }

            if (empty($filter['min_rate']) === false) {
                $queryBuilder->where(['key' => DepositTerm::RATE, 'operator' => '>=', 'value' => $filter['min_rate']]);
            }

            if (empty($filter['max_rate']) === false) {
                $queryBuilder->where(['key' => DepositTerm::RATE, 'operator' => '<=', 'value' => $filter['max_rate']]);
            }

            if (empty($filter[Deposit::BANK_ID]) === false) {
                $queryBuilder->whereHas(
                    'deposit',
                    function (\ProAi\Versioning\Builder $q) use ($filter) {
                        $q->whereIn(Deposit::BANK_ID, $filter[Deposit::BANK_ID]);
                    }
                );
            }

            $filterWherePairs = [
                DepositTerm::CURRENCY_ID,
                DepositTerm::IS_IRREVOCABLE_FIELD,
                DepositTerm::CAPITALIZATION_ALLOWED,
                DepositTerm::PERCENT_WITHDRAWAL_ALLOWED,
                DepositTerm::MIN_SUM,
                DepositTerm::MAX_SUM,
                DepositTerm::PROLONGATION,
            ];

            foreach ($filterWherePairs as $field) {
                $value = isset($filter[$field]) ? $filter[$field] : null;
                if ($value !== null && $value !== '') {
                    $queryBuilder->where($field, $value);
                }
            }

            $filterWhereInPairs = [
                DepositTerm::DEPOSIT_ID,
                DepositTerm::RATE_TYPE,
                DepositTerm::REFILLING_TYPE,
                DepositTerm::INTERNET,
                DepositTerm::CALCULATION_OF_INTEREST_PERIOD,
                DepositTerm::PERIOD,
            ];

            foreach ($filterWhereInPairs as $field) {
                $value = (isset($filter[$field])) ? $filter[$field] : null;
                if ($value !== null && $value !== '') {
                    $queryBuilder->whereIn($field, $value);
                    $whereIns[$field] = $value;
                }
            }
        }

        return $queryBuilder;
    }


}