<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\DepositTerm;
use App\DepositTermUpdate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class DepositTermUpdateController
 * @package App\Http\Controllers
 */
class DepositTermUpdateController extends AdminEntityController
{
    const MASS_UPDATE = 'mass-update';

    protected $parentEntities = [
        'deposit',
        'depositTerm' => [
            self::ROUTE_FIELD => 'term',
        ]
    ];


    /**
     * @param Model $model
     * @param Request $request
     * @param array $parentIds
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function saveEntity(Model $model, Request $request, array $parentIds): \Illuminate\Http\RedirectResponse
    {
        $data = $this->validate($request, static::getValidationArray());
        $this->applyUpdate(last($parentIds), $data);
        return parent::saveEntity($model, $request, $parentIds);
    }


    /**
     * @param int $depositTermId
     * @param array $data
     */
    protected function applyUpdate(int $depositTermId, array $data) : void
    {
        $startDate  = \DateTime::createFromFormat('Y-m-d', $data[\App\DepositTermUpdate::START_DATE])->setTime(0,0);
        $informDate = \DateTime::createFromFormat('Y-m-d', $data[\App\DepositTermUpdate::INFORM_DATE])->setTime(0,0);
        if ($startDate >= $informDate) {
            /** @var \App\DepositTerm $term */
            $term   = DepositTerm::findOrFail($depositTermId);
            $fields = $term->getVersionedAttributeNames();
            foreach ($fields as $field) {
                if (isset($data[$field])) {
                    $term->setAttribute($field, $data[$field]);
                }
            }

            $term->setAttribute(\App\DepositTerm::UPDATED_AT, $startDate);
            $term->save();
        }
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        /** @noinspection SpellCheckingInspection */
        return [
            DepositTermUpdate::ID => 'nullable|regex:/[0-9]+/u',
            DepositTermUpdate::DEPOSIT_TERM_ID => 'nullable|regex:/[0-9]+/u',
            DepositTermUpdate::INFORM_DATE => 'required',
            DepositTermUpdate::START_DATE => 'required',
            DepositTermUpdate::DEPOSIT_FROM_DATE => 'nullable',
            DepositTermUpdate::DEPOSIT_TO_DATE => 'nullable',
            DepositTerm::IRREDUCIBLE_BALANCE => 'nullable|between:0,1000000',
            DepositTerm::FULL_REVOKE_CONDITIONS => 'nullable',
            DepositTerm::FULL_REVOKE_PERCENT => 'nullable',
            DepositTerm::PARTIAL_REVOKE_CONDITIONS => 'nullable',
            DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT => 'nullable',
            DepositTerm::CALCULATION_OF_INTEREST_PERIOD => 'nullable|' . Rule::in('', ...DepositTerm::getAllCalculationPeriods()),
            DepositTerm::CAPITALIZATION_ALLOWED => Rule::in('', ...DepositTerm::getAllCapitalizationStatuses()),
            DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => Rule::in('', ...DepositTerm::getAllPercentWithdrawalStatuses()),
            DepositTerm::RATE_TYPE => 'nullable|' . Rule::in('', ...DepositTerm::getAllRateTypes()),
            DepositTerm::RATE => 'nullable|between:0.99,99',
            DepositTerm::FIXED_RATE_PERIOD => 'nullable|regex:/[pdmyPDMY0-9]+/u',
            DepositTerm::REFILLING_TYPE => 'nullable|' . Rule::in('', ...DepositTerm::getAllRefillingTypes()),
            DepositTerm::REFILLING_PERIOD => 'nullable|regex:/[pdmyPDMY0-9]+/u',
            DepositTerm::REFILLING_MIN_SUM => 'nullable|between:0,1000',
            DepositTerm::PROLONGATION => 'nullable|between:0,99',
            DepositTerm::PROLONGATION_CONDITIONS => 'nullable',
            DepositTerm::MIN_SUM => 'nullable|between:0,1000000',
            DepositTerm::MAX_SUM => 'nullable|between:0,1000000000000',
            DepositTerm::THIRD_PERSON => 'nullable|' . Rule::in(...DepositTerm::getAllThirdPersonStatuses()),
            DepositTerm::LINK => 'nullable|max:256',
            DepositTerm::RESTRICTION => 'nullable|max:256',
        ];
    }


    /**
     * @return array
     */
    static protected function getMassUpdateValidationArray() : array
    {
        $validationArray = static::getValidationArray();
        $validationArray[DepositTermUpdate::DEPOSIT_TERM_ID] = 'required';

        return $validationArray;
    }


    /**
     * @param Deposit $deposit
     * @return \Illuminate\View\View
     */
    public function massUpdate(Deposit $deposit) : \Illuminate\View\View
    {
        $router = $this->getRouterName();

        $viewVariables = [];

        try {
            $viewVariables = [
                static::ROUTER_NAME => $this->getRouterName(),
                static::MODEL => new \App\DepositTermUpdate(),
                static::VALIDATION => static::getMassUpdateValidationArray(),
                'deposit' => $deposit,
            ];
        } catch (\Exception $e) {
            report($e);
            info('Application was unable to get view variables in ' . static::class . '->' . __METHOD__);
            abort(500);
        }

        return view(
            $router . '.' . static::MASS_UPDATE,
            $viewVariables
        );
    }


    /**
     * @param Request $request
     * @param int $depositId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function massUpdateStore(Request $request, $depositId) : \Illuminate\Http\RedirectResponse
    {
        $data    = $this->validate($request, static::getMassUpdateValidationArray());
        $deposit = \App\Deposit::findOrFail($depositId);

        foreach ($data[\App\DepositTermUpdate::DEPOSIT_TERM_ID] as $termId) {
            $this->applyUpdate($termId, $data);

            try {
                $newUpdate = new \App\DepositTermUpdate;
                $model     = $this->fillModel($newUpdate, array_merge($data, [\App\DepositTermUpdate::DEPOSIT_TERM_ID => $termId]));

                $routerParams = [
                    'deposit' => $deposit,
                    'term' => \App\DepositTerm::findOrFail($termId),
                ];
                $this->associateDependencies($model, $routerParams);

                $model->save();
            } catch (\Exception $e) {
                report($e);
                info('Application was unable to store entity ' . static::class . ' to database. ' . print_r($model->getAttributes(), true));
                abort(500);
            }
        }

        return redirect()->route('deposit.term.index', ['deposit' => $deposit]);
    }


}