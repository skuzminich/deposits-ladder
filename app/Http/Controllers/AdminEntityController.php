<?php /** @noinspection PhpUnusedParameterInspection */
namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class AdminEntityController
 *
 * @package App\Http\Controllers
 */
abstract class AdminEntityController extends EntityController
{


    /**
     * Display a listing of the resource.
     *
     * @param Request   $request
     * @param mixed ...$parentIds
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function index(Request $request, ...$parentIds) : \Illuminate\View\View
    {
        $filter = $request->get(static::FILTER);

        $viewVariables = [];

        try {
            $items = $this->getItems($parentIds, $filter);

            $standardVariables = $this->loadParents(static::VIEW_FIELD, $parentIds);
            $customVariables   = $this->getCustomViewVariables($request);
            $methodVariables   = [
                static::ROUTER_NAME => $this->getRouterName(),
                static::ITEMS => $items,
                static::FILTER => $filter,
            ];

            $viewVariables = array_merge($standardVariables, $methodVariables, $customVariables);
        } catch (\Exception $e) {
            report($e);
            info('Application was unable to get items or view variables in ' . static::class . '->' . __METHOD__);
            abort(500);
        }

        return view(
            $this->getRouterName() . '.' . static::INDEX,
            $viewVariables
        );
    }


    /**
     * @param array $parentIds
     * @param array|null $filter
     * @return \Illuminate\Database\Eloquent\Collection|Model[]|\Illuminate\Support\Collection
     */
    protected function getItems(array $parentIds = [], array $filter = null) : \Illuminate\Database\Eloquent\Collection
    {
        /** @var Model $modelClassName */
        $modelClassName = $this->getModelClassName();
        if (empty($parentIds)) {
            $items = $modelClassName::all();
        } else {
            $entity      = end($this->parentEntities);
            $parentId    = end($parentIds);
            $parentField = $entity[static::DB_FIELD] . '_' . self::ID;

            $where[$parentField] = $parentId;

            $items = $modelClassName::with($this->getRelationsForList())->where($where)->get();
        }

        return $items;
    }


    /**
     * @return array
     */
    protected function getRelationsForList() : array
    {
        $entity = end($this->parentEntities);
        $with   = [$entity[static::DEPENDENCY_METHOD]];

        return $with;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param array $args
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function create(Request $request, ...$args) : \Illuminate\View\View
    {
        /** @var Model $modelClass */
        $modelClassName = $this->getModelClassName();
        $model = new $modelClassName;

        return $this->_getEditView($model, $args, $request);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param array $args
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function edit(Request $request, ...$args) : \Illuminate\View\View
    {
        $modelId = array_pop($args);
        /** @var Model $modelClass */
        $modelClass = $this->getModelClassName();
        $model      = $modelClass::findOrFail($modelId);

        return $this->_getEditView($model, $args, $request);
    }


    /**
     * @param Model $model
     * @param array $parentIds
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function _getEditView(Model $model, array $parentIds, Request $request) : \Illuminate\View\View
    {
        $router = $this->getRouterName();

        $viewVariables = [];

        try {
            $standardVariables = $this->loadParents(static::VIEW_FIELD, $parentIds);
            $customVariables   = $this->getCustomViewVariables($request);

            $methodVariables = [
                static::ROUTER_NAME => $this->getRouterName(),
                static::MODEL => $model,
                static::VALIDATION => static::getValidationArray(),
            ];

            $viewVariables = array_merge($standardVariables, $methodVariables, $customVariables);
        } catch (\Exception $e) {
            report($e);
            info('Application was unable to get view variables in ' . static::class . '->' . __METHOD__);
            abort(500);
        }

        return view(
            $router . '.' . static::EDIT,
            $viewVariables
        );
    }


    /**
     * @param Request $request
     * @return array
     */
    protected function getCustomViewVariables(Request $request) : array
    {
        return [];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param array $parentIds
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, ...$parentIds) : \Illuminate\Http\RedirectResponse
    {
        /** @var Model $modelClass */
        $modelClass = $this->getModelClassName();
        $model      = new $modelClass;

        return $this->saveEntity($model, $request, $parentIds);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array $parentIds
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ...$parentIds) : \Illuminate\Http\RedirectResponse
    {
        /** @var Model $modelClass */
        $modelClass = $this->getModelClassName();
        $modelId    = array_pop($parentIds);
        $model      = $modelClass::findOrFail($modelId);

        return $this->saveEntity($model, $request, $parentIds);
    }


    /**
     * Fills the model and saves it to storage
     *
     * @param Model $model
     * @param Request $request
     * @param array $parentIds
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function saveEntity(Model $model, Request $request, array $parentIds) : \Illuminate\Http\RedirectResponse
    {
        $routerParams = [];

        try {
            $data  = $this->validate($request, static::getValidationArray());
            $model = $this->fillModel($model, $data);

            $routerParams = $this->loadParents(static::ROUTE_FIELD, $parentIds);
            $this->associateDependencies($model, $routerParams);

            $model->save();
        } catch (\Exception $e) {
            report($e);
            info('Application was unable to store entity ' . static::class . ' to database. ' . print_r($model->getAttributes(), true));
            abort(500);
        }

        return redirect()->route($this->getRouterName() . '.' . static::INDEX, $routerParams);
    }


    /**
     * @param Model $model
     * @param array $routeParams
     * @return Model
     */
    protected function associateDependencies(Model &$model, array $routeParams)
    {
        $parentsCount = count($this->parentEntities);
        for ($i = 0; $i < $parentsCount; $i++) {
            $dependency = $this->parentEntities[$i][self::DEPENDENCY_METHOD];
            $route      = $this->parentEntities[$i][self::ROUTE_FIELD];

            if (method_exists($model, $dependency)) {
                $model->$dependency()->associate($routeParams[$route]);
            }
        }

        return null;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @return null
     * @throws \Exception
     */
    protected function destroy()
    {
        $args    = func_get_args();
        $modelId = last($args);
        /** @var Model $modelClass */
        $modelClass = $this->getModelClassName();
        $model      = $modelClass::findOrFail($modelId);
        $model->delete();

        return null;
    }


}