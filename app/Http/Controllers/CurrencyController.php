<?php

namespace App\Http\Controllers;

use App\Currency;

/**
 * Class CurrencyController
 * @package App\Http\Controllers
 */
class CurrencyController extends AdminEntityController
{


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [
            Currency::ID => 'nullable|regex:/[0-9]+/u',
            Currency::NAME => 'required|max:32:',
            Currency::ISO => 'required|max:3',
            Currency::SYMBOL => 'required|max:2',
        ];
    }


}
