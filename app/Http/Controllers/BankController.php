<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Scopes\BankScope;
use Illuminate\Validation\Rule;

/**
 * Class BankController
 * @package App\Http\Controllers
 */
class BankController extends AdminEntityController
{


    /**
     * @param array $parentIds
     * @param array|null $filter
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getItems(array $parentIds = [], array $filter = null) : \Illuminate\Database\Eloquent\Collection
    {
        $items = Bank::withoutGlobalScope(BankScope::class)->get();

        return $items;
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [
            Bank::ID => 'nullable|regex:/[0-9]+/u',
            Bank::FULL_NAME => 'required|max:128',
            Bank::SHORT_NAME => 'required|max:64',
            Bank::LINK => 'required|max:64',
            Bank::STATUS  => 'required|' . Rule::in(...Bank::getAllStatuses()),
        ];
    }


}