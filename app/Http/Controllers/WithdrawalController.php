<?php

namespace App\Http\Controllers;

use App\Withdrawal;

/**
 * Class WithdrawalController
 * @package App\Http\Controllers
 */
class WithdrawalController extends AdminEntityController
{

    /** @var array */
    protected $parentEntities = [
        'user',
        'userDeposit' => [
            self::ROUTE_FIELD => 'deposit',
        ]
    ];


    /**
     * @return string
     */
    protected function getRouterName() : string
    {
        return 'user.deposit.withdrawal';
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [
            Withdrawal::ID => 'nullable|regex:/[0-9]+/u',
            Withdrawal::USER_DEPOSIT_IT => 'required|regex:/[0-9]+/u',
            Withdrawal::AMOUNT => 'required|between:0,1000000000000',
            Withdrawal::WITHDRAWAL_DATE => 'required',
        ];
    }


}