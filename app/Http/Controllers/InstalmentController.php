<?php

namespace App\Http\Controllers;

use App\Instalment;

/**
 * Class InstalmentController
 * @package App\Http\Controllers
 */
class InstalmentController extends AdminEntityController
{

    /** @var array */
    protected $parentEntities = [
        'user',
        'userDeposit' => [
            self::ROUTE_FIELD => 'deposit',
        ]
    ];


    /**
     * @return string
     */
    protected function getRouterName() : string
    {
        return 'user.deposit.instalment';
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [
            Instalment::ID => 'nullable|regex:/[0-9]+/u',
            Instalment::USER_DEPOSIT_IT => 'required|regex:/[0-9]+/u',
            Instalment::AMOUNT => 'required|between:0,1000000000000',
            Instalment::DEPOSIT_DATE => 'required',
        ];
    }


}
