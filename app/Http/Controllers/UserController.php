<?php

namespace App\Http\Controllers;

use App\User;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends AdminEntityController
{


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [
            User::ID => 'nullable|regex:/[0-9]+/u',
            User::NAME => 'required|max:255',
            User::EMAIL => 'required|max:255',
        ];
    }


}
