<?php

namespace App\Http\Controllers\Api;

use App\Withdrawal;

/**
 * Class WithdrawalController
 * @package App\Http\Controllers\Api
 */
class WithdrawalController extends ApiEntityController
{
    protected $parentEntities = [
        'user',
        'userDeposit' => [
            self::ROUTE_FIELD => 'deposit',
        ]
    ];

    protected $publicColumns = [Withdrawal::ID, Withdrawal::USER_DEPOSIT_IT, Withdrawal::AMOUNT, Withdrawal::WITHDRAWAL_DATE];


    /**
     * @return string
     */
    protected function getRouterName() : string
    {
        return 'users.deposits.withdrawals';
    }


}
