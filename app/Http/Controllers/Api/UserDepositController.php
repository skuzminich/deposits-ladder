<?php

namespace App\Http\Controllers\Api;

use App\UserDeposit;

/**
 * Class UserDepositController
 * @package App\Http\Controllers\Api
 */
class UserDepositController extends ApiEntityController
{
    protected $parentEntities = ['user'];

    protected $publicColumns = [UserDeposit::ID, UserDeposit::DEPOSIT_TERM_ID, UserDeposit::USER_ID,
        UserDeposit::BALANCE, UserDeposit::START_DATE, UserDeposit::END_DATE];
}
