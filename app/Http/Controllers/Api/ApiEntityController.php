<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\EntityController;
use App\Http\Resources\Collection;
use App\Http\Resources\Resource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Controller
 * @package App\Http\Controllers\Api
 */
class ApiEntityController extends EntityController
{

    protected $publicColumns = ['*'];


    /** @noinspection PhpMissingParentConstructorInspection */


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->setEntityName();
        $this->setParentEntityVariable();
    }


    /**
     * @return string
     */
    protected function getResourceClassName() : string
    {
        $nameSpace = current(explode('\\', static::class));
        return '\\' . $nameSpace . '\\Http\\Resources\\' . ucfirst($this->entityName);
    }


    /**
     * @return string
     */
    protected function getRouterName(): string
    {
        return Str::snake($this->entityName, 's.') . 's';
    }


    /**
     * Display a listing of the resource.
     *
     * @param array $parentIds
     * @return Collection
     */
    public function index(...$parentIds) : Collection
    {
        /** @var Model $modelClass */
        $modelClass = $this->getModelClassName();
        $models     = $modelClass::all($this->publicColumns);

        $routerParams = $this->loadParents(static::ROUTE_FIELD, $parentIds);
        $self = route($this->getRouterName() . '.' . static::INDEX, $routerParams);

        $collection = new Collection($models, $self, $this->getResourceClassName());

        return $collection;
    }


    /**
     * Display the specified resource.
     *
     * @param  array $parentIds
     * @return Resource
     */
    public function show(...$parentIds) : Resource
    {
        $modelId = array_pop($parentIds);

        /** @var Model $modelClass */
        $modelClass = $this->getModelClassName();
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        $model = $modelClass::findOrFail($modelId, $this->publicColumns);

        $resourceClass = $this->getResourceClassName();
        /** @var Resource $resource */
        $resource = new $resourceClass($model);

        return $resource;
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        return [];
    }


}
