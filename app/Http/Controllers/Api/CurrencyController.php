<?php
namespace App\Http\Controllers\Api;

use App\Currency;

/**
 * Class CurrencyController
 * @package App\Http\Controllers\Api
 */
class CurrencyController extends ApiEntityController
{
    protected $publicColumns = [Currency::ID, Currency::NAME, Currency::ISO, Currency::SYMBOL];
}