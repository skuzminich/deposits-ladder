<?php

namespace App\Http\Controllers\Api;

use App\Bank;

/**
 * Class BankController
 * @package App\Http\Controllers\Api
 */
class BankController extends ApiEntityController
{
    protected $publicColumns = [Bank::ID, Bank::SHORT_NAME, Bank::FULL_NAME, Bank::LINK, Bank::STATUS];
}
