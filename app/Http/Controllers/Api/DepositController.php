<?php

namespace App\Http\Controllers\Api;

use App\Deposit;

/**
 * Class DepositController
 * @package App\Http\Controllers\Api
 */
class DepositController extends ApiEntityController
{
    protected $publicColumns = [Deposit::ID, Deposit::BANK_ID, Deposit::NAME, Deposit::LINK, Deposit::STATUS, Deposit::OPEN_RESTRICTION];
}
