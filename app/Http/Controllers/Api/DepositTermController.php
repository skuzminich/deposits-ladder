<?php

namespace App\Http\Controllers\Api;

use App\DepositTerm;

/**
 * Class DepositTermController
 * @package App\Http\Controllers\Api
 */
class DepositTermController extends ApiEntityController
{
    protected $parentEntities = ['deposit'];

    protected $publicColumns = [DepositTerm::ID, DepositTerm::DEPOSIT_ID, DepositTerm::CURRENCY_ID, DepositTerm::PERIOD,
        DepositTerm::IS_IRREVOCABLE_FIELD, DepositTerm::NAME, DepositTerm::CALCULATION_OF_INTEREST_PERIOD,
        DepositTerm::CAPITALIZATION_ALLOWED, DepositTerm::PERCENT_WITHDRAWAL_ALLOWED, DepositTerm::RATE_TYPE, DepositTerm::REFILLING_TYPE,
        DepositTerm::INTERNET, DepositTerm::PROLONGATION, DepositTerm::MIN_SUM, DepositTerm::IRREDUCIBLE_BALANCE,
        DepositTerm::FULL_REVOKE_CONDITIONS, DepositTerm::FULL_REVOKE_PERCENT, DepositTerm::PARTIAL_REVOKE_CONDITIONS,
        DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT, DepositTerm::FIXED_RATE_PERIOD, DepositTerm::REFILLING_PERIOD,
        DepositTerm::REFILLING_MIN_SUM, DepositTerm::PROLONGATION_CONDITIONS, DepositTerm::MAX_SUM, DepositTerm::THIRD_PERSON,
        DepositTerm::LINK, DepositTerm::RESTRICTION];
}
