<?php

namespace App\Http\Controllers\Api;

use App\Instalment;

/**
 * Class InstalmentController
 * @package App\Http\Controllers\Api
 */
class InstalmentController extends ApiEntityController
{
    protected $parentEntities = [
        'user',
        'userDeposit' => [
            self::ROUTE_FIELD => 'deposit',
        ]
    ];

    protected $publicColumns = [Instalment::ID, Instalment::USER_DEPOSIT_IT, Instalment::AMOUNT, Instalment::DEPOSIT_DATE];


    /**
     * @return string
     */
    protected function getRouterName() : string
    {
        return 'users.deposits.instalments';
    }


}