<?php

namespace App\Http\Controllers\Api;

use App\DepositTerm;
use App\DepositTermUpdate;

/**
 * Class DepositTermUpdateController
 * @package App\Http\Controllers\Api
 */
class DepositTermUpdateController extends ApiEntityController
{
    protected $parentEntities = [
        'deposit',
        'depositTerm' => [
            self::ROUTE_FIELD => 'term',
        ]
    ];

    protected $publicColumns = [DepositTermUpdate::ID, DepositTermUpdate::INFORM_DATE, DepositTermUpdate::DEPOSIT_TERM_ID,
        DepositTermUpdate::START_DATE, DepositTermUpdate::DEPOSIT_FROM_DATE, DepositTermUpdate::DEPOSIT_TO_DATE,
        DepositTerm::CALCULATION_OF_INTEREST_PERIOD, DepositTerm::CAPITALIZATION_ALLOWED, DepositTerm::PERCENT_WITHDRAWAL_ALLOWED,
        DepositTerm::RATE_TYPE, DepositTerm::REFILLING_TYPE, DepositTerm::PROLONGATION, DepositTerm::MIN_SUM,
        DepositTerm::IRREDUCIBLE_BALANCE, DepositTerm::FULL_REVOKE_CONDITIONS, DepositTerm::FULL_REVOKE_PERCENT,
        DepositTerm::PARTIAL_REVOKE_CONDITIONS, DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT, DepositTerm::FIXED_RATE_PERIOD,
        DepositTerm::REFILLING_PERIOD, DepositTerm::REFILLING_MIN_SUM, DepositTerm::PROLONGATION_CONDITIONS, DepositTerm::MAX_SUM,
        DepositTerm::THIRD_PERSON, DepositTerm::LINK, DepositTerm::RESTRICTION];
}
