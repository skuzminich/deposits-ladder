<?php

namespace App\Http\Controllers;

use App\Currency;
use App\DepositTerm;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class DepositTermController
 * @package App\Http\Controllers
 */
class DepositTermController extends AdminEntityController
{

    protected $parentEntities = ['deposit'];

    /** @var array  */
    const CHECKBOXES = [DepositTerm::IS_IRREVOCABLE_FIELD, DepositTerm::CAPITALIZATION_ALLOWED, DepositTerm::PERCENT_WITHDRAWAL_ALLOWED,
        DepositTerm::THIRD_PERSON];


    /**
     * @param Request $request
     * @return array
     */
    protected function getCustomViewVariables(Request $request) : array
    {
        $currencies = Currency::all(['id', 'name']);
        return ['currencies' => collect([['id' => '', 'name' => __('common.choose_please')]])->concat($currencies)];
    }


    /**
     * @return array
     */
    protected function getRelationsForList(): array
    {
        return ['deposit', 'deposit.bank', 'currency'];
    }


    /**
     * @return array
     */
    static protected function getValidationArray() : array
    {
        /** @noinspection SpellCheckingInspection */
        return [
            DepositTerm::ID => 'nullable|regex:/[0-9]+/u',
            DepositTerm::CURRENCY_ID => 'required',
            DepositTerm::PERIOD => 'required|regex:/[pdmyPDMY0-9]+/u',
            DepositTerm::IS_IRREVOCABLE_FIELD => Rule::in(...DepositTerm::getAllIrrevocableStatuses()),
            DepositTerm::NAME => 'nullable|max:128',
            DepositTerm::IRREDUCIBLE_BALANCE => 'nullable|between:0,1000000',
            DepositTerm::FULL_REVOKE_CONDITIONS => 'nullable',
            DepositTerm::FULL_REVOKE_PERCENT => 'nullable',
            DepositTerm::PARTIAL_REVOKE_CONDITIONS => 'nullable',
            DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT => 'nullable',
            DepositTerm::CALCULATION_OF_INTEREST_PERIOD => 'required|' . Rule::in(...DepositTerm::getAllCalculationPeriods()),
            DepositTerm::CAPITALIZATION_ALLOWED => Rule::in(...DepositTerm::getAllCapitalizationStatuses()),
            DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => Rule::in(...DepositTerm::getAllPercentWithdrawalStatuses()),
            DepositTerm::RATE_TYPE => 'required|' . Rule::in(...DepositTerm::getAllRateTypes()),
            DepositTerm::RATE => 'required|between:0.99,99',
            DepositTerm::FIXED_RATE_PERIOD => 'nullable|regex:/[pdmyPDMY0-9]+/u',
            DepositTerm::REFILLING_TYPE => 'required|' . Rule::in(...DepositTerm::getAllRefillingTypes()),
            DepositTerm::REFILLING_PERIOD => 'nullable|regex:/[pdmyPDMY0-9]+/u',
            DepositTerm::REFILLING_MIN_SUM => 'nullable|between:0,1000',
            DepositTerm::INTERNET => 'required',
            DepositTerm::PROLONGATION => 'required|between:0,99',
            DepositTerm::PROLONGATION_CONDITIONS => 'nullable',
            DepositTerm::MIN_SUM => 'required|between:0,1000000',
            DepositTerm::MAX_SUM => 'nullable|between:0,1000000000000',
            DepositTerm::THIRD_PERSON => Rule::in(...DepositTerm::getAllThirdPersonStatuses()),
            DepositTerm::LINK => 'nullable|max:256',
            DepositTerm::RESTRICTION => 'nullable|max256',
        ];
    }


}
