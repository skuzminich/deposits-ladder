<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * @package App
 *
 * @property integer $id
 * @property string $name
 * @property string $iso
 * @property string $symbol
 * @property DepositTerm[] $depositTerms
 */
class Currency extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * @var string
     */
    protected $keyType = 'integer';

    /** @var array  */
    protected $fillable = [self::NAME, self::ISO, self::SYMBOL];

    /** FIELDS NAMES */
    const ID     = 'id';
    const NAME   = 'name';
    const ISO    = 'iso';
    const SYMBOL = 'symbol';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function depositTerms() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\DepositTerm');
    }


}