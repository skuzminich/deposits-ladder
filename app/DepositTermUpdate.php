<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ProAI\Versioning\SoftDeletes;

/**
 * Class DepositTermUpdate
 * @package App
 *
 * @property int $id
 * @property int $deposit_term_id
 * @property string $start_date
 * @property string $finish_date
 * @property float $irreducible_balance
 * @property string $full_revoke_conditions
 * @property string $partial_revoke_conditions
 * @property float $full_revoke_percent
 * @property float $partial_revoke_sanctions_percent
 * @property integer $calculation_of_interest_period
 * @property integer $capitalization_allowed
 * @property integer $percent_withdrawal_allowed
 * @property integer $rate_type
 * @property float $rate
 * @property string $fixed_rate_period
 * @property integer $refilling_type
 * @property string $refilling_period
 * @property integer $prolongation
 * @property string $prolongation_conditions
 * @property float $min_sum
 * @property float $max_sum
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property DepositTerm $depositTerm
 */
class DepositTermUpdate extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    /** @var array  */
    protected $fillable = [self::INFORM_DATE, self::START_DATE, DepositTerm::IRREDUCIBLE_BALANCE, self::DEPOSIT_FROM_DATE,
        self::DEPOSIT_TO_DATE, DepositTerm::FULL_REVOKE_CONDITIONS, DepositTerm::PARTIAL_REVOKE_CONDITIONS,
        DepositTerm::FULL_REVOKE_PERCENT, DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT, DepositTerm::CALCULATION_OF_INTEREST_PERIOD,
        DepositTerm::CAPITALIZATION_ALLOWED,        DepositTerm::PERCENT_WITHDRAWAL_ALLOWED, DepositTerm::RATE_TYPE, DepositTerm::RATE,
        DepositTerm::FIXED_RATE_PERIOD, DepositTerm::REFILLING_TYPE, DepositTerm::REFILLING_PERIOD, DepositTerm::REFILLING_MIN_SUM,
        DepositTerm::PROLONGATION, DepositTerm::PROLONGATION_CONDITIONS, DepositTerm::MIN_SUM, DepositTerm::MAX_SUM,
        DepositTerm::THIRD_PERSON, DepositTerm::LINK, DepositTerm::RESTRICTION, self::CREATED_AT, self::UPDATED_AT, self::DELETED_AT];

    /** FIELDS NAMES */
    const ID = 'id';
    const DEPOSIT_TERM_ID   = 'deposit_term_id';
    const INFORM_DATE       = 'inform_date';
    const START_DATE        = 'start_date';
    const DEPOSIT_FROM_DATE = 'deposit_from_date';
    const DEPOSIT_TO_DATE   = 'deposit_to_date';
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'updated_at';
    const DELETED_AT        = 'deleted_at';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function depositTerm() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\DepositTerm');
    }


}