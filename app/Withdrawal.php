<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ProAI\Versioning\SoftDeletes;
use ProAI\Versioning\Versionable;

/**
 * Class Withdrawal
 * @package App
 *
 * @property int $id
 * @property int $user_deposit_id
 * @property int $users_id
 * @property float $amount
 * @property string $withdrawal_date
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property UserDeposit $userDeposit
 * @property User $user
 */
class Withdrawal extends Model
{
    use Versionable, SoftDeletes;

    public $timestamps = true;

    /** @var array  */
    protected $fillable = [self::USER_DEPOSIT_IT, self::CREATED_AT, self::UPDATED_AT, self::DELETED_AT];

    /** @var array  */
    protected $versioned = [self::AMOUNT, self::WITHDRAWAL_DATE, self::UPDATED_AT, self::DELETED_AT];

    /** FIELDS NAMES */
    const ID = 'id';
    const USER_DEPOSIT_IT = 'user_deposit_id';
    const AMOUNT = 'amount';
    const WITHDRAWAL_DATE = 'withdrawal_date';
    const CREATED_AT      = 'created_at';
    const UPDATED_AT      = 'updated_at';
    const DELETED_AT      = 'deleted_at';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDeposit() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\UserDeposit');
    }


}