<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <b-nav pills>
                        <b-nav-item-dropdown id="dropdown_home" text="{{ __('common.home') }}">
                            <b-dropdown-item href="/home/index">@lang('banks.deposits.deposit_search')</b-dropdown-item>
                            <b-dropdown-item href="/home/trend">@lang('banks.deposits.rates_trend')</b-dropdown-item>
                        </b-nav-item-dropdown>
                        <b-nav-item-dropdown id="dropdown_banks" text="{{ __('banks.list') }}">
                            <b-dropdown-item href="/bank">@lang('banks.list')</b-dropdown-item>
                            <b-dropdown-item href="/bank/create">@lang('banks.create')</b-dropdown-item>
                        </b-nav-item-dropdown>

                        <b-nav-item-dropdown  id="dropdown_deposits" text="{{ __('banks.deposits.list') }}">
                            <b-dropdown-item href="/deposit">@lang('banks.deposits.list')</b-dropdown-item>
                            <b-dropdown-item href="/deposit/create">@lang('banks.deposits.create')</b-dropdown-item>
                        </b-nav-item-dropdown>

                        <b-nav-item-dropdown id="dropdown_users" text="{{ __('banks.users.list') }}">
                            <b-dropdown-item href="/user">@lang('banks.users.list')</b-dropdown-item>
                        </b-nav-item-dropdown>

                        <b-nav-item-dropdown id="dropdown_currencies" text="{{ __('banks.currencies.list') }}">
                            <b-dropdown-item href="/currency">@lang('banks.currencies.list')</b-dropdown-item>
                            <b-dropdown-item href="/currency/create">@lang('banks.currencies.create')</b-dropdown-item>
                        </b-nav-item-dropdown>
                    </b-nav>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
