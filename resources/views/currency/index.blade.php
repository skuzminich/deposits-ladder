@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render($routerName . '.index') }}

    <b-container fluid>
        <h1>@lang('banks.currencies.list')</h1>
    </b-container>

    <table-basic
            :route_name = "`{{ route($routerName . '.index', [], false) }}`"
            :items = "{{ $items }}"
            :fields = "[
                 {key: '{{ \App\Currency::NAME }}', sortable: true, label: '@lang('banks.currencies.name')'},
                 {key: '{{ \App\Currency::ISO }}', sortable: true, label: '@lang('banks.currencies.iso')'},
                 {key: '{{ \App\Currency::SYMBOL }}', sortable: true, label: '@lang('banks.currencies.symbol')'},
                 {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-basic>

@endsection