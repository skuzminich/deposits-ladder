@extends('layouts.app')

@section('content')

    @startForm()

    <h1>
        @if(empty($model->id))
            @lang('banks.currencies.create')
        @else
            @lang('banks.currencies.edit', ['currency' => $model->iso])
        @endif
    </h1>

    @text([
        'name' => \App\Currency::NAME,
        'label' => __('banks.currencies.name')])

    @text([
        'name' => \App\Currency::ISO,
        'label' => __('banks.currencies.iso')])

    @text([
        'name' => \App\Currency::SYMBOL,
        'label' => __('banks.currencies.symbol')])

    @endForm()

@endsection
