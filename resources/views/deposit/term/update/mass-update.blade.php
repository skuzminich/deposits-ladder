@extends('layouts.app')

@section('content')

    <b-container>

        {{ Breadcrumbs::render($routerName . '.mass-update', ...[$deposit]) }}

        <b-form
                action="{{ route($routerName . '.mass-update-store', ['deposit' => $deposit]) }}"
                method="POST"
                class="needs-validation"
                novalidate
        >

            @csrf

            <h1>
                @lang('banks.deposits.deposit', ['deposit' => $deposit->name, 'bank' => $deposit->bank->short_name]).
                @lang('banks.deposits.terms.create_multiple_updates')
            </h1>

            <b-form-group label="{{ __('banks.deposits.terms.terms') }}*:"
                          label-for="{{ \App\DepositTermUpdate::DEPOSIT_TERM_ID }}"
                          :label-cols="3"
                          label-class="text-sm-right"
                          horizontal
            >
                <select class="form-control"
                        id="{{ \App\DepositTermUpdate::DEPOSIT_TERM_ID }}"
                        name="{{ \App\DepositTermUpdate::DEPOSIT_TERM_ID }}[]"
                        multiple
                        @validation(['name' => \App\DepositTermUpdate::DEPOSIT_TERM_ID])
                >
                    @foreach($deposit->depositTerms as $term)
                        <option value="{{ $term[\App\DepositTerm::ID] }}">
                            @if (empty($term[\App\DepositTerm::NAME]) === false)
                                {{ $term[\App\DepositTerm::NAME] }}
                            @endif
                            {{ $term[\App\DepositTerm::PERIOD] }}
                            {{ $term->currency[\App\Currency::SYMBOL] }} -
                            {{ \App\DepositTerm::getAllIrrevocableStatuses(\App\DepositTerm::WITH_LABELS)[$term[\App\DepositTerm::IS_IRREVOCABLE_FIELD]] }},
                            {{ \App\DepositTerm::getAllRateTypes(\App\DepositTerm::WITH_LABELS)[$term[\App\DepositTerm::RATE_TYPE]] }}
                        </option>
                    @endforeach
                </select>

                @error(['name' => \App\DepositTermUpdate::DEPOSIT_TERM_ID])

            </b-form-group>

            @date([
                'name' => \App\DepositTermUpdate::INFORM_DATE,
                'label' => __('banks.deposits.terms.updates.inform_date')])

            @date([
                'name' => \App\DepositTermUpdate::START_DATE,
                'label' => __('banks.deposits.terms.updates.start_date')])

            @date([
                'name' => \App\DepositTermUpdate::DEPOSIT_FROM_DATE,
                'label' => __('banks.deposits.terms.updates.deposit_from_date')])

            @date([
                'name' => \App\DepositTermUpdate::DEPOSIT_TO_DATE,
                'label' => __('banks.deposits.terms.updates.deposit_to_date')])

            @radio([
                'name' => \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD,
                'label' => __('banks.deposits.terms.calculation_of_interest_period'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllCalculationPeriods(\App\DepositTerm::WITH_LABELS)])

            @radio([
                'name' => \App\DepositTerm::CAPITALIZATION_ALLOWED,
                'label' => __('banks.deposits.terms.capitalization_allowed'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllCapitalizationStatuses(\App\DepositTerm::WITH_LABELS)])

            @radio([
                'name' => \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED,
                'label' => __('banks.deposits.terms.percent_withdrawal_allowed'),
                'options' => ['' => __('banks.not_changed')] +  \App\DepositTerm::getAllPercentWithdrawalStatuses(\App\DepositTerm::WITH_LABELS)])

            @radio([
                'name' => \App\DepositTerm::RATE_TYPE,
                'label' => __('banks.deposits.terms.rate_type'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllRateTypes(\App\DepositTerm::WITH_LABELS)])

            @text([
                'name' => \App\DepositTerm::FIXED_RATE_PERIOD,
                'label' => __('banks.deposits.terms.fixed_rate_period')])

            @number([
                'name' => \App\DepositTerm::RATE,
                'label' => __('banks.deposits.terms.rate')])

            @radio([
                'name' => \App\DepositTerm::REFILLING_TYPE,
                'label' => __('banks.deposits.terms.refilling_type'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllRefillingTypes(\App\DepositTerm::WITH_LABELS)])

            @text([
                'name' => \App\DepositTerm::REFILLING_PERIOD,
                'label' => __('banks.deposits.terms.refilling_period')])

            @number([
                'name' => \App\DepositTerm::REFILLING_MIN_SUM,
            'label' => __('banks.deposits.terms.refilling_min_sum')])

            @number([
                'name' => \App\DepositTerm::PROLONGATION,
                'label' => __('banks.deposits.terms.prolongation')])

            @text([
                'name' => \App\DepositTerm::PROLONGATION_CONDITIONS,
                'label' => __('banks.deposits.terms.prolongation_conditions')])

            @number([
                'name' => \App\DepositTerm::MIN_SUM,
                'label' => __('banks.deposits.terms.min_sum')])

            @number([
                'name' => \App\DepositTerm::MAX_SUM,
                'label' => __('banks.deposits.terms.max_sum')])

            @number([
                'name' => \App\DepositTerm::IRREDUCIBLE_BALANCE,
                'label' => __('banks.deposits.terms.irreducible_balance')])

            @text([
                'name' => \App\DepositTerm::FULL_REVOKE_CONDITIONS,
                'label' => __('banks.deposits.terms.full_revoke_conditions')])

            @number([
                'name' => \App\DepositTerm::FULL_REVOKE_PERCENT,
                'label' => __('banks.deposits.terms.full_revoke_percent')])

            @text([
                'name' => \App\DepositTerm::PARTIAL_REVOKE_CONDITIONS,
                'label' => __('banks.deposits.terms.partial_revoke_conditions')])

            @number([
                'name' => \App\DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT,
                'label' => __('banks.deposits.terms.partial_revoke_sanctions_percent')])

            @checkbox([
                'name' => \App\DepositTerm::THIRD_PERSON,
                'label' => __('banks.deposits.terms.third_person')])

            @text([
                'name' => \App\DepositTerm::LINK,
                'label' => __('banks.deposits.terms.link')])

            @text([
                'name' => \App\DepositTerm::RESTRICTION,
                'label' => __('banks.deposits.terms.restriction')])

            <b-button type="submit" variant="primary" class="offset-sm-3">@lang('common.submit')</b-button>
        </b-form>
    </b-container>
@endsection
