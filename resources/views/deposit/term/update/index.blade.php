@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render($routerName . '.index', ...[$deposit, $depositTerm]) }}

    <b-container fluid>
        <h1>@lang('banks.deposits.deposit', ['deposit' => $deposit->name, 'bank' => $deposit->bank->short_name]).
            @lang('banks.deposits.terms.edit', ['period' => $depositTerm->period, 'currency' => $depositTerm->currency->iso]).
            @lang('banks.deposits.terms.updates.terms_update')</h1>
    </b-container>

    <table-basic
            :route_name = "`{{ route($routerName . '.index', ['deposit' => $deposit, 'term' => $depositTerm], false) }}`"
            :items = "{{ $items }}"
            :fields = "[
                 {key: '{{ \App\DepositTermUpdate::START_DATE}}', sortable: true, label: '@lang('banks.deposits.terms.updates.start_date')', formatter: (value) => {return new Date(value).toLocaleDateString()}},
                 {key: '{{ \App\DepositTermUpdate::DEPOSIT_FROM_DATE }}', sortable: true, label: '@lang('banks.deposits.terms.updates.deposit_from_date')', formatter: (value) => {return new Date(value).toLocaleDateString()}},
                 {key: '{{ \App\DepositTermUpdate::DEPOSIT_TO_DATE }}', sortable: true, label: '@lang('banks.deposits.terms.updates.deposit_to_date')', formatter: (value) => {if (value !== null)return new Date(value).toLocaleDateString()}},
                 {
                    key: '{{ \App\DepositTerm::RATE_TYPE }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.updates.changes')',
                    formatter: (value, key, item) => {
                        var skip = ['deposit_term', '{{ \App\DepositTermUpdate::ID }}', '{{ \App\DepositTermUpdate::DEPOSIT_TERM_ID }}', '{{ \App\DepositTermUpdate::INFORM_DATE}}', '{{ \App\DepositTermUpdate::DEPOSIT_FROM_DATE}}',
                            '{{ \App\DepositTermUpdate::DEPOSIT_TO_DATE}}', '{{ \App\DepositTermUpdate::CREATED_AT }}', '{{ \App\DepositTermUpdate::UPDATED_AT }}', '{{ \App\DepositTermUpdate::DELETED_AT }}'];
                        var result = [];
                        var values = {
                            {{ \App\DepositTerm::RATE_TYPE }}                      : {{ json_encode(\App\DepositTerm::getAllRateTypes(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::REFILLING_TYPE }}                 : {{ json_encode(\App\DepositTerm::getAllRefillingTypes(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::IS_IRREVOCABLE_FIELD }}           : {{ json_encode(\App\DepositTerm::getAllIrrevocableStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::CAPITALIZATION_ALLOWED }}         : {{ json_encode(\App\DepositTerm::getAllCapitalizationStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED }}     : {{ json_encode(\App\DepositTerm::getAllPercentWithdrawalStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD }} : {{ json_encode(\App\DepositTerm::getAllCalculationPeriods(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::THIRD_PERSON }} : {{ json_encode(\App\DepositTerm::getAllThirdPersonStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                        }
                        for (var propertyName in item) {
                            if (item[propertyName] !== null && skip.indexOf(propertyName) === -1) {
                                var termValue = item[propertyName];
                                if (values.hasOwnProperty(propertyName)) {
                                    termValue = values[propertyName][item[propertyName]].toLowerCase();
                                }

                                var label = $t('banks.deposits.terms.updates.' + propertyName);
                                if (label == 'banks.deposits.terms.updates.' + propertyName) {
                                    label = $t('banks.deposits.terms.' + propertyName);
                                }
                                result.push(label + ' - ' + termValue);
                            }
                        }

                        return result.join(', ');
                    }
                 },
                 {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-basic>

@endsection