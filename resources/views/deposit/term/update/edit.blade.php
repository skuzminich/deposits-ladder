@extends('layouts.app')

@section('content')

    <b-container>

        @if ($model->id)
            {{ Breadcrumbs::render($routerName . '.edit', ...[$deposit, $depositTerm, $model]) }}
        @else
            {{ Breadcrumbs::render($routerName . '.create', ...[$deposit, $depositTerm]) }}
        @endif

        <b-form
                action="@if ($model->id){{ route($routerName . '.update', ['deposit' => $deposit, 'term' => $depositTerm, 'update' => $model]) }}@else{{ route($routerName . '.store', ['deposit' => $deposit, 'term' => $depositTerm]) }}@endif"
                method="POST"
                class="needs-validation"
                novalidate
        >

            @if ($model->id)
                @method('PUT')
            @endif

            @csrf

            <input id="id" type="hidden" name="id" value="{{ $model->id }}" />

            <h1>
                @lang('banks.deposits.deposit', ['deposit' => $deposit->name, 'bank' => $deposit->bank->short_name]).
                @lang('banks.deposits.terms.edit', ['period' => $depositTerm->period, 'currency' => $depositTerm->currency->iso]).
                @if (empty($model->id))
                    @lang('banks.deposits.terms.updates.create')
                @else
                    @lang(
                    'banks.deposits.terms.updates.edit',
                        [
                        'start_date' => $model->deposit_from_date ? (date_create_from_format('Y-m-d H:i:s', $model->deposit_from_date))->format('d.m.Y') : '',
                        'end_date' => $model->deposit_to_date ? (date_create_from_format('Y-m-d H:i:s', $model->deposit_to_date))->format('d.m.Y') : '',
                        ]
                    )
                @endif
            </h1>

            @date([
                'name' => \App\DepositTermUpdate::INFORM_DATE,
                'label' => __('banks.deposits.terms.updates.inform_date')])

            @date([
                'name' => \App\DepositTermUpdate::START_DATE,
                'label' => __('banks.deposits.terms.updates.start_date')])

            @date([
                'name' => \App\DepositTermUpdate::DEPOSIT_FROM_DATE,
                'label' => __('banks.deposits.terms.updates.deposit_from_date')])

            @date([
                'name' => \App\DepositTermUpdate::DEPOSIT_TO_DATE,
                'label' => __('banks.deposits.terms.updates.deposit_to_date')])

            @radio([
                'name' => \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD,
                'label' => __('banks.deposits.terms.calculation_of_interest_period'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllCalculationPeriods(\App\DepositTerm::WITH_LABELS)])

            @radio([
                'name' => \App\DepositTerm::CAPITALIZATION_ALLOWED,
                'label' => __('banks.deposits.terms.capitalization_allowed'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllCapitalizationStatuses(\App\DepositTerm::WITH_LABELS)])

            @radio([
                'name' => \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED,
                'label' => __('banks.deposits.terms.percent_withdrawal_allowed'),
                'options' => ['' => __('banks.not_changed')] +  \App\DepositTerm::getAllPercentWithdrawalStatuses(\App\DepositTerm::WITH_LABELS)])

            @radio([
                'name' => \App\DepositTerm::RATE_TYPE,
                'label' => __('banks.deposits.terms.rate_type'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllRateTypes(\App\DepositTerm::WITH_LABELS)])

            @text([
                'name' => \App\DepositTerm::FIXED_RATE_PERIOD,
                'label' => __('banks.deposits.terms.fixed_rate_period')])

            @number([
                'name' => \App\DepositTerm::RATE,
                'label' => __('banks.deposits.terms.rate')])

            @radio([
                'name' => \App\DepositTerm::REFILLING_TYPE,
                'label' => __('banks.deposits.terms.refilling_type'),
                'options' => ['' => __('banks.not_changed')] + \App\DepositTerm::getAllRefillingTypes(\App\DepositTerm::WITH_LABELS)])

            @text([
                'name' => \App\DepositTerm::REFILLING_PERIOD,
                'label' => __('banks.deposits.terms.refilling_period')])

            @number([
                'name' => \App\DepositTerm::REFILLING_MIN_SUM,
            'label' => __('banks.deposits.terms.refilling_min_sum')])

            @number([
                'name' => \App\DepositTerm::PROLONGATION,
                'label' => __('banks.deposits.terms.prolongation')])

            @text([
                'name' => \App\DepositTerm::PROLONGATION_CONDITIONS,
                'label' => __('banks.deposits.terms.prolongation_conditions')])

            @number([
                'name' => \App\DepositTerm::MIN_SUM,
                'label' => __('banks.deposits.terms.min_sum')])

            @number([
                'name' => \App\DepositTerm::MAX_SUM,
                'label' => __('banks.deposits.terms.max_sum')])

            @number([
                'name' => \App\DepositTerm::IRREDUCIBLE_BALANCE,
                'label' => __('banks.deposits.terms.irreducible_balance')])

            @text([
                'name' => \App\DepositTerm::FULL_REVOKE_CONDITIONS,
                'label' => __('banks.deposits.terms.full_revoke_conditions')])

            @number([
                'name' => \App\DepositTerm::FULL_REVOKE_PERCENT,
                'label' => __('banks.deposits.terms.full_revoke_percent')])

            @text([
                'name' => \App\DepositTerm::PARTIAL_REVOKE_CONDITIONS,
                'label' => __('banks.deposits.terms.partial_revoke_conditions')])

            @number([
                'name' => \App\DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT,
                'label' => __('banks.deposits.terms.partial_revoke_sanctions_percent')])

            @checkbox([
                'name' => \App\DepositTerm::THIRD_PERSON,
                'label' => __('banks.deposits.terms.third_person')])

            @text([
                'name' => \App\DepositTerm::LINK,
                'label' => __('banks.deposits.terms.link')])

            @text([
                'name' => \App\DepositTerm::RESTRICTION,
                'label' => __('banks.deposits.terms.restriction')])

            <b-button type="submit" variant="primary" class="offset-sm-3">@lang('common.submit')</b-button>
        </b-form>
    </b-container>
@endsection
