@extends('layouts.app')

@section('content')

    <b-container>

        @if ($model->id)
            {{ Breadcrumbs::render($routerName . '.edit', ...[$deposit, $model]) }}
        @else
            {{ Breadcrumbs::render($routerName . '.create', $deposit) }}
        @endif

        <b-form
                action="@if ($model->id){{ route($routerName . '.update', ['deposit' => $deposit, 'term' => $model]) }}@else{{ route($routerName . '.store', $deposit) }}@endif"
                method="POST"
                class="needs-validation"
                novalidate
        >

            @if ($model->id)
                @method('PUT')
            @endif

            @csrf

            <input id="id" type="hidden" name="id" value="{{ $model->id }}" />

            <h1>
                @lang('banks.deposits.deposit', ['deposit' => $deposit->name, 'bank' => $deposit->bank->short_name]).
                @if (empty($model->id))
                    @lang('banks.deposits.terms.create')
                @else
                    @lang(
                    'banks.deposits.terms.edit',
                        [
                        'period' => $model->period,
                        'currency' => $model->currency->iso,
                        ]
                    )
                @endif
            </h1>

            @text([
                'name' => \App\DepositTerm::NAME,
                'label' => __('banks.deposits.terms.name')])

            @text([
                'name' => \App\DepositTerm::PERIOD,
                'label' => __('banks.deposits.terms.period')])

            @checkbox([
                'name' => \App\DepositTerm::IS_IRREVOCABLE,
                'label' => __('banks.deposits.terms.irrevocable')])

            @select([
                'name' => \App\DepositTerm::CURRENCY_ID,
                'label' => __('banks.deposits.terms.currency'),
                'options' => $currencies])

            @radio([
                'name' => \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD,
                'label' => __('banks.deposits.terms.calculation_of_interest_period'),
                'options' => \App\DepositTerm::getAllCalculationPeriods(\App\DepositTerm::WITH_LABELS)])

            @checkbox([
                'name' => \App\DepositTerm::CAPITALIZATION_ALLOWED,
                'label' => __('banks.deposits.terms.capitalization_allowed')])

            @checkbox([
                'name' => \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED,
                'label' => __('banks.deposits.terms.percent_withdrawal_allowed')])

            @radio([
                'name' => \App\DepositTerm::RATE_TYPE,
                'label' => __('banks.deposits.terms.rate_type'),
                'options' => \App\DepositTerm::getAllRateTypes(\App\DepositTerm::WITH_LABELS)])

            @text([
                'name' => \App\DepositTerm::FIXED_RATE_PERIOD,
                'label' => __('banks.deposits.terms.fixed_rate_period')])

            @number([
                'name' => \App\DepositTerm::RATE,
                'label' => __('banks.deposits.terms.rate')])

            @radio([
                'name' => \App\DepositTerm::REFILLING_TYPE,
                'label' => __('banks.deposits.terms.refilling_type'),
                'options' => \App\DepositTerm::getAllRefillingTypes(\App\DepositTerm::WITH_LABELS)])

            @text([
                'name' => \App\DepositTerm::REFILLING_PERIOD,
                'label' => __('banks.deposits.terms.refilling_period')])

            @number([
                'name' => \App\DepositTerm::REFILLING_MIN_SUM,
                'label' => __('banks.deposits.terms.refilling_min_sum')])

            @radio([
                'name' => \App\DepositTerm::INTERNET,
                'label' => __('banks.deposits.terms.internet'),
                'options' => \App\DepositTerm::getAllInternetStatuses(\App\DepositTerm::WITH_LABELS)])

                @number([
                    'name' => \App\DepositTerm::PROLONGATION,
                    'label' => __('banks.deposits.terms.prolongation')])

            @text([
                'name' => \App\DepositTerm::PROLONGATION_CONDITIONS,
                'label' => __('banks.deposits.terms.prolongation_conditions')])

            @number([
                'name' => \App\DepositTerm::MIN_SUM,
                'label' => __('banks.deposits.terms.min_sum')])

            @number([
                'name' => \App\DepositTerm::MAX_SUM,
                'label' => __('banks.deposits.terms.max_sum')])

            @number([
                'name' => \App\DepositTerm::IRREDUCIBLE_BALANCE,
                'label' => __('banks.deposits.terms.irreducible_balance')])

            @text([
                'name' => \App\DepositTerm::FULL_REVOKE_CONDITIONS,
                'label' => __('banks.deposits.terms.full_revoke_conditions')])

            @number([
                'name' => \App\DepositTerm::FULL_REVOKE_PERCENT,
                'label' => __('banks.deposits.terms.full_revoke_percent')])

            @text([
                'name' => \App\DepositTerm::PARTIAL_REVOKE_CONDITIONS,
                'label' => __('banks.deposits.terms.partial_revoke_conditions')])

            @number([
                'name' => \App\DepositTerm::PARTIAL_REVOKE_SANCTIONS_PERCENT,
                'label' => __('banks.deposits.terms.partial_revoke_sanctions_percent')])

            @checkbox([
                'name' => \App\DepositTerm::THIRD_PERSON,
                'label' => __('banks.deposits.terms.third_person')])

            @text([
                'name' => \App\DepositTerm::LINK,
                'label' => __('banks.deposits.terms.link')])

            @text([
                'name' => \App\DepositTerm::RESTRICTION,
                'label' => __('banks.deposits.terms.restriction')])

            @if (empty($model->id) === false)
                <b-row class="mb-3">
                    <b-button href="update" class="offset-sm-3">@lang('banks.deposits.terms.updates.list')</b-button>
                </b-row>
            @endif

            <b-button type="submit" variant="primary" class="offset-sm-3">@lang('common.submit')</b-button>
        </b-form>
    </b-container>
@endsection
