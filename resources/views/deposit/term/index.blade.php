@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render($routerName . '.index', $deposit) }}

    <b-container fluid>
        <h1>@lang('banks.deposits.terms.list', ['deposit' => $deposit->name, 'bank' => $deposit->bank->short_name])</h1>
    </b-container>

    <table-button-column
            :route_name = "`{{ route($routerName . '.index', ['deposit' => $deposit], false) }}`"
            :items = "{{ $items }}"
            :create2_label = "`@lang('banks.deposits.terms.create_multiple_updates')`"
            :create2_action = "`{{ route($routerName . '.update.mass-update', ['deposit' => $deposit], false) }}`"
            :button_label = "`@lang('banks.deposits.terms.updates.updates')`"
            :button_action = "`{{ route($routerName . '.update.index', ['deposit' => $deposit, 'term' => ':item_id'], false) }}`"
            :fields = "[
                {
                    key: 'deposit.{{ \App\Deposit::NAME}}',
                    sortable: true,
                    label: '@lang('banks.deposits.name')',
                    formatter: (value, key, row) => {
                        var name = row.{{ \App\DepositTerm::NAME }};
                        if (!name) {
                            name = value;
                        }

                        return row.deposit.bank.{{  \App\Bank::SHORT_NAME }} + ' ' + name;
                    }
                },
                {key: 'currency.{{ \App\Currency::ISO }}', sortable: true, label: '@lang('banks.deposits.terms.currency')'},
                {key: '{{ \App\DepositTerm::RATE }}', sortable: true, label: '@lang('banks.deposits.terms.rate')'},
                {key: '{{ \App\DepositTerm::PERIOD }}', sortable: true, label: '@lang('banks.deposits.terms.period')'},
                {
                    key: '{{ \App\DepositTerm::IS_IRREVOCABLE }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.revocable')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllIrrevocableStatuses(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::RATE_TYPE }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.rate_type')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllRateTypes(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::REFILLING_TYPE }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.refilling_type')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllRefillingTypes(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::INTERNET }}',
                    label: '@lang('banks.deposits.terms.internet')',
                    sortable: true,
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllInternetStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.calculation_of_interest_period')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllCalculationPeriods(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                     }
                },
                {
                    key: '{{ \App\DepositTerm::CAPITALIZATION_ALLOWED }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.capita_lization')',
                    formatter: (value) => { if (parseInt(value) === {{ \App\DepositTerm::CAPITALIZATION_ENABLED}}) return '@lang('banks.deposits.terms.capitalization_allowed')'; }
                },
                {
                    key: '{{ \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.per_cent_with_drawal_allowed')',
                    formatter: (value) => { if (parseInt(value) === {{ \App\DepositTerm::PERCENTS_WITHDRAWAL_ENABLED}}) return '@lang('banks.deposits.terms.percent_withdrawal_allowed')'; }
                },
                {key: '{{ \App\DepositTerm::PROLONGATION }}', sortable: true, label: '@lang('banks.deposits.terms.prolon_gation')'},
                {
                    key: '{{ \App\DepositTerm::MIN_SUM }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.min_max_sum')',
                    formatter: (value, key, row) => {
                        if (value !== null && row.max_sum !== null) {
                            return value + ' - ' + row.max_sum;
                        } else if (value !== null) {
                            return '>' + value;
                        } else {
                            return '<' + row.max_sum;
                        }
                    },
                },
                {
                    key: 'link',
                    sortable: false,
                    label: '@lang('banks.deposits.link')',
                    formatter: (value, key, row) => {
                        if (value !== null) {
                            return value;
                        } else {
                            return row.deposit.link;
                        }
                    }
                },
                {key: 'button', label: '@lang('banks.deposits.terms.updates.updates')'},
                {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-button-column>

@endsection