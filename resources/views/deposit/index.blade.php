@extends('layouts.app')

@section('content')

    @php
        foreach ($items as &$deposit) {
            if ($deposit->status === \App\Deposit::STATUS_ARCHIVED || $deposit->status === \App\Deposit::STATUS_FREEZED) {
                $deposit->_rowVariant = 'warning';
            }
        }
    @endphp

    {{ Breadcrumbs::render($routerName . '.index') }}

    <b-container fluid>
        <h1>@lang('banks.deposits.list')</h1>
    </b-container>

    <b-container fluid class="mb-3">
        <b-form
                action="{{ route($routerName . '.index') }}"
                method="GET"
        >
            @csrf
            <b-row>
                <b-col md="5">
                    <b-form-group horizontal label="{{ __('banks.deposits.bank') }}" class="mb-0" label-class="text-sm-right">
                        <!--suppress HtmlFormInputWithoutLabel -->
                        <select class="form-control"
                                id="filter-bank"
                                name="filter[bank]"
                        >
                            @foreach($banks as $bank)
                                <option value="{{ $bank['id'] }}" @if ((int)$bank['id'] === (int)$filter['bank']) selected="selected" @endif>
                                    {{ $bank['short_name'] }}
                                </option>
                            @endforeach
                        </select>
                    </b-form-group>
                </b-col>
                <b-col md="5">
                    <b-form-group label="{{ __('common.search') }}"
                                  label-for="filter-search"
                                  :label-cols="3"
                                  label-class="text-sm-right"
                                  horizontal
                    >

                        <b-form-input id="filter-search"
                                      type="text"
                                      name="filter[search]"
                                      value="@if (empty($filter['search']) === false){{ $filter['search'] }}@endif"
                        >
                        </b-form-input>
                    </b-form-group>
                </b-col>
                <b-col md="2">
                    <b-button type="submit" variant="primary">@lang('common.filter')</b-button>
                </b-col>
            </b-row>
        </b-form>
    </b-container>

    <table-button-column
            :route_name = "`{{ route($routerName . '.index', [], false) }}`"
            :items = "{{ $items }}"
            :button_label = "`@lang('banks.deposits.terms.terms')`"
            :button_action = "`{{ route($routerName . '.term.index', ['deposit' => ':item_id'], false) }}`"
            :fields = "[
                {
                    key: 'bank.{{\App\Bank::SHORT_NAME}}',
                    sortable: true,
                    label: '@lang('banks.deposits.bank')'
                },
                {key: '{{ \App\Deposit::NAME }}', sortable: true, label: '@lang('banks.deposits.name')'},
                {
                    key: '{{ \App\Deposit::STATUS }}',
                    label: '@lang('banks.deposits.status')',
                    sortable: true,
                    formatter: (value) => {
                        var values = {{ json_encode(\App\Deposit::getAllStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {key: 'link', sortable: false, label: '@lang('banks.deposits.link')'},
                {key: 'button', label: '@lang('banks.deposits.terms.terms')'},
                {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-button-column>

@endsection