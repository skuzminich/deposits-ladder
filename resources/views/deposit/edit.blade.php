@extends('layouts.app')

@section('content')

    @startForm()

    <h1>
        @if(empty($model->id))
            @lang('banks.deposits.create')
        @else
            @lang('banks.deposits.edit', ['deposit' => $model->name, 'bank' => $model->bank->short_name])
        @endif
    </h1>

    @select([
        'name' => \App\Deposit::BANK_ID,
        'label' => __('banks.deposits.bank'),
        'options' => $banks,
        'textField' => 'short_name'])

    @text([
        'name' => \App\Deposit::NAME,
        'label' => __('banks.deposits.name')])

    @radio([
        'name' => \App\Deposit::STATUS,
        'label' => __('banks.deposits.status'),
        'options' => \App\Deposit::getAllStatuses(\App\Deposit::WITH_LABELS)])

    @text([
        'name' => \App\Deposit::LINK,
        'label' => __('banks.deposits.link')])

    @text([
        'name' => \App\Deposit::OPEN_RESTRICTION,
        'label' => __('banks.deposits.open_restriction')])

    @if (empty($model->id) === false)
        <b-row class="mb-3">
            <b-button href="term" class="offset-sm-3">@lang('banks.deposits.terms.terms')</b-button>
        </b-row>
    @endif

    @endForm()

@endsection
