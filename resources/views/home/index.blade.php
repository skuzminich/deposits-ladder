@extends('layouts.app')

@section('content')
    <b-container fluid>
        <h1>@lang('banks.deposits.deposit_search')</h1>
    </b-container>

    @include('home.filter', ['action' => route($routerName . '.index', [], false)])

    <h2>{{ __('common.found') . ': ' . $items->total() }}</h2>
    <table-button-column
            :route_name = "`{{ route($routerName . '.index', [], false) }}`"
            :items = "{{ json_encode($items->items()) }}"
            :fields = "[
                {
                    key: 'deposit.{{ \App\Deposit::NAME}}',
                    sortable: true,
                    label: '@lang('banks.deposits.name')',
                    formatter: (value, key, row) => {
                        var name = row.{{ \App\DepositTerm::NAME }};
                        if (!name) {
                            name = value;
                        }

                        return row.deposit.bank.{{  \App\Bank::SHORT_NAME }} + ' ' + name;
                    }
                },
                {key: 'currency.{{ \App\Currency::ISO }}', sortable: true, label: '@lang('banks.deposits.terms.currency')'},
                {key: '{{ \App\DepositTerm::RATE }}', sortable: true, label: '@lang('banks.deposits.terms.rate')'},
                {key: '{{ \App\DepositTerm::PERIOD }}', sortable: true, label: '@lang('banks.deposits.terms.period')'},
                {
                    key: '{{ \App\DepositTerm::IS_IRREVOCABLE_FIELD }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.revocable')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllIrrevocableStatuses(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::RATE_TYPE }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.rate_type')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllRateTypes(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::REFILLING_TYPE }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.refilling_type')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllRefillingTypes(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::INTERNET }}',
                    label: '@lang('banks.deposits.terms.internet')',
                    sortable: true,
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllInternetStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.calculation_of_interest_period')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllCalculationPeriods(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::CAPITALIZATION_ALLOWED }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.capita_lization')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllCapitalizationStatuses(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {
                    key: '{{ \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.per_cent_with_drawal_allowed')',
                    formatter: (value) => {
                        var values = {{ json_encode(\App\DepositTerm::getAllPercentWithdrawalStatuses(\App\DepositTerm::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                    }
                },
                {key: '{{ \App\DepositTerm::PROLONGATION }}', sortable: true, label: '@lang('banks.deposits.terms.prolon_gation')'},
                {
                    key: '{{ \App\DepositTerm::MIN_SUM }}',
                    sortable: true,
                    label: '@lang('banks.deposits.terms.min_max_sum')',
                    formatter: (value, key, row) => {
                        if (value !== null && row.max_sum !== null) {
                            return value + ' - ' + row.max_sum;
                        } else if (value !== null) {
                            return '>' + value;
                        } else {
                            return '<' + row.max_sum;
                        }
                    },
                },
                {
                    key: 'link',
                    sortable: false,
                    label: '@lang('banks.deposits.link')',
                    formatter: (value, key, row) => {
                        if (value !== null) {
                            return value;
                        } else {
                            return row.deposit.link;
                        }
                    }
                },
                {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-button-column>
    {{
        $items
            ->appends([
                'filter' => [
                    \App\Deposit::BANK_ID => (isset($filter[\App\Deposit::BANK_ID])) ? $filter[\App\Deposit::BANK_ID] : null,
                    \App\DepositTerm::CURRENCY_ID => (isset($filter[\App\DepositTerm::CURRENCY_ID])) ? $filter[\App\DepositTerm::CURRENCY_ID] : null,
                    \App\DepositTerm::DEPOSIT_ID => (isset($filter[\App\DepositTerm::DEPOSIT_ID])) ? $filter[\App\DepositTerm::DEPOSIT_ID] : null,
                    \App\DepositTerm::IS_IRREVOCABLE_FIELD => (isset($filter[\App\DepositTerm::IS_IRREVOCABLE_FIELD])) ? $filter[\App\DepositTerm::IS_IRREVOCABLE_FIELD] : null,
                    \App\DepositTerm::RATE_TYPE => (isset($filter[\App\DepositTerm::RATE_TYPE])) ? $filter[\App\DepositTerm::RATE_TYPE] : null,
                    \App\DepositTerm::REFILLING_TYPE => (isset($filter[\App\DepositTerm::REFILLING_TYPE])) ? $filter[\App\DepositTerm::REFILLING_TYPE] : null,
                    \App\DepositTerm::INTERNET => (isset($filter[\App\DepositTerm::INTERNET])) ? $filter[\App\DepositTerm::INTERNET] : null,
                    \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD => (isset($filter[\App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD])) ? $filter[\App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD] : null,
                    \App\DepositTerm::CAPITALIZATION_ALLOWED => (isset($filter[\App\DepositTerm::CAPITALIZATION_ALLOWED])) ? $filter[\App\DepositTerm::CAPITALIZATION_ALLOWED] : null,
                    \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED => (isset($filter[\App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED])) ? $filter[\App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED] : null,
                    \App\DepositTerm::MIN_SUM => (isset($filter[\App\DepositTerm::MIN_SUM])) ? $filter[\App\DepositTerm::MIN_SUM] : null,
                    \App\DepositTerm::MAX_SUM => (isset($filter[\App\DepositTerm::MAX_SUM])) ? $filter[\App\DepositTerm::MAX_SUM] : null,
                    \App\DepositTerm::PROLONGATION => (isset($filter[\App\DepositTerm::PROLONGATION])) ? $filter[\App\DepositTerm::PROLONGATION] : null,
                    \App\DepositTerm::PERIOD => (isset($filter[\App\DepositTerm::PERIOD])) ? $filter[\App\DepositTerm::PERIOD] : null,
                    'search' => (isset($filter['search'])) ? $filter['search'] : null,
                    'start_date' => (isset($filter['from_date'])) ? $filter['from_date'] : null,
                    'end_date' => (isset($filter['to_date'])) ? $filter['to_date'] : null,
                    'min_rate' => (isset($filter['min_rate'])) ? $filter['min_rate'] : null,
                    'max_rate' => (isset($filter['max_rate'])) ? $filter['max_rate'] : null,
                ]
            ])
            ->links()
    }}
@endsection
