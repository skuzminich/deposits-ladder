<b-container fluid class="mb-3">
    <b-form
            action="{{ $action }}"
            method="GET"
    >
        @csrf
        <b-row class="mb-3">
            <b-col md="4">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.currency') }}" class="mb-0" label-class="text-sm-right">
                    <!--suppress HtmlFormInputWithoutLabel -->
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::CURRENCY_ID }}"
                            name="filter[{{ \App\DepositTerm::CURRENCY_ID }}]"
                    >
                        <option value="">
                            {{ __('common.choose_please') }}
                        </option>
                        @foreach($currencies as $currency)
                            <option value="{{ $currency[\App\Currency::ID] }}" @if (empty($filter[\App\Currency::ID]) === false && (int)$currency[\App\Currency::ID] === (int)$filter[\App\DepositTerm::CURRENCY_ID]) selected="selected" @endif>
                                {{ $currency[\App\Currency::NAME] }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="4">
                <b-form-group horizontal label="{{ __('banks.deposits.bank') }}" class="mb-0" label-class="text-sm-right">
                    <!--suppress HtmlFormInputWithoutLabel -->
                    <select class="form-control"
                            id="filter-{{ \App\Deposit::BANK_ID }}"
                            name="filter[{{ \App\Deposit::BANK_ID }}][]"
                            multiple
                    >
                        @foreach($banks as $bank)
                            <option value="{{ $bank[\App\Bank::ID] }}" @if (empty($filter[\App\Deposit::BANK_ID]) === false && in_array($bank[\App\Bank::ID], $filter[\App\Deposit::BANK_ID])) selected="selected" @endif>
                                {{ $bank[\App\Bank::SHORT_NAME] }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="4">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.deposit') }}" class="mb-0" label-class="text-sm-right">
                    <!--suppress HtmlFormInputWithoutLabel -->
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::DEPOSIT_ID }}"
                            name="filter[{{ \App\DepositTerm::DEPOSIT_ID }}][]"
                            multiple
                    >
                        @foreach($deposits as $deposit)
                            <option value="{{ $deposit[\App\Deposit::ID] }}" @if (empty($filter[\App\DepositTerm::DEPOSIT_ID]) === false && in_array($deposit[\App\Deposit::ID], $filter[\App\DepositTerm::DEPOSIT_ID])) selected="selected" @endif>
                                {{ $deposit[\App\Deposit::NAME] }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
        </b-row>
        <b-row class="mb-3">
            <b-col md="4">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.irrevocable') . '/' . __('banks.deposits.terms.revocable') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::IS_IRREVOCABLE_FIELD }}"
                            name="filter[{{ \App\DepositTerm::IS_IRREVOCABLE_FIELD }}]"
                    >
                        <option value="">
                            {{ __('common.choose_please') }}
                        </option>
                        @foreach(\App\DepositTerm::getAllIrrevocableStatuses(\App\DepositTerm::WITH_LABELS) as $irrevocableStatus => $irrevocableLabel)
                            <option value="{{ $irrevocableStatus }}" @if (isset($filter[\App\DepositTerm::IS_IRREVOCABLE_FIELD]) && (int)$irrevocableStatus === (int)$filter[\App\DepositTerm::IS_IRREVOCABLE_FIELD]) selected="selected" @endif>
                                {{ $irrevocableLabel }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="4">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.rate_type') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::RATE_TYPE }}"
                            name="filter[{{ \App\DepositTerm::RATE_TYPE }}][]"
                            multiple
                    >
                        @foreach(\App\DepositTerm::getAllRateTypes(\App\DepositTerm::WITH_LABELS) as $rateType => $rateTypeLabel)
                            <option value="{{ $rateType }}" @if (isset($filter[\App\DepositTerm::RATE_TYPE]) && in_array($rateType, $filter[\App\DepositTerm::RATE_TYPE])) selected="selected" @endif>
                                {{ $rateTypeLabel }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="4">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.refilling_type') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::REFILLING_TYPE }}"
                            name="filter[{{ \App\DepositTerm::REFILLING_TYPE }}][]"
                            multiple
                    >
                        @foreach(\App\DepositTerm::getAllRefillingTypes(\App\DepositTerm::WITH_LABELS) as $refillingType => $refillingTypeLabel)
                            <option value="{{ $refillingType }}" @if (isset($filter[\App\DepositTerm::REFILLING_TYPE]) && in_array($refillingType, $filter[\App\DepositTerm::REFILLING_TYPE])) selected="selected" @endif>
                                {{ $refillingTypeLabel }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
        </b-row>
        <b-row class="mb-3">
            <b-col md="3">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.internet') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::INTERNET }}"
                            name="filter[{{ \App\DepositTerm::INTERNET }}][]"
                            multiple
                    >
                        @foreach(\App\DepositTerm::getAllInternetStatuses(\App\DepositTerm::WITH_LABELS) as $internetType => $internetTypeLabel)
                            <option value="{{ $internetType }}" @if (isset($filter[\App\DepositTerm::INTERNET]) && in_array($internetType, $filter[\App\DepositTerm::INTERNET])) selected="selected" @endif>
                                {{ $internetTypeLabel }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="3">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.calculation_of_interest_period') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD }}"
                            name="filter[{{ \App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD }}][]"
                            multiple
                    >
                        @foreach(   \App\DepositTerm::getAllCalculationPeriods(\App\DepositTerm::WITH_LABELS) as $calculationPeriod => $calculationPeriodLabel)
                            <option value="{{ $calculationPeriod }}" @if (isset($filter[\App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD]) && in_array($calculationPeriod, $filter[\App\DepositTerm::CALCULATION_OF_INTEREST_PERIOD])) selected="selected" @endif>
                                {{ $calculationPeriodLabel }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="3">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.capitalization_allowed') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::CAPITALIZATION_ALLOWED }}"
                            name="filter[{{ \App\DepositTerm::CAPITALIZATION_ALLOWED }}]"
                    >
                        <option value="">
                            {{ __('common.choose_please') }}
                        </option>
                        @foreach(\App\DepositTerm::getAllCapitalizationStatuses(\App\DepositTerm::WITH_LABELS) as $capitalizationStatus => $capitalizationLabel)
                            <option value="{{ $capitalizationStatus }}" @if (isset($filter[\App\DepositTerm::CAPITALIZATION_ALLOWED]) && (int)$capitalizationStatus === (int)$filter[\App\DepositTerm::CAPITALIZATION_ALLOWED]) selected="selected" @endif>
                                {{ $capitalizationLabel }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="3">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.percent_withdrawal_allowed') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED }}"
                            name="filter[{{ \App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED }}]"
                    >
                        <option value="">
                            {{ __('common.choose_please') }}
                        </option>
                        @foreach(\App\DepositTerm::getAllPercentWithdrawalStatuses(\App\DepositTerm::WITH_LABELS) as $percentWithdrawalStatus => $percentWithdrawalLabel)
                            <option value="{{ $percentWithdrawalStatus }}" @if (isset($filter[\App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED]) && (int)$percentWithdrawalStatus === (int)$filter[\App\DepositTerm::PERCENT_WITHDRAWAL_ALLOWED]) selected="selected" @endif>
                                {{ $percentWithdrawalLabel }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
        </b-row>
        <b-row class="mb-3">
            <b-col md="4">
                <b-form-group label="{{ __('banks.deposits.terms.min_sum') }}"
                              label-for="filter-min_sum"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-min_sum"
                                  type="text"
                                  name="filter[min_sum]"
                                  value="@if (empty($filter['min_sum']) === false){{ $filter['min_sum'] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
            <b-col md="4">
                <b-form-group label="{{ __('banks.deposits.terms.max_sum') }}"
                              label-for="filter-max_sum"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-max_sum"
                                  type="text"
                                  name="filter[max_sum]"
                                  value="@if (empty($filter['max_sum']) === false){{ $filter['max_sum'] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
            <b-col md="4">
                <b-form-group label="{{ __('banks.deposits.terms.prolongation') }}"
                              label-for="filter-{{ \App\DepositTerm::PROLONGATION }}"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-{{ \App\DepositTerm::PROLONGATION }}"
                                  type="text"
                                  name="filter[{{\App\DepositTerm::PROLONGATION}}]"
                                  value="@if (empty($filter[\App\DepositTerm::PROLONGATION]) === false){{ $filter[\App\DepositTerm::PROLONGATION] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
        </b-row>
        <b-row class="mb-3">
            <b-col md="4">
                <b-form-group label="{{ __('common.search') }}"
                              label-for="filter-search"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-search"
                                  type="text"
                                  name="filter[search]"
                                  value="@if (empty($filter['search']) === false){{ $filter['search'] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
        </b-row>
        <b-row class="mb-3">
            <b-col md="2">
                <b-form-group label="{{ __('banks.deposits.from_date') }}:"
                              label-for="filter-from_date"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-from_date"
                                  type="date"
                                  name="filter[from_date]"
                                  value="@if (empty($filter['from_date']) === false){{ $filter['from_date'] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
            <b-col md="2">
                <b-form-group label="{{ __('banks.deposits.to_date') }}:"
                              label-for="filter-to_date"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-to_date"
                                  type="date"
                                  name="filter[to_date]"
                                  value="@if (empty($filter['to_date']) === false){{ $filter['to_date'] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
            <b-col md="3">
                <b-form-group horizontal label="{{ __('banks.deposits.terms.period') }}" class="mb-0" label-class="text-sm-right">
                    <select class="form-control"
                            id="filter-{{ \App\DepositTerm::PERIOD }}"
                            name="filter[{{ \App\DepositTerm::PERIOD }}][]"
                            multiple
                    >
                        @foreach($periods as $period)
                            <option value="{{ $period }}" @if (isset($filter[\App\DepositTerm::PERIOD]) && in_array($period, $filter[\App\DepositTerm::PERIOD])) selected="selected" @endif>
                                {{ $period }}
                            </option>
                        @endforeach
                    </select>
                </b-form-group>
            </b-col>
            <b-col md="2">
                <b-form-group label="{{ __('banks.deposits.terms.min_rate') }}"
                              label-for="filter-min_rate"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-min_rate"
                                  type="text"
                                  name="filter[min_rate]"
                                  value="@if (empty($filter['min_rate']) === false){{ $filter['min_rate'] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
            <b-col md="2">
                <b-form-group label="{{ __('banks.deposits.terms.max_rate') }}"
                              label-for="filter-max_rate"
                              :label-cols="3"
                              label-class="text-sm-right"
                              horizontal
                >

                    <b-form-input id="filter-max_rate"
                                  type="text"
                                  name="filter[max_rate]"
                                  value="@if (empty($filter['max_rate']) === false){{ $filter['max_rate'] }}@endif"
                    >
                    </b-form-input>
                </b-form-group>
            </b-col>
        </b-row>
        <b-row>
            <b-col md="12" class="text-center">
                <b-button type="submit" variant="primary">@lang('common.filter')</b-button>
            </b-col>
        </b-row>
    </b-form>
</b-container>