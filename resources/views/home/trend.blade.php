@extends('layouts.app')

@section('content')
    <b-container fluid>
        <h1>@lang('banks.deposits.rates_trend')</h1>
    </b-container>

    @include('home.filter', ['action' => route($routerName . '.trend', [], false)])

    <h2>{{ __('common.found') . ': ' . count($items) }}</h2>
    <table-button-column
            :route_name = "`{{ route($routerName . '.trend', [], false) }}`"
            :items = "{{ json_encode($items) }}"
            :fields = "[
                {
                    key: 'deposit.{{ \App\Deposit::NAME}}',
                    sortable: true,
                    label: '@lang('banks.deposits.name')',
                    formatter: (value, key, row) => {
                        var name = row.{{ \App\DepositTerm::NAME }};
                        if (!name) {
                            name = value;
                        }

                        return row.deposit.bank.{{ \App\Bank::SHORT_NAME }} + ' ' + name;
                    }
                },
                {
                    key: '{{ \App\DepositTerm::PERIOD}}',
                    sortable: false,
                    label: '@lang('banks.deposits.terms.terms')',
                    formatter: (value, key, row) => {
                        var result = [row.{{ \App\DepositTerm::PERIOD }}];

                        var values = {
                            {{ \App\DepositTerm::IS_IRREVOCABLE_FIELD }}           : {{ json_encode(\App\DepositTerm::getAllIrrevocableStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::RATE_TYPE }}                      : {{ json_encode(\App\DepositTerm::getAllRateTypes(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                            {{ \App\DepositTerm::REFILLING_TYPE }}                 : {{ json_encode(\App\DepositTerm::getAllRefillingTypes(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }},
                        }

                        result.push(values['{{ \App\DepositTerm::IS_IRREVOCABLE_FIELD }}'][row.{{ \App\DepositTerm::IS_IRREVOCABLE_FIELD }}]);
                        result.push(values['{{ \App\DepositTerm::RATE_TYPE }}'][row.{{ \App\DepositTerm::RATE_TYPE }}]);
                        result.push('{{ __('banks.deposits.terms.refilling_type') }}' + ' ' + values['{{ \App\DepositTerm::REFILLING_TYPE }}'][row.{{ \App\DepositTerm::REFILLING_TYPE }}].toLowerCase());
                        result.push(row.currency.{{ \App\Currency::ISO }});

                        return result.join('. ');
                    }
                },
                <?php /** @var array $updateDates */ ?>
                <?php foreach($updateDates as $dateString): ?>
                {
                    key: '{{ $dateString }}',
                    sortable: true,
                    label: '{{ $dateString }}',
                    tdClass : (value, key, row) => {
                        if (value > {{ $maxRatesAtMoments[$dateString] }} * 0.99) {
                            return 'max-rate';
                        }

                        return null;
                    }
                },
                <?php endforeach; ?>
                {
                    key: 'link',
                    sortable: false,
                    label: '@lang('banks.deposits.link')',
                    formatter: (value, key, row) => {
                        if (value !== null) {
                            return value;
                        } else {
                            return row.deposit.link;
                        }
                    }
                }
                ]"
    >
        @csrf
    </table-button-column>
@endsection
