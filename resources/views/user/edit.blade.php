@extends('layouts.app')

@section('content')

    @startForm()

    <h1>@lang('banks.users.user', ['user' => $model->name])</h1>

    @text([
        'name' => \App\User::NAME,
        'label' => __('banks.users.name')])

    @text([
        'name' => \App\User::EMAIL,
        'label' => __('banks.users.email')])

    @endForm()

@endsection