@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render($routerName . '.index', $user) }}

    <b-container fluid>
        <h1>@lang('banks.users.deposits.user_deposit') {{ $user->name }}</h1>
    </b-container>

    <table-button-column
            :route_name = "`{{ route($routerName . '.index', ['user' => $user], false) }}`"
            :items = "{{ $items }}"
            :button_label = "`@lang('banks.users.deposits.instalments')`"
            :button_action = "`{{ route($routerName . '.instalment.index', ['user' => $user, 'deposit' => ':item_id'], false) }}`"
            :button_label2 = "`@lang('banks.users.deposits.withdrawals')`"
            :button_action2 = "`{{ route($routerName . '.withdrawal.index', ['user' => $user, 'deposit' => ':item_id'], false) }}`"
            :fields = "[
                {key: '{{ \App\UserDeposit::START_DATE }}', sortable: true, label: '@lang('banks.users.deposits.start_date')', formatter: (value) => {return new Date(value).toLocaleDateString()}},
                {key: '{{ \App\UserDeposit::BALANCE }}', sortable: true, label: '@lang('banks.users.deposits.balance')'},
                {key: 'deposit_term.{{ \App\DepositTerm::PERIOD }}', sortable: true, label: '@lang('banks.users.deposits.period')'},
                {key: 'deposit_term.currency.{{ \App\Currency::ISO }}', sortable: true, label: '@lang('banks.users.deposits.currency')'},
                {key: 'deposit_term.deposit.{{ \App\Deposit::NAME }}', sortable: true, label: '@lang('banks.users.deposits.deposit')'},
                {key: 'deposit_term.deposit.bank.{{ \App\Bank::SHORT_NAME }}', sortable: true, label: '@lang('banks.users.deposits.bank')'},
                {key: 'button', label: '@lang('banks.users.instalments.list')'},
                {key: 'button2', label: '@lang('banks.users.withdrawals.list')'},
                {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-button-column>

@endsection