@extends('layouts.app')

@section('content')

    <b-container>

        @if ($model->id)
            {{ Breadcrumbs::render($routerName . '.edit', ...[$user, $model]) }}
        @else
            {{ Breadcrumbs::render($routerName . '.create', $user) }}
        @endif

        <b-form
                action="@if ($model->id){{ route($routerName . '.update', ['user' => $user, 'deposit' => $model]) }}@else{{ route($routerName . '.store', $user) }}@endif"
                method="POST"
                class="needs-validation"
                novalidate
        >

            @if ($model->id)
                @method('PUT')
            @endif

            @csrf

            <h1>
                @lang('banks.users.user', ['user' => $user->name]).
                @if (empty($model->id))
                    @lang('banks.users.deposits.create')
                @else
                    @lang(
                    'banks.users.deposits.edit',
                        [
                        'deposit' => $model->depositTerm->deposit->name,
                        'currency' => $model->depositTerm->currency->iso,
                        'period' => $model->depositTerm->period,
                        'start_date' => (date_create_from_format('Y-m-d H:i:s', $model->start_date))->format('d.m.Y'),
                        ]
                    )
                @endif
            </h1>

            <input id="id" type="hidden" name="id" value="{{ $model->id }}" />
            <input id="{{ \App\UserDeposit::USER_ID }}" type="hidden" name="{{ \App\UserDeposit::USER_ID }}" value="{{ $user->id }}" />

            @select([
                'name' => \App\UserDeposit::DEPOSIT_TERM_ID,
                'label' => __('banks.users.deposits.term'),
                'options' => $depositTerms])

            @number([
                'name' => \App\UserDeposit::BALANCE,
                'label' => __('banks.users.deposits.balance')])

            @date([
                'name' => \App\UserDeposit::START_DATE,
                'label' => __('banks.users.deposits.start_date')])

            @date([
                'name' => \App\UserDeposit::END_DATE,
                'label' => __('banks.users.deposits.end_date')])

            @if (empty($model->id) === false)
                <b-row class="mb-3">
                    <b-button href="instalment" class="offset-sm-3">@lang('banks.users.deposits.instalments')</b-button>
                    <b-button href="withdrawal" class="offset-sm-3">@lang('banks.users.deposits.withdrawals')</b-button>
                </b-row>
            @endif

            <b-button type="submit" variant="primary" class="offset-sm-3">@lang('common.submit')</b-button>
        </b-form>
    </b-container>
@endsection
