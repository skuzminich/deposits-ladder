@extends('layouts.app')

@section('content')

    <b-container>

        @if ($model->id)
            {{ Breadcrumbs::render($routerName . '.edit', ...[$user, $userDeposit, $model]) }}
        @else
            {{ Breadcrumbs::render($routerName . '.create', ...[$user, $userDeposit]) }}
        @endif

        <b-form
                action="@if ($model->id){{ route($routerName . '.update', ['user' => $user, 'deposit' => $userDeposit, 'withdrawal' => $model]) }}
                    @else{{ route($routerName . '.store', ['user' => $user, 'deposit' => $userDeposit]) }}@endif"
                method="POST"
                class="needs-validation"
                novalidate
        >

            @if ($model->id)
                @method('PUT')
            @endif

            @csrf

            <input id="id" type="hidden" name="id" value="{{ $model->id }}" />

            <h1>
                @lang('banks.users.user', ['user' => $user->name]).
                @lang(
                'banks.users.deposits.edit',
                    [
                    'deposit' => $userDeposit->depositTerm->deposit->name,
                    'currency' => $userDeposit->depositTerm->currency->iso,
                    'period' => $userDeposit->depositTerm->period,
                    'start_date' => (date_create_from_format('Y-m-d H:i:s', $userDeposit->start_date))->format('d.m.Y'),
                    ]
                ).
                @if (empty($model->id))
                    @lang('banks.users.withdrawals.create')
                @else
                    @lang(
                    'banks.users.withdrawals.edit',
                        [
                        'withdrawal_date' => (date_create_from_format('Y-m-d H:i:s', $model->withdrawal_date))->format('d.m.Y'),
                        'amount' => $model->amount,
                        ]
                    )
                @endif
            </h1>

            <input id="{{ \App\Instalment::USER_DEPOSIT_IT }}" type="hidden" name="{{ \App\Instalment::USER_DEPOSIT_IT }}" value="{{ $userDeposit->id }}" />

            @number([
                'name' => \App\Withdrawal::AMOUNT,
                'label' => __('banks.users.withdrawals.amount')])

            @date([
                'name' => \App\Withdrawal::WITHDRAWAL_DATE,
                'label' => __('banks.users.withdrawals.withdrawal_date')])

            <b-button type="submit" variant="primary" class="offset-sm-3">@lang('common.submit')</b-button>
        </b-form>
    </b-container>
@endsection
