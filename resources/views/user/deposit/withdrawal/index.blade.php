@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render($routerName . '.index', ...[$user, $userDeposit]) }}

    <b-container fluid>
        <h1>
            @lang('banks.users.user', ['user' => $userDeposit->user->name]).
            @lang('banks.users.withdrawals.withdrawals')
            @lang(
                'banks.users.deposits.edit',
                [
                    'deposit' => $userDeposit->depositTerm->deposit->name,
                    'currency' => $userDeposit->depositTerm->currency->iso,
                    'period' => $userDeposit->depositTerm->period,
                    'start_date' => (date_create_from_format('Y-m-d H:i:s', $userDeposit->start_date))->format('d.m.Y'),
                ]
            )
        </h1>
    </b-container>

    <table-basic
            :route_name = "`{{ route($routerName . '.index', ['user' => $user, 'deposit' => $userDeposit], false) }}`"
            :items = "{{ $items }}"
            :fields = "[
                {key: '{{ \App\Withdrawal::WITHDRAWAL_DATE }}', sortable: true, label: '@lang('banks.users.withdrawals.withdrawal_date')', formatter: (value) => {return new Date(value).toLocaleDateString()}},
                {key: '{{ \App\Withdrawal::AMOUNT }}', sortable: true, label: '@lang('banks.users.withdrawals.amount')'},
                {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-basic>

@endsection