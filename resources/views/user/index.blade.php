@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render($routerName . '.index') }}

    <b-container fluid>
        <h1>@lang('banks.users.list')</h1>
    </b-container>

    <table-button-column
            :route_name = "`{{ route($routerName . '.index', [], false) }}`"
            :items = "{{ $items }}"
            :button_label = "`@lang('banks.users.deposits.list')`"
            :button_action = "`{{ route($routerName . '.deposit.index', ['user' => ':item_id'], false) }}`"
            :fields = "[
                 {key: '{{ \App\User::NAME }}', sortable: true, label: '@lang('banks.users.name')'},
                 {key: '{{ \App\User::EMAIL }}', sortable: true, label: '@lang('banks.users.email')'},
                 {key: 'button', label: '@lang('banks.users.deposits.list')'},
                 {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-button-column>

    <div class="container">
        <div class="col-md-12">
            <passport-clients></passport-clients>
            <passport-authorized-clients></passport-authorized-clients>
            <passport-personal-access-tokens></passport-personal-access-tokens>
        </div>
    </div>

@endsection