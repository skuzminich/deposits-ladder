<b-form-group label="{{ $label }}@if (empty($validation) === false && empty($validation[$name]) === false && strpos($validation[$name], 'required') !== false)*@endif:"
              label-for="{{ $name }}"
              :label-cols="3"
              label-class="text-sm-right"
              horizontal
>

    <b-form-checkbox id="{{ $name }}"
                        checked="{{ old($name, $model->$name) }}"
                        name="{{ $name }}"
                        @validation()
                        value="1"
                        unchecked-value="0"
    >
    </b-form-checkbox>

    <div class="invalid-feedback">
        @if ($errors->has($name))
            @foreach ($errors->get($name) as $error)
                <span>{{ $error }}</span>
            @endforeach
        @else
            {{ __('Please, check the checkbox') }}
        @endif
    </div>

</b-form-group>
