<b-form-group label="{{ $label }}@if (empty($validation) === false && empty($validation[$name]) === false && strpos($validation[$name], 'required') !== false)*@endif:"
              label-for="{{ $name }}"
              :label-cols="3"
              label-class="text-sm-right"
              horizontal
>

    <b-form-radio-group id="{{ $name }}"
                        checked="{{ old($name, $model->$name) }}"
                        name="{{ $name }}"
                        @validation(['class' => 'pt-2 manual-validation'])
    >
        @foreach ($options as $value => $label)
            <b-form-radio value="{{ $value }}">
                {{ $label }}
            </b-form-radio>
        @endforeach
    </b-form-radio-group>

    <div class="invalid-feedback">
        @if ($errors->has($name))
            @foreach ($errors->get($name) as $error)
                <span>{{ $error }}</span>
            @endforeach
        @else
            {{ __('Please, choose an option') }}
        @endif
    </div>

</b-form-group>
