<b-form-invalid-feedback>

    @if ($errors->has($name))
        @foreach ($errors->get($name) as $error)
            <span>{{ $error }}</span>
        @endforeach
    @else
        {{ __('Please, fill this field correctly') }}
    @endif

</b-form-invalid-feedback>