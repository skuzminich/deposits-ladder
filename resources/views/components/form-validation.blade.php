@if (empty($validation) === false && empty($validation[$name]) === false)

    state="@if ($errors->any() && $errors->has($name)) {{ 'valid' }} @elseif ($errors->any()) {{ 'invalid' }} @else {{ '' }} @endif"
    class="@if (empty($class) === false) {{ $class }} @endif @if ($errors->any() && $errors->has($name)) {{ 'is-invalid manual-validation' }} @elseif ($errors->any()) {{ 'is-valid manual-validation' }} @else {{ '' }} @endif"

    @foreach(explode('|', $validation[$name]) as $rule)
        @php
            $ruleArray = explode(':', $rule);
            $ruleName = current($ruleArray);
        @endphp
        @if (in_array($ruleName, ['required', 'between', 'min', 'max']))
            @switch($ruleName)
                @case('between')
                    @php $args = explode(',', $ruleArray[1]) @endphp
                    {{ 'minlength=' . $args[0] }}
                    {{ 'maxlength=' . $args[1] }}
                    @break
                @case('min')
                    @if($type === 'number')
                        {{ 'min=' . $ruleArray[1] }}
                    @else
                        {{ 'minlength=' . $ruleArray[1] }}
                    @endif

                    @break
                @case('max')
                    {{ 'maxlength=' . $ruleArray[1] }}
                    @break
                @default
                    {{ $rule }}
            @endswitch
        @endif
    @endforeach
@endif