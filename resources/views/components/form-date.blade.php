<b-form-group label="{{ $label }}@if (empty($validation) === false && empty($validation[$name]) === false && strpos($validation[$name], 'required') !== false)*@endif:"
              label-for="{{ $name }}"
              :label-cols="3"
              label-class="text-sm-right"
              horizontal
>

    <b-form-input id="{{ $name }}"
                  type="date"
                  name="{{ $name }}"
                  value="{{ substr(old($name, $model->$name), 0, 10) }}"
                  @validation()
    >
    </b-form-input>

    @error()

</b-form-group>
