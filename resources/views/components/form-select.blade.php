<b-form-group label="{{ $label }}@if (empty($validation) === false && empty($validation[$name]) === false && strpos($validation[$name], 'required') !== false)*@endif:"
              label-for="{{ $name }}"
              :label-cols="3"
              label-class="text-sm-right"
              horizontal
>

    <!--suppress HtmlFormInputWithoutLabel -->
    <select class="form-control"
            id="{{ $name }}"
            name="{{ $name }}"
            @if (empty($multiple) === false)
                multiple="multiple"
            @endif
            @validation()
    >
        @foreach($options as $option)
            <option value="{{ $option['id'] }}" @if ((int)$option['id'] === (int)old($name, $model->$name)) selected="selected" @endif>
                @if (empty($textField)) {{ $option['name'] }} @else {{ $option[$textField] }} @endif
            </option>
        @endforeach
    </select>

    @error()

</b-form-group>
