<b-form-group label="{{ $label }}@if (empty($validation) === false && empty($validation[$name]) === false && strpos($validation[$name], 'required') !== false)*@endif:"
              label-for="{{ $name }}"
              :label-cols="3"
              label-class="text-sm-right"
              horizontal
>

    <b-form-input id="{{ $name }}"
                  type="number"
                  step=".01"
                  name="{{ $name }}"
                  value="{{ old($name, $model->$name) }}"
                  @validation(['type' => 'number'])
    >
    </b-form-input>

    @error()

</b-form-group>
