<b-container>

    @if ($model->id)
        {{ Breadcrumbs::render($routerName . '.edit', $model) }}
    @else
        {{ Breadcrumbs::render($routerName . '.create') }}
    @endif

    <b-form
            action="@if ($model->id){{ route($routerName . '.update', $model->id) }}@else{{ route($routerName . '.store') }}@endif"
            method="POST"
            class="needs-validation"
            novalidate
    >

        @if ($model->id)
            @method('PUT')
        @endif

        @csrf

        <input id="id" type="hidden" name="id" value="{{ $model->id }}" />
