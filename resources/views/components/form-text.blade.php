<b-form-group label="{{ $label }}@if (empty($validation) === false && empty($validation[$name]) === false && strpos($validation[$name], 'required') !== false)*@endif:"
              label-for="{{ $name }}"
              :label-cols="3"
              label-class="text-sm-right"
              horizontal
>

    <b-form-input id="{{ $name }}"
                  type="text"
                  name="{{ $name }}"
                  value="{{ old($name, $model->$name) }}"
                  @validation()
    >
    </b-form-input>

    @error()

</b-form-group>
