@extends('layouts.app')

@section('content')

    @startForm()

    <h1>
        @if(empty($model->id))
            @lang('banks.create')
        @else
            @lang('banks.edit', ['bank' => $model->short_name])
        @endif
    </h1>

    @text([
        'name' => \App\Bank::SHORT_NAME,
        'label' => __('banks.short_name')])

    @text([
        'name' => \App\Bank::FULL_NAME,
        'label' => __('banks.full_name')])

    @text([
        'name' => \App\Bank::LINK,
        'label' => __('banks.link')])

    @radio([
        'name' => \App\Bank::STATUS,
        'label' => __('banks.status'),
        'options' => \App\Bank::getAllStatuses(\App\Bank::WITH_LABELS)])

    @if (empty($model->id) === false)
        <b-row class="mb-3">
            <b-button href="/deposit?filter%5Bbank%5D={{ $model->id }}" class="offset-sm-3">@lang('banks.deposits.list')</b-button>
        </b-row>
    @endif

    @endForm()

@endsection