@extends('layouts.app')

@section('content')

    @php
    foreach ($items as &$bank) {
        if ($bank->status === \App\Bank::STATUS_DEPOSITS_FREEZED) {
            $bank->_rowVariant = 'warning';
        } elseif ($bank->status === \App\Bank::STATUS_LIQUIDATED) {
            $bank->_rowVariant = 'danger';
        }
    }
    @endphp

    {{ Breadcrumbs::render($routerName . '.index') }}

    <b-container fluid>
        <h1>@lang('banks.list')</h1>
    </b-container>

    <!--suppress SpellCheckingInspection -->
    <table-button-column
            :route_name = "`{{ route($routerName . '.index', [], false) }}`"
            :items = "{{ $items }}"
            :button_label = "`@lang('banks.deposits.list')`"
            :button_action = "`/deposit?filter%5Bbank%5D=:item_id`"
            :fields = "[
                 {key: '{{ \App\Bank::SHORT_NAME }}', sortable: true, label: '@lang('banks.short_name')'},
                 {key: '{{ \App\Bank::FULL_NAME }}', sortable: true, label: '@lang('banks.full_name')'},
                 {
                    key: '{{ \App\Bank::STATUS }}',
                    label: '@lang('banks.status')',
                    sortable: true,
                    formatter: (value) => {
                        var values = {{ json_encode(\App\Bank::getAllStatuses(\App\Bank::WITH_LABELS), JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE) }};
                        return (values[value]);
                     }
                 },
                 {key: 'link', sortable: false, label: '@lang('banks.link')'},
                 {key: 'button', label: '@lang('banks.deposits.list')'},
                 {key: 'actions', label: '@lang('common.actions')'}]"
    >
        @csrf
    </table-button-column>
@endsection