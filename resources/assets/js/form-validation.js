(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                let manualValidation = $('.manual-validation');
                manualValidation.removeClass('is-valid').removeClass('is-invalid');
                $('.manual-validation .custom-radio .custom-control-input').removeClass('is-valid').removeClass('is-invalid');

                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }

                manualValidation.each(function(i, elementWrapper) {
                    ($(elementWrapper).find('.custom-radio .custom-control-input').each(function(j, element){
                        if ($(element).is(':invalid')) {
                            elementWrapper.classList.add('is-invalid');
                        }
                    }))
                });

                form.classList.add('was-validated');
            }, false);
        });

        let inputs = $('input.manual-validation');
        Array.prototype.filter.call(inputs, function(input) {
            // noinspection JSUnusedLocalSymbols
            input.addEventListener('change', function(event) {
                if (input.classList.contains('is-invalid')) {
                    input.classList.remove('is-invalid');
                    input.classList.add('is-valid');
                }
            }, false);
        });

        let radioWrappers = $('div.manual-validation');
        radioWrappers.each(function(index, radioContainer) {
            let radios = $(radioContainer).find('.custom-radio .custom-control-input');
            Array.prototype.filter.call(radios, function(radio) {
                if (radioContainer.classList.contains('is-invalid')) {
                    radio.classList.add('is-invalid');
                }
                if (radioContainer.classList.contains('is-valid')) {
                    radio.classList.add('is-valid');
                }

                // noinspection JSUnusedLocalSymbols
                radio.addEventListener('change', function(event) {
                    if (radio.classList.contains('is-invalid')) {
                        let allRadios = $(radioContainer).find('.custom-radio .custom-control-input');
                        allRadios.removeClass('is-invalid').addClass('is-valid');
                    }
                    $(radioContainer).removeClass('is-invalid').addClass('is-valid');
                }, false);
            });

        })

    }, false);
})();