// noinspection JSUnresolvedFunction
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// noinspection JSUnresolvedFunction
window.Vue = require('vue');

import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

import VueResource from 'vue-resource';
Vue.use(VueResource);

import VueInternationalization from 'vue-i18n';
Vue.use(VueInternationalization);

import Locale from './vue-i18n-locales.generated';
const lang = document.documentElement.lang.substr(0, 2);

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});


// noinspection JSUnresolvedFunction
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component(
    'table-basic',
    require('./components/TableBasic.vue')
);

// noinspection JSUnresolvedFunction
Vue.component(
    'table-button-column',
    require('./components/TableButtonColumn.vue')
);

// noinspection JSUnresolvedFunction
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

// noinspection JSUnresolvedFunction
Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

// noinspection JSUnresolvedFunction
Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

// noinspection JSUnusedLocalSymbols
const app = new Vue({
    el: '#app',
    i18n
});

// noinspection JSUnresolvedFunction
require('./form-validation');