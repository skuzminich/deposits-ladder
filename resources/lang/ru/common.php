<?php
return [
    'home' => 'Главная',
    'submit' => 'Сохранить',
    'create' => 'Создать',
    'edit' => 'Изменить',
    'delete' => 'Удалить',
    'are_you_sure_delete' => 'Вы уверены, что хотите удалить',
    'sorry_unable_delete' => 'Извините, мы не смогли удалить запись! :\'(',
    'deleted_successfully' => 'Запись успешно удалена',
    'actions' => 'Действия',
    'are_you_sure' => 'Вы уверены?',
    'cancel' => 'Отмена',
    'filter' => 'Фильтровать',
    'search' => 'Поиск',
    'choose_please' => 'Выберите, пожалуйста',
    'link' => 'Ссылка',
    'found' => 'Найдено',
];