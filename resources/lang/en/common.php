<?php
return [
    'home' => 'Home',
    'submit' => 'Save',
    'create' => 'Create',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'are_you_sure_delete' => 'Are you sure, you want to delete',
    'sorry_unable_delete' => 'Sorry, we were unable to delete it ! :\'(',
    'deleted_successfully' => 'Item was deleted successfully',
    'actions' => 'Actions',
    'are_you_sure' => 'Are you sure?',
    'cancel' => 'Cancel',
    'filter' => 'Filter',
    'search' => 'Search',
    'choose_please' => 'Choose, please',
    'link' => 'Link',
    'found' => 'Found',
];