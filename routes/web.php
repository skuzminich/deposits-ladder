<?php /** @noinspection PhpUndefinedClassInspection */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '  home');

Route::resource('bank', 'BankController')->except('show');
Route::resource('deposit', 'DepositController')->except('show');
Route::resource('deposit.term', 'DepositTermController')->except('show');
Route::resource('deposit.term.update', 'DepositTermUpdateController')->except('show');
Route::resource('user', 'UserController')->except('show');
Route::resource('user.deposit', 'UserDepositController')->except('show');
Route::resource('user.deposit.instalment', 'InstalmentController')->except('show');
Route::resource('user.deposit.withdrawal', 'WithdrawalController')->except('show');
Route::resource('currency', 'CurrencyController')->except('show');

Route::get('/deposit/{deposit}/term/mass-update', 'DepositTermUpdateController@massUpdate')
    ->name('deposit.term.update.mass-update');
Route::post('/deposit/{deposit}/term/mass-update-store', 'DepositTermUpdateController@massUpdateStore')
    ->name('deposit.term.update.mass-update-store');

/** @noinspection PhpUndefinedMethodInspection */
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/index', 'HomeController@index')->name('home.index');
Route::get('/home/trend', 'HomeController@trend')->name('home.trend');
