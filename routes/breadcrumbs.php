<?php /** @noinspection PhpUndefinedFieldInspection */

try {
    Breadcrumbs::for(
        'home',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->push(__('common.home'), route('home'));
        }
    );

    Breadcrumbs::for(
        'bank.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->parent('home');
            $trail->push(__('banks.index'), route('bank.index'));
        }
    );

    Breadcrumbs::for(
        'bank.create',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->parent('bank.index');
            $trail->push(__('banks.create'), route('bank.create'));
        }
    );

    Breadcrumbs::for(
        'bank.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Bank $bank) {
            $trail->parent('bank.index');
            $trail->push(__('banks.edit', ['bank' => $bank->short_name]), route('bank.edit', $bank));
        }
    );

    Breadcrumbs::for(
        'currency.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->parent('home');
            $trail->push(__('banks.currencies.list'), route('currency.index'));
        }
    );

    Breadcrumbs::for(
        'currency.create',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->parent('currency.index');
            $trail->push(__('banks.currencies.create'), route('currency.create'));
        }
    );

    Breadcrumbs::for(
        'currency.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Currency $currency) {
            $trail->parent('currency.index');
            $trail->push(__('banks.currencies.edit', ['currency' => $currency->iso]), route('currency.edit', $currency));
        }
    );

    Breadcrumbs::for(
        'deposit.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->parent('home');
            $trail->push(__('banks.deposits.list'), route('deposit.index'));
        }
    );

    Breadcrumbs::for(
        'deposit.create',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->parent('deposit.index');
            $trail->push(__('banks.deposits.create'), route('deposit.create'));
        }
    );

    Breadcrumbs::for(
        'deposit.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit) {
            $trail->parent('deposit.index');
            $trail->push(__('banks.deposits.edit', ['deposit' => $deposit->name, 'bank' => $deposit->bank->short_name]), route('deposit.edit', $deposit));
        }
    );

    Breadcrumbs::for(
        'deposit.term.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit) {
            $trail->parent('deposit.index');
            $trail->push(__('banks.deposits.terms.list', ['deposit' => $deposit->name, 'bank' => $deposit->bank->short_name]), route('deposit.term.index', $deposit));
        }
    );

    Breadcrumbs::for(
        'deposit.term.create', function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit) {
            $trail->parent('deposit.term.index', $deposit);
            $trail->push(__('banks.deposits.terms.create'), route('deposit.term.create', $deposit));
        }
    );

    Breadcrumbs::for(
        'deposit.term.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit, \App\DepositTerm $depositTerm) {
            $trail->parent('deposit.term.index', $deposit);
            $trail->push(
                __('banks.deposits.terms.edit', ['period' => $depositTerm->period, 'currency' => $depositTerm->currency->iso]),
                route('deposit.term.edit', ['deposit' => $deposit, 'term' => $depositTerm])
            );
        }
    );

    Breadcrumbs::for(
        'deposit.term.update.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit, \App\DepositTerm $depositTerm) {
            $trail->parent('deposit.term.edit', $deposit, $depositTerm);
            $trail->push(
                __('banks.deposits.terms.updates.list', ['deposit' => $deposit->name]),
                route('deposit.term.update.index', ['deposit' => $deposit, 'term' => $depositTerm])
            );
        }
    );

    Breadcrumbs::for(
        'deposit.term.update.create',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit, \App\DepositTerm $depositTerm) {
            $trail->parent('deposit.term.update.index', $deposit, $depositTerm);
            $trail->push(
                __('banks.deposits.terms.updates.create'),
                route('deposit.term.update.create', ['deposit' => $deposit, 'term' => $depositTerm])
            );
        }
    );

    Breadcrumbs::for(
        'deposit.term.update.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit,
                  \App\DepositTerm $depositTerm, \App\DepositTermUpdate $update
        ) {
            $trail->parent('deposit.term.update.index', $deposit, $depositTerm);
            $fromDate  = \App\DepositTermUpdate::DEPOSIT_FROM_DATE;
            $startDate = date_create_from_format('Y-m-d H:i:s',$update->$fromDate);
            $toDate    = \App\DepositTermUpdate::DEPOSIT_TO_DATE;
            $endDate   = date_create_from_format('Y-m-d H:i:s', $update->$toDate);
            $trail->push(
                __(
                    'banks.deposits.terms.updates.edit',
                    [
                        'start_date' => $startDate->format('d.m.Y'),
                        'end_date' => ($endDate) ? $endDate->format('d.m.Y') : ''
                    ]
                ),
                route('deposit.term.update.edit', ['deposit' => $deposit, 'term' => $depositTerm, 'update' => $update])
            );
        }
    );

    Breadcrumbs::for(
        'deposit.term.update.mass-update',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\Deposit $deposit) {
            $trail->parent('deposit.term.index', $deposit);
            $trail->push(
                __('banks.deposits.terms.create_multiple_updates'),
                route('deposit.term.update.mass-update', ['deposit' => $deposit])
            );
        }
    );

    Breadcrumbs::for(
        'user.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail) {
            $trail->parent('home');
            $trail->push(__('banks.users.list'), route('user.index'));
        }
    );

    Breadcrumbs::for(
        'user.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user) {
            $trail->parent('user.index');
            $trail->push(__('banks.users.edit', ['user' => $user->name]), route('user.edit', $user));
        }
    );

    Breadcrumbs::for(
        'user.deposit.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user) {
            $trail->parent('user.edit', $user);
            $trail->push(__('banks.users.deposits.list', ['user' => $user->name]), route('user.deposit.index', $user));
        }
    );

    Breadcrumbs::for(
        'user.deposit.create',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user) {
            $trail->parent('user.deposit.index', $user);
            $trail->push(__('banks.users.deposits.create'), route('user.deposit.create', $user));
        }
    );

    Breadcrumbs::for(
        'user.deposit.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user, \App\UserDeposit $userDeposit) {
            $trail->parent('user.deposit.index', $user);
            $startDate = date_create_from_format('Y-m-d H:i:s', $userDeposit->start_date);
            $trail->push(
                __(
                    'banks.users.deposits.edit',
                    [
                        'deposit' => $userDeposit->depositTerm->deposit->name,
                        'currency' => $userDeposit->depositTerm->currency->iso,
                        'period' => $userDeposit->depositTerm->period,
                        'start_date' => $startDate->format('d.m.Y'),
                    ]
                ),
                route('user.deposit.edit', ['user' => $user, 'deposit' => $userDeposit])
            );
        }
    );

    Breadcrumbs::for(
        'user.deposit.instalment.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user, \App\UserDeposit $userDeposit) {
            $trail->parent('user.deposit.edit', $user, $userDeposit);
            $trail->push(
                __('banks.users.instalments.list'),
                route('user.deposit.instalment.index', ['user' => $user, 'deposit' => $userDeposit])
            );
        }
    );

    Breadcrumbs::for(
        'user.deposit.instalment.create',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user, \App\UserDeposit $userDeposit) {
            $trail->parent('user.deposit.instalment.index', $user, $userDeposit);
            $trail->push(
                __('banks.users.instalments.create'),
                route('user.deposit.instalment.create', ['user' => $user, 'deposit' => $userDeposit])
            );
        }
    );

    Breadcrumbs::for(
        'user.deposit.instalment.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user, \App\UserDeposit $userDeposit,
                  \App\Instalment $instalment
        ) {
            $trail->parent('user.deposit.instalment.index', $user, $userDeposit);
            $depositDate = date_create_from_format('Y-m-d H:i:s', $instalment->deposit_date);
            $trail->push(
                __('banks.users.instalments.edit', ['deposit_date' => $depositDate->format('d.m.Y'), 'amount' => $instalment->amount]),
                route('user.deposit.instalment.edit', ['user' => $user, 'deposit' => $userDeposit, 'instalment' => $instalment])
            );
        }
    );

    Breadcrumbs::for(
        'user.deposit.withdrawal.index',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user, \App\UserDeposit $userDeposit) {
            $trail->parent('user.deposit.edit', $user, $userDeposit);
            $trail->push(
                __('banks.users.withdrawals.list'),
                route('user.deposit.withdrawal.index', ['user' => $user, 'deposit' => $userDeposit])
            );
        }
    );

    Breadcrumbs::for(
        'user.deposit.withdrawal.create',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user, \App\UserDeposit $userDeposit) {
            $trail->parent('user.deposit.withdrawal.index', $user, $userDeposit);
            $trail->push(
                __('banks.users.withdrawals.create'),
                route('user.deposit.withdrawal.create', ['user' => $user, 'deposit' => $userDeposit])
            );
        }
    );

    Breadcrumbs::for(
        'user.deposit.withdrawal.edit',
        function (\DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator $trail, \App\User $user, \App\UserDeposit $userDeposit,
            \App\Withdrawal $withdrawal
        ) {
            $trail->parent('user.deposit.withdrawal.index', $user, $userDeposit);
            $withdrawalDate = date_create_from_format('Y-m-d H:i:s', $withdrawal->withdrawal_date);
            $trail->push(
                __(
                    'banks.users.withdrawals.edit',
                    ['withdrawal_date' => $withdrawalDate->format('d.m.Y'), 'amount' => $withdrawal->amount]
                ),
                route('user.deposit.withdrawal.edit', ['user' => $user, 'deposit' => $userDeposit, 'withdrawal' => $withdrawal])
            );
        }
    );
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
    /** @noinspection PhpUnhandledExceptionInspection */
    throw $e;
}
