<?php /** @noinspection PhpUndefinedClassInspection */

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('banks', 'Api\\BankController')
    ->except(['store', 'update', 'destroy']);

Route::apiResource('currencies', 'Api\\CurrencyController')
    ->except(['store', 'update', 'destroy']);

Route::apiResource('deposits', 'Api\\DepositController')
    ->except(['store', 'update', 'destroy']);

Route::apiResource('deposits.terms', 'Api\\DepositTermController')
    ->except(['store', 'update', 'destroy']);

Route::apiResource('deposits.terms.updates', 'Api\\DepositTermUpdateController')
    ->except(['store', 'update', 'destroy']);

Route::apiResource('users.deposits', 'Api\\UserDepositController');

Route::apiResource('users.deposits.instalments', 'Api\\InstalmentController');

Route::apiResource('users.deposits.withdrawals', 'Api\\WithdrawalController');